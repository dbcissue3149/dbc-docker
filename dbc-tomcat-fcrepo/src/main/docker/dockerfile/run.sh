#!/usr/bin/env bash
set -e  

/usr/local/bin/confd -onetime -backend env

echo "Modifying properties"
export FIELD_SEARCH_PROPERTIES=$(cat /tmp/fieldsearchlucene.xml)
perl -i -pe 'BEGIN{undef $/;} s/<module role="org.fcrepo.server.search.FieldSearch".*?<\/module>/$ENV{'FIELD_SEARCH_PROPERTIES'}/smg' $FEDORA_HOME/server/config/fedora.fcfg

echo "Starting Tomcat"
exec /usr/local/tomcat/bin/catalina.sh run
