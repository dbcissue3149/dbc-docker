#!/usr/bin/env bash

# You have the following environment variables automatically at your disposal:
# Your own compose file (you have to write it)
# COMPOSE_FILE=$BASEDIR/src/main/docker/share/compose/$ARTIFACT_ID-compose.yml

# The compose files of the projects you depend on are placed here.
# SHARED_COMPOSE_FILE_DIR=$BASEDIR/src/resource/docker/compose

# The collection of shared compose files with -f parameter in front,
# so they are easy to use when calling compose.
# COMPOSE_DEPENDENCIES=-f a-compose-i-depend-upon.yml -f another-compose-i-depend-upon.yml

function be
{
    if [ "$1" == "quiet" ]; then
        set +x
    else [ "$1" == "loud" ];
        set -x
    fi
}
be loud

function fail
{
    if [ "$1" == "early" ]; then
        set -eo pipefail
    else [ "$1" == "late" ];
        set +eo pipefail
    fi

}
fail early

# Default tests
#
# The default* functions below, starts the default composes, tests that they are up, and takes them down again.
# You do not have to use the default behaviour. You can skip it completely, or extend it.
# If you want to skip it, simply delete the defaultXXX function calls.
# If you want to extend it, add your own calls that does whatever you want done.
# Extension example
# default        -> trap defaultCleanup EXIT
# default        -> defaultStartup
# default        -> defaultTest
# your extension -> exitIfStringDoesNotAppearInLogs CONTAINER_ID 10 "Expected string in logs" # CONTAINER_ID SECONDS_TO_WAIT STRING_WE_ARE_LOOKING_FOR
#
# You can see exactly what these methods do in the shell script setupEnvironmentAndCommonFunctionsForDevelopers.sh

# Cleanup is performed on all exits, and should keep the build machine clean.
#     If you dont need it, delete it.
#     If you want to do additional cleanup, write your own function and trap it to EXIT, like so:
#     trap myCustomCleanupFunction EXIT
trap defaultCleanup EXIT

# DefaultStartup is a generic startup that should work for most containers out of the box.
#     If you dont need it, delete it.
#     If you want to do additional startup things, just do it, like so:
#     echo Test started at `date`
defaultStartup

# DefaultTest is a basic test that should work for most setups.
defaultTest

# Common test functions
#
# We have written some scripts you can use. They are shell scripts, and they are suffixed with .function
# You are welcome to bugfix the existing, or add your own (generic) .function files.
# The common .function files are located in /dbc-docker/dbc-maven-testsuite/src/main/docker/share/test-suite/common-test-functions

# Custom behaviour
#
# If the default tests and common functions does not fit your need, write your own.
# Consider sharing them as common functions if they are good quality and generic.