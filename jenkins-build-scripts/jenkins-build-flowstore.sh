#!/usr/bin/env bash
set -e

# Pull latest glassfish image
docker pull docker-index:5000/dbc-glassfish
docker tag -f docker-index:5000/dbc-glassfish dbc-glassfish

# Build the update
cd ./dbc-glassfish-flowstore
docker build --no-cache -t dbc-glassfish-flowstore .
docker tag -f dbc-glassfish-flowstore docker-index:5000/dbc-glassfish-flowstore

# Push the processor image
docker push docker-index:5000/dbc-glassfish-flowstore

# Clean up the two glassfish based images
docker images | grep -- dbc-glassfish-flowstore | awk '{print $1}'|xargs -r docker rmi

# Clean up the two glassfish based images
docker images | grep -- dbc-glass | awk '{print $1}'|xargs -r docker rmi

echo 'Docker images in all, on this server:'
docker images --all | wc -l

