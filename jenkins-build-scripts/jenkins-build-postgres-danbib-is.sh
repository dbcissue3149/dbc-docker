#!/usr/bin/env bash
set -e

# Pull latest Postgres image
docker pull docker-index:5000/dbc-postgres
docker tag -f docker-index:5000/dbc-postgres dbc-postgres

# Build postgres-danbib-is
cd ./dbc-postgres-danbib-is
docker build --no-cache -t dbc-postgres-danbib-is .
docker tag -f dbc-postgres-danbib-is docker-index:5000/dbc-postgres-danbib-is

# Push the postgres-danbib-is image
docker push docker-index:5000/dbc-postgres-danbib-is

# Clean up the danbib-is image
docker images | grep -- dbc-postgres-danbib-is | awk '{print $1}'|xargs -r docker rmi

# Clean up the postgres based images
docker images | grep -- dbc-postgres | awk '{print $1}'|xargs -r docker rmi

echo 'Docker images in all, on this server:'
docker images --all | wc -l
