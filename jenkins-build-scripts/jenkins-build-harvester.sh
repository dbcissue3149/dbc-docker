#!/usr/bin/env bash
set -e

# Pull latest glassfish image
docker pull docker-index:5000/dbc-glassfish
docker tag -f docker-index:5000/dbc-glassfish dbc-glassfish

# Build the harvester
cd ./dbc-glassfish-harvester
docker build --no-cache -t dbc-glassfish-harvester .
docker tag -f dbc-glassfish-harvester docker-index:5000/dbc-glassfish-harvester

# Push the processor image
docker push docker-index:5000/dbc-glassfish-harvester

# Clean up the harvester imnage
docker images | grep -- dbc-glassfish-harvester | awk '{print $1}'|xargs -r docker rmi

# Clean up the glassfish image
docker images | grep -- dbc-glass | awk '{print $1}'|xargs -r docker rmi

echo 'Docker images in all, on this server:'
docker images --all | wc -l

