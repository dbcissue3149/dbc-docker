#!/usr/bin/env bash

# exit on failure
set -e

# Print command traces before executing command.
set -o xtrace

docker --version
docker-compose --version

TEST_RUNNER_IMAGE=test-runner-image
TEST_RUNNER_CONTAINER_ID=test-runner-container
COMPOSE_ENV_NAME=test
COMPOSE_TEST_RUNNER_IMAGE="$COMPOSE_ENV_NAME"_"$TEST_RUNNER_IMAGE"
CLEANUP_LOG_FILE=cleanup.log

function cleanup
{
    echo Let cleanup finish even if there are errors
    set +e

    echo jenkins-test-openformat-java7.sh stopped. Cleaning up > $CLEANUP_LOG_FILE
    echo Get the result file, regardless how it looks ... >> $CLEANUP_LOG_FILE
    docker cp $TEST_RUNNER_CONTAINER_ID:/home/oftest/junit.xml . || true

    echo Stopping compose system >> $CLEANUP_LOG_FILE
    docker-compose -p $COMPOSE_ENV_NAME -f openformat-compose-java7-base.yml -f openformat-compose-java7.yml stop || true

    echo Removing compose system >> $CLEANUP_LOG_FILE
    docker-compose -p $COMPOSE_ENV_NAME -f openformat-compose-java7-base.yml -f openformat-compose-java7.yml rm -f || true

    echo Removing test-runner >> $CLEANUP_LOG_FILE
    docker stop $TEST_RUNNER_CONTAINER_ID || true
    docker rm   $TEST_RUNNER_CONTAINER_ID || true

    echo Removing test-runner >> $CLEANUP_LOG_FILE
    docker stop test-runner || true
    docker rm   test-runner || true

    echo Removing test image $COMPOSE_TEST_RUNNER_IMAGE created by compose >> $CLEANUP_LOG_FILE
    docker rmi $COMPOSE_TEST_RUNNER_IMAGE || true

    echo Done cleaning up >> $CLEANUP_LOG_FILE
}

function setup
{
    echo destroy old $TEST_RUNNER_CONTAINER_ID container if its there
    docker rm -f $TEST_RUNNER_CONTAINER_ID || true

    # If you change the test image, it needs to be rebuild. Since this is not done automatically by Jenkins
    #   the line below is provided so you can force it.
    #   I have left the line, so the image is always rebuilt, based on the assumption that it is better the
    #   Job is slow than incorrect.
    docker build --no-cache=true -t dbc-integration-test-openformat-java7 dbc-integration-test-openformat-java7

    docker-compose -p $COMPOSE_ENV_NAME -f openformat-compose-java7-base.yml up -d
    # Wait until the service is up, so I can connect to it.
    SECS_TO_WAIT=30
    echo "Wait $SECS_TO_WAIT seconds for the service to start"
    sleep $SECS_TO_WAIT
}

# Always stop the service after test, regardless of how the script was exited.
trap cleanup EXIT
# Cleanup before as well, since it seems Jenkins does not handle trap EXIT very well.
#   See also https://gist.github.com/datagrok/dfe9604cb907523f4a2f
cleanup
setup

docker-compose -p $COMPOSE_ENV_NAME -f openformat-compose-java7-base.yml -f openformat-compose-java7.yml run --name $TEST_RUNNER_CONTAINER_ID $TEST_RUNNER_IMAGE /home/oftest/run-regression-test-java-7.sh
exit 0
