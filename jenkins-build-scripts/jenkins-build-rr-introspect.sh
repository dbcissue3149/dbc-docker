#!/usr/bin/env bash
set -e

# Pull latest glassfish image
docker pull docker-index:5000/dbc-glassfish
docker tag -f docker-index:5000/dbc-glassfish dbc-glassfish

# Build the rr-introspect
cd ./dbc-glassfish-rr-introspect
docker build --build-arg RELEASE_NUM=$1 --no-cache -t dbc-glassfish-rr-introspect .
docker tag -f dbc-glassfish-rr-introspect docker-index:5000/dbc-glassfish-rr-introspect

# Push the rr-introspect image
docker push docker-index:5000/dbc-glassfish-rr-introspect

# Clean up the rr-introspect imnage
docker images | grep -- dbc-glassfish-rr | awk '{print $1}'|xargs -r docker rmi

# Clean up the glassfish image
#docker images | grep -- dbc-glass | awk '{print $1}'|xargs -r docker rmi

echo 'Docker images in all, on this server:'
docker images --all | wc -l

