#!/usr/bin/env bash
set -e
echo "┌┬┐┌┐ ┌─┐  ┌─┐┌─┐┌─┐┌─┐┬ ┬┌─┐  ┌─┐┬ ┬┌─┐";
echo " ││├┴┐│    ├─┤├─┘├─┤│  ├─┤├┤   ├─┘├─┤├─┘";
echo "─┴┘└─┘└─┘  ┴ ┴┴  ┴ ┴└─┘┴ ┴└─┘  ┴  ┴ ┴┴  ";
# Build the dbc-apache-php
cd ./dbc-apache-php
docker build --no-cache -t dbc-apache-php .
docker tag -f dbc-apache-php docker-index:5000/dbc-apache-php

# Push the dbc-apache-php image
docker push docker-index:5000/dbc-apache-php

# Clean up the dbc-apache-php image
docker images | grep -- dbc-apache-php | awk '{print $1}'|xargs -r docker rmi

echo 'Docker images in all, on this server:'
docker images --all | wc -l
