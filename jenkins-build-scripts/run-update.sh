#!/usr/bin/env bash
echo "Stopping existing containers"
docker stop dbc-glassfish-update || true
docker rm dbc-glassfish-update || true
echo "Staring container"
nohup docker run --name dbc-glassfish-update --rm -v /cmdbdata:/tmp --env-file=/data/dataio/docker/env-update.env -p 4852:4848 -p 8092:8080 docker-index:5000/dbc-glassfish-update >/data/dataio/docker/update.log 2>&1 &
