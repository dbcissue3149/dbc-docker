#!/usr/bin/env bash
set -e

# Pull latest glassfish image
docker pull docker-index:5000/dbc-glassfish
docker tag -f docker-index:5000/dbc-glassfish dbc-glassfish

# Build the logstore 
cd ./dbc-glassfish-logstore
docker build --no-cache -t dbc-glassfish-logstore .
docker tag -f dbc-glassfish-logstore docker-index:5000/dbc-glassfish-logstore

# Push the processor image
docker push docker-index:5000/dbc-glassfish-logstore

# Clean up the two glassfish based images
#docker images | grep -- dbc-glassfish-logstore | awk '{print $1}'|xargs -r docker rmi

# Clean up the two glassfish based images
docker images | grep -- dbc-glass | awk '{print $1}'|xargs -r docker rmi

echo 'Docker images in all, on this server:'
docker images --all | wc -l

