#!/usr/bin/env bash
#docker run --name dbc-glassfish-jobprocessor --rm -v /tmp/cmdb:/cmdbdata -v /home/jbr/dataiologs:/data/dataio/logs -v /etc/localtime:/etc/localtime:ro -e "CMDB=http://danbib-i01:8001/dataio" -e "TOKEN=etellerandet" -p 4848:4848 -p 8080:8080 --add-host queue:$(getip.sh dataio-be-s02)  --add-host "jobstore filestore":$(getip.sh dataio-be-s01) --add-host logstoredb:$(getip.sh pgtest) dbc-glassfish-jobprocessor


#docker run --name dbc-glassfish-jobprocessor --rm -p 4848:4848 -p 8080:8080 --env-file=./env-jobprocessor.env docker-index:5000/dbc-glassfish-jobprocessor
docker run --name dbc-glassfish-jobprocessor --rm -p 4848:4848 -p 8080:8080 --add-host=dbc-glassfish-jobstore:172.20.1.191 --add-host=dbc-glassfish-flowstore:172.20.1.191 --add-host=dbc-glassfish-logstore:172.20.1.192  --add-host=dbc-glassfish-filestore:172.20.1.191 --add-host=dbc-openmq:172.20.1.227 docker-index:5000/dbc-glassfish-jobprocessor

