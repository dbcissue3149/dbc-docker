#!/usr/bin/env bash
set -e

# Pull latest glassfish image
docker pull docker-index:5000/dbc-glassfish
docker tag -f docker-index:5000/dbc-glassfish dbc-glassfish

# Build the update
cd ./dbc-glassfish-update-jesper-bruun
docker build --no-cache -t dbc-glassfish-update .
docker tag -f dbc-glassfish-update docker-index:5000/dbc-glassfish-update

# Push the processor image
docker push docker-index:5000/dbc-glassfish-update

# Clean up the two glassfish based images
docker images | grep -- dbc-glassfish-upd | awk '{print $1}'|xargs -r docker rmi

# Clean up the two glassfish based images
docker images | grep -- dbc-glass | awk '{print $1}'|xargs -r docker rmi

echo 'Docker images in all, on this server:'
docker images --all | wc -l

