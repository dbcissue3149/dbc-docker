#!/usr/bin/env bash
docker run --name dbc-glassfish-rr-introspect --rm -v /cmdbdata:/tmp --env-file=./env-rr-introspect.env -p 4851:4848 -p 8091:8080 docker-index:5000/dbc-glassfish-rr-introspect
