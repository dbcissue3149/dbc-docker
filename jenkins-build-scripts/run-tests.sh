#!/usr/bin/env bash

echo "DEPRECATED"
echo "Use the one in docker-tools-testsuite"
echo "Exiting ..."
exit 1

# Settings some defensive bash vars, see
# http://www.davidpashley.com/articles/writing-robust-shell-scripts/#id2382181
# These will apply to alle the scripts.

set -o errexit # Exit whenever you encounter an error
set -o nounset # Exit if an unset variable is used
set -o verbose # Be loud. You could keep this on, as it will make it easier to
               #   pinpoint an error during automatic (CI) test execution.

function showHelp {
	echo " ---- USAGE ----"
	echo $0 " Runs all tests"
	echo $0 "--prefix PREFIX [--prefix|-p] Runs all tests with prefix."
	echo "     If prefix is 'dbc-postgres' test 01-dbc-postgres-create.sh"
	echo "     and 02-dbc-postgres-destroy will be executed,"
	echo "     but 01-dbc-glassfish.sh will not. This allows you to "
	echo "     run partial tests."
} 

PREFIX=test.d/*.sh

# As long as there are arguments given by the user
while [[ $# > 0 ]]
do
	key="$1"
	
	case $key in 
		-p|--prefix)
        # allow user to execute only tests with a certain prefix
        PREFIX=test.d/*$2*.sh
	    shift # past switch
	    shift # past argument
	    ;;
		-h|--help)
		showHelp
		exit 0
	    ;;
	    *)
	    echo "Unknown option $2"
	    ;;
	esac
done	    

echo Executing tests with prefix $PREFIX
TESTS_EXECUTED=""

shopt -s nullglob
for test in $PREFIX; do
	echo "===> Executing test [" $test "]" 
	$BASH $test
	
    EXIT_CODE=$?
    if [[ "$EXIT_CODE" != "0" ]]; then
        echo "Exit code from script ["$test"] is not zero ["$EXIT_CODE"] stopping test execution"
        echo "FAILURE-DBC-DOCKER-TEST Test exited with error code" 
        exit 1
      fi	
	TESTS_EXECUTED+="$test "
done

echo "Executed tests " $TESTS_EXECUTED
echo "SUCCESS-DBC-DOCKER-TEST All tests executed without error codes"

