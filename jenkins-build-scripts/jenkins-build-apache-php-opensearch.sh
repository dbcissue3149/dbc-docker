#!/usr/bin/env bash
set -e

docker pull docker-index:5000/dbc-apache-php
docker tag -f docker-index:5000/dbc-apache-php dbc-apache-php

# Build Opensearch image
cd ./dbc-apache-php-opensearch
docker build --build-arg RELEASE_NUM=$1 --no-cache -t dbc-apache-php-opensearch .
docker tag -f dbc-apache-php-opensearch docker-index:5000/dbc-apache-php-opensearch

# docker push docker-index:5000/dbc-apache-php-opensearch

# Clean up
# docker images | grep -- dbc-apache-php-opensearch | awk '{print $1}'|xargs -r docker rmi

echo 'Docker images in all, on this server:'
docker images --all | wc -l
