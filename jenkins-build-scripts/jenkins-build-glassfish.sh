#!/usr/bin/env bash
set -e

# Pull latest java8 image
docker pull docker-index:5000/java8
docker tag -f docker-index:5000/java8 dbc-java8

# Build the glassfish
cd ./dbc-glassfish
docker build --no-cache -t dbc-glassfish .
docker tag -f dbc-glassfish docker-index:5000/dbc-glassfish

# Push the glassfish image
docker push docker-index:5000/dbc-glassfish

# Clean up the glassfish image
docker images | grep -- dbc-glassfish | awk '{print $1}'|xargs -r docker rmi

# Clean up the java8 based images
docker images | grep -- dbc-java8 | awk '{print $1}'|xargs -r docker rmi

echo 'Docker images in all, on this server:'
docker images --all | wc -l
