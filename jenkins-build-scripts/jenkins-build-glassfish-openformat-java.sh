#!/usr/bin/env bash
set -e

# Pull latest glassfish image
docker pull docker-index:5000/dbc-glassfish
docker tag -f docker-index:5000/dbc-glassfish dbc-glassfish

# Build the openformat glassfish
cd ./dbc-glassfish-openformat-java
docker build --no-cache -t dbc-glassfish-openformat-java .
docker tag -f dbc-glassfish-openformat-java docker-index:5000/dbc-glassfish-openformat-java

# Push the openformat glassfish image
docker push docker-index:5000/dbc-glassfish-openformat-java

# clean up will be part of a post job...

#docker images | grep -- dbc-glassfish-openformat-java
# Clean up the openformat glassfish image
#docker images | grep -- dbc-glassfish-openformat-java | awk '{print $1}'|xargs -r docker rmi


#echo 'Docker images in all, on this server:'
#docker images --all | wc -l
