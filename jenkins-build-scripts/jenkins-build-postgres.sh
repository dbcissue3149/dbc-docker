#!/usr/bin/env bash
set -e

# Pull latest Debian Jessie image
docker pull docker-index:5000/jessie
docker tag -f docker-index:5000/jessie dbc-jessie

# Build Postgres
cd ./dbc-postgres
docker build --no-cache -t dbc-postgres .
docker tag -f dbc-postgres docker-index:5000/dbc-postgres

# Push the Postgres image
docker push docker-index:5000/dbc-postgres

# Clean up the postgres image
docker images | grep -- dbc-postgres | awk '{print $1}'|xargs -r docker rmi

# Clean up the java8 based images
docker images | grep -- dbc-jessie | awk '{print $1}'|xargs -r docker rmi

echo 'Docker images in all, on this server:'
docker images --all | wc -l
