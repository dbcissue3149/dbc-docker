#!/usr/bin/env bash
docker run --name dbc-glassfish-harvester --rm -v /cmdbdata:/tmp --env-file=./env-harvester.env -p 4849:4848 -p 8090:8080 --add-host "filestoredb filestore jobstore":$(getip.sh dataio-be-s01) --add-host postgresdb:172.16.4.75 dbc-glassfish-harvester

