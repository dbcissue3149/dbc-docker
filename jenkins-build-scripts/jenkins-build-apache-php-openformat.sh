#!/usr/bin/env bash
set -e
echo "┌┬┐┌┐ ┌─┐  ┌─┐┌─┐┌─┐┌─┐┬ ┬┌─┐  ┌─┐┬ ┬┌─┐  ┌─┐┌─┐┌─┐┌┐┌┌─┐┌─┐┬─┐┌┬┐┌─┐┌┬┐";
echo " ││├┴┐│    ├─┤├─┘├─┤│  ├─┤├┤   ├─┘├─┤├─┘  │ │├─┘├┤ │││├┤ │ │├┬┘│││├─┤ │ ";
echo "─┴┘└─┘└─┘  ┴ ┴┴  ┴ ┴└─┘┴ ┴└─┘  ┴  ┴ ┴┴    └─┘┴  └─┘┘└┘└  └─┘┴└─┴ ┴┴ ┴ ┴ ";

# Build the dbc-apache-php-openformat
cd ./dbc-apache-php-openformat
docker build --no-cache -t dbc-apache-php-openformat .
docker tag -f dbc-apache-php-openformat docker-index:5000/dbc-apache-php-openformat

# Push the dbc-apache-php-openformat image
docker push docker-index:5000/dbc-apache-php-openformat

# Clean up the dbc-apache-php image
docker images | grep -- dbc-apache-php-openformat | awk '{print $1}'|xargs -r docker rmi

echo 'DBC Docker images in all, on this server:'
docker images --all | grep dbc | wc -l
