#!/usr/bin/env bash
set -e

# Pull latest glassfish image
docker pull docker-index:5000/java8
docker tag -f docker-index:5000/java8 dbc-java8

# Build the harvester
cd ./dbc-openmq
docker build --no-cache -t dbc-openmq .
docker tag -f dbc-openmq docker-index:5000/dbc-openmq

# Push the processor image
docker push docker-index:5000/dbc-openmq

# Clean up the harvester imnage
docker images | grep -- dbc-openmq | awk '{print $1}'|xargs -r docker rmi

# Clean up the glassfish image
docker images | grep -- dbc-java8 | awk '{print $1}'|xargs -r docker rmi

echo 'Docker images in all, on this server:'
docker images --all | wc -l

