#!/usr/bin/env bash
docker run --name dbc-glassfish-filestore --rm -v /cmdbdata:/tmp --env-file=./env-filestore-its.env -p 4850:4848 -p 8091:8080 --add-host "filestoredb jobstore jobstore":$(getip.sh dataio-be-s01) docker-index:5000/dbc-glassfish-filestore

