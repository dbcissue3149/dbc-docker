#!/usr/bin/env bash

psql -c "ALTER USER $POSTGRES_USER WITH SUPERUSER; CREATE extension IF NOT EXISTS plv8;"
