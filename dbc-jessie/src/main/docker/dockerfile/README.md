# debian-baseimage

Dockerfile for creating Debian Jessie baseimages


## Basic usage

```bash
$ make
$ make push
$ make clean
```


## Limitations

Currently only support for building 64 bit Jessie-images.
