#!/usr/bin/env bash

if [ -z "$ETCDCTL_ENDPOINT" ]; then
  CMDLINE=$(cat /proc/cmdline)
  for x in $CMDLINE; do
    if [[ "$x" == cmdb=* ]]; then
      export ETCDCTL_ENDPOINT=$(echo "$x" | sed "s/^cmdb=//")
    fi
  done
fi

exec /usr/bin/etcdctl "$@"
