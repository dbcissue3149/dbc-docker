What?
A Docker image containing an apache2 (2.4.10 - Debian) with php5, php5-yaz, php5-memcache, php5-oci, php5-curl (maybe more, check the Dockerfile) based on
 the image dbc-debian.
 
Customizations
* The index.html file has been overlayed with a dummy dbc html page.
* An phpinfo.php page is exposed.
* modified php.ini
    + with memory_limit set to 4G
    + always_populate_raw_post_data = On

Why?
This is general image and should be the foundation for new more specific apache-php-images.
oracle extension php5-oci8 is NOT installed as it is not used in the current setup, and DBC
wants to get away from oracle, so there is not reason to set it up (and it caused problems)

How?
Installs the latest apache2, php5 with modules.

Usage
Should not be used. Base new images on top of this. See dbc-apache- openformat-php
However
$ docker build -t yourname/dbc-apache-php .
$ docker run --name dbc-apache-php -p 80:80 yourname/dbc-apache-php
$ curl localhost:80/index.html
$ curl localhost:80/phpinfo.php

When?
This image was created 4/12-2015 user: LBABW (andreas@lundogbendsen.dk)

