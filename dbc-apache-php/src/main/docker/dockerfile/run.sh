#!/usr/bin/env bash
set -e

/usr/local/bin/confd -onetime -backend env

echo "Starting httpd"
exec apache2ctl -DFOREGROUND
