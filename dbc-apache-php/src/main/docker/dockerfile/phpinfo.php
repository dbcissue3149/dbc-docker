<h1>PHP Info</h1>
<?php
// Show all information, defaults to INFO_ALL
phpinfo();
?>
<h1>PHP Info Modules</h1>
<?php
// Show just the module information.
// phpinfo(8) yields identical results.
phpinfo(INFO_MODULES);
?>
<h1>PHP ini-values</h1>
<?php
print_r(ini_get_all());
?>
