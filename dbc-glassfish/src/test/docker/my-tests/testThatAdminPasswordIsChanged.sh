#!/usr/bin/env bash

# You have the following environment variables automatically at your disposal:
# Your own compose file (you have to write it)
# COMPOSE_FILE=$BASEDIR/src/main/docker/share/compose/$ARTIFACT_ID-compose.yml

# The compose files of the projects you depend on are placed here.
# SHARED_COMPOSE_FILE_DIR=$BASEDIR/src/resource/docker/compose

# The collection of shared compose files with -f parameter in front,
# so they are easy to use when calling compose.
# COMPOSE_DEPENDENCIES=-f a-compose-i-depend-upon.yml -f another-compose-i-depend-upon.yml

function be
{
    if [ "$1" == "quiet" ]; then
        set +x
    else [ "$1" == "loud" ];
        set -x
    fi
}
be loud

function fail
{
    if [ "$1" == "early" ]; then
        set -eo pipefail
    else [ "$1" == "late" ];
        set +eo pipefail
    fi

}
fail early

# cleanup is performed on all exits, and should keep the build machine clean. Extend it for your needs

trap defaultCleanup EXIT

# defaultStartup is a generic startup that should work for most containers out of the box. Extend it for your needs
function myStartup
{
    docker-compose -f $COMPOSE_FILE scale $ARTIFACT_ID=2
    docker-compose -f $COMPOSE_FILE $COMPOSE_DEPENDENCIES up -d
    CONTAINER_ID=$(docker-compose -f $COMPOSE_FILE $COMPOSE_DEPENDENCIES ps -q $ARTIFACT_ID)
}
myStartup

# defaultTest is a basic test that should work for most setups. Extend it for your needs
defaultTest

# If the default stuff fits you, keep it and add your own stuff below

# Examples. Check the functions, e.g. exitIfPostgresIsNotRunning.function for exact signature.
# exitIfPostgresIsNotRunning $CONTAINER_ID
# exitIfStringDoesNotAppearInLogs CONTAINER_ID SECONDS_TO_WAIT STRING_WE_ARE_LOOKING_FOR

# Examples. Check the functions, e.g. exitIfPostgresIsNotRunning.function for exact signature.
# exitIfPostgresIsNotRunning $CONTAINER_ID
# exitIfStringDoesNotAppearInLogs CONTAINER_ID SECONDS_TO_WAIT STRING_WE_ARE_LOOKING_FOR

exitIfStringDoesNotAppearInLogs $CONTAINER_ID 120 "Command enable-secure-admin executed successfully"
