#!/usr/bin/env bash

function be
{
    if [ "$1" == "quiet" ]; then
        set +x
    else [ "$1" == "loud" ];
        set -x
    fi
}
be loud

function fail
{
    if [ "$1" == "early" ]; then
        set -eo pipefail
    else [ "$1" == "late" ];
        set +eo pipefail
    fi

}
fail early

function cleanup
{
    echo Cleaning up
    if [ ! -z "$CONTAINER_ID" ]; then
        echo Stopping compose $COMPOSE_FILE $COMPOSE_DEPENDENCIES
        docker-compose -f $COMPOSE_FILE $COMPOSE_DEPENDENCIES down
    fi
}
trap cleanup EXIT

function myStartup
{
    docker-compose -f $COMPOSE_FILE scale $ARTIFACT_ID=5
    docker-compose -f $COMPOSE_FILE $COMPOSE_DEPENDENCIES up -d
    CONTAINER_ID=$(docker-compose -f $COMPOSE_FILE $COMPOSE_DEPENDENCIES ps -q $ARTIFACT_ID)
}
myStartup

defaultTest

#docker-compose -f $COMPOSE_FILE scale $ARTIFACT_ID=5
#docker-compose -f $COMPOSE_FILE up -d
#CONTAINER_ID=$(docker-compose -f $COMPOSE_FILE ps -q $ARTIFACT_ID)
#exitIfContainerDoesNotStart 10 $CONTAINER_ID
#docker-compose -f $COMPOSE_FILE scale $ARTIFACT_ID=5
#docker-compose -f $COMPOSE_FILE up -d
#CONTAINER_ID=$(docker-compose -f $COMPOSE_FILE ps -q $ARTIFACT_ID)
#exitIfContainerDoesNotStart 10 $CONTAINER_ID
exitIfStringDoesNotAppearInLogs $CONTAINER_ID 120 "Command enable-secure-admin executed successfully"
