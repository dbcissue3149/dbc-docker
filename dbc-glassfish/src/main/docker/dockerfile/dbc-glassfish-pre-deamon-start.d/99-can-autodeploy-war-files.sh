#!/usr/bin/env bash

set -e

# when the shell expands, dont include the pattern in the list of results (e.g. exclude *.xml)
shopt -s nullglob
for warfile in $GLASSFISH_HOME/glassfish/domains/domain1/autodeploy/*.war; do
	echo Marking $warfile for autodeployment by touching it    
    touch $warfile
    chown $GLASSFISH_USER:$GLASSFISH_USER $warfile
done
