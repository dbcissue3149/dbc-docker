#!/usr/bin/env bash

set -e

# when the shell expands, dont include the pattern in the list of results (e.g. exclude *.xml)
shopt -s nullglob
for earfile in $GLASSFISH_HOME/glassfish/domains/domain1/autodeploy/*.ear; do
	echo Marking $earfile for autodeployment by touching it    
    touch $earfile
    chown $GLASSFISH_USER:$GLASSFISH_USER $earfile
done
