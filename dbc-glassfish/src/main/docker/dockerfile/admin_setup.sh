#!/usr/bin/expect

set timeout -1
spawn ./asadmin 
expect "asadmin> " 
send "start-domain\n"
expect "asadmin> "
send "change-admin-password\n"
expect ">"
send "admin\n"
expect "password>"
send "\n"
expect "admin password>"
send "GoFish\n"
expect "again> "
send "GoFish\n"
expect ">"
send "y\n"
expect "asadmin> "
send "enable-secure-admin\n"
expect "in user name>"
send "admin\n"
expect "> "
send "GoFish\n"
expect "> "
send "restart-domain\n"
expect "asadmin> "
send "stop-domain\n"
expect ">"
send "quit\n"
expect eof

