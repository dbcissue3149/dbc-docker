#!/usr/bin/env bash
set -e

# when the shell expands, dont include the pattern in the list of results (e.g. exclude *.xml)
shopt -s nullglob

echo "=> Starting glassfish configuration in " $PWD
if [ -f $GLASSFISH_USER_HOME/.glassfish_has_been_configured ]; then
    echo "=> Glassfish 'admin' password already changed! Leaving it as it is"
else

	if [ -d /docker-entrypoint-confd.d ]; then
		for script in /docker-entrypoint-confd.d/*.sh; do
    		echo "===> Executing confd script " $script
			$script
		done
	fi

    echo "=> Glassfish has not been configured. Starting configuration chain in dbc-glassfish.d/"
    echo "==> Starting Glassfish server"
    asadmin start-domain
    
    # Run all scripts in dbc-glassfish.d/ dir
	if [ -d /home/gfish/dbc-glassfish.d ]; then
		for f in /home/gfish/dbc-glassfish.d/*.sh; do
    		echo "===> Executing pre script " $f 
			$f
		done
	fi
    
    touch $GLASSFISH_USER_HOME/.glassfish_has_been_configured
    
    echo "==> Stopping Glassfish server"
    asadmin stop-domain

	# Stuff that needs to be done when glassfish is not running.
	# Case : If autodeploy war is copied into glassfish when its running,
	#        glassfish may not be done deploying it when its stopped, so
	#        the autodeployment may be in an undefined state.
	#        To prevent this, we need to copy the war to autodeploy when 
	#        glassfish is stopped.
	if [ -d /home/gfish/dbc-glassfish-pre-deamon-start.d ]; then
		for f in /home/gfish/dbc-glassfish-pre-deamon-start.d/*.sh; do
    		echo "===> Executing pre-deamon-start script " $f 
			$f
		done
	fi
    
fi

exec asadmin start-domain --verbose

