#!/usr/bin/env bash

set -e

# when the shell expands, dont include the pattern in the list of results (e.g. exclude *.xml)
shopt -s nullglob
for cmdfile in ./dbc-glassfish.d/*.cmd; do
	echo "===> Feeding ["$cmdfile"] to asadmin" 
	asadmin --host localhost --port $GLASSFISH_ADMIN_PORT multimode  --file ./$cmdfile
done
