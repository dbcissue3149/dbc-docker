#!/usr/bin/env bash

# See 
#     http://docs.oracle.com/cd/E18930_01/html/821-2433/add-resources-1.html#SJSASEEREFMANadd-resources-1 
#     http://docs.oracle.com/cd/E18930_01/html/821-2416/gixps.html
#     http://docs.oracle.com/cd/E18930_01/html/821-2417/giyhw.html

set -e

echo "Version 12:56"
shopt -s nullglob
for resourcefile in ./dbc-glassfish.d/*.xml; do
	echo "===> Loading ["$resourcefile"] into glassfish"
	asadmin --host localhost --port $GLASSFISH_ADMIN_PORT --passwordfile ./passfile.txt add-resources $resourcefile  
done



