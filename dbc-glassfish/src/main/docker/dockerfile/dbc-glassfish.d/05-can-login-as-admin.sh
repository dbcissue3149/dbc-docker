#!/usr/bin/env bash

set -e

echo "=> Setting admin password to " $AS_ADMIN_PASSWORD
/home/gfish/change_admin_password_func.sh $AS_ADMIN_PASSWORD
/home/gfish/enable_secure_admin.sh $AS_ADMIN_PASSWORD

