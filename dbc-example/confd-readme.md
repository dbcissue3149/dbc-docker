# [confd]
A common task when dockerizing a component is injecting configuration.
confd provides a reasonably common and docker-friendly method for

- Getting (key,value) pairs (from environment variables, a cmdb, ...)
- Rendering configuration files using a template and the (key,value) pairs
- Launching, and optionally relaunching, an application

Using confd with your docker image usually a three-stage process:

1. In [src/main/docker/dockerfile/confd/conf.d/example.toml] describe each file you wish confd to render with
   at least its source template, destination file and values to look up in toml
   format (basically YAML).

   An example is included in [src/main/docker/dockerfile/confd/conf.d/example.toml]

2. In confd/templates/example.conf.tmpl include your actual template. The
   interpolation language is Go's text/template with a number of provided
   functions like "getv", documented the Github page.
   Given a key value-pair (/foo/bar/baz,quux), the template
   'Bar: {{getv "/foo/bar"}}'
   renders as
   'Bar: quux'

   An example is included in [src/main/docker/dockerfile/confd/templates/example.conf.tmpl]

3. Add the confd hierarchy to your docker image in /etc/ and wrap your
   executable in confd. For instance:
   `/usr/bin/myservice`
   becomes
   `/usr/local/bin/confd -onetime -backend env && /usr/bin/myservice`
   for onetime mode, utilizing values from environment variables for
   rendering. For other backends, for instance etcd, it's possible to run confd
   in daemon mode, polling the configuration service for changes and rerendering
   and restarting the service as necessary.
   
Confd buys heavily into the whole Docker/Golang ecosystem, so if in doubt,
assume those conventions are in place.

[confd]: https://github.com/kelseyhightower/confd
