#!/usr/bin/env bash

/usr/local/bin/confd -onetime -backend env

exec /usr/local/bin/myservice.sh
