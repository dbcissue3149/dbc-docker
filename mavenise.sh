#!/usr/bin/env bash

for project in $@ ;do
    echo Handling [$project]

    if [ ! -f $project-compose.yml ]; then
        echo "$project-compose.yml not found!"
        exit 1
    fi

    if [ -f "$project/pom.xml" ]; then
        echo "There is a pom.xml in $project. This indicates that the project has been mavenized. Exiting"
        exit 1
    fi


    mv $project tmp-$project

    mkdir -pv $project/src/main/docker/
    mv tmp-$project $project/src/main/docker/dockerfile

    mkdir -pv $project/src/main/docker/share/compose
    mv $project-compose.yml $project/src/main/docker/share/compose

    mkdir -pv $project/src/test/docker/my-tests
    cp dbc-example/src/test/docker/my-tests/test-example.sh $project/src/test/docker/my-tests/$project-default-tests.sh
    chmod +x $project/src/test/docker/my-tests/$project-default-tests.sh

    cp dbc-example/pom.xml $project

    # substitute $project dbc-new-image in pom.xml
    sed -i "s/dbc-new-image/$project/g" $project/pom.xml

    # add <module>$project</module> entry to dbc-docker/pom.xml
    sed -i "s:<modules>:<modules><module>$project</module>:g" pom.xml

done

