#!/usr/bin/env bash

set -e

# when the shell expands, dont include the pattern in the list of results (e_g_ exclude *_xml)
# shopt -s nullglob



WAR=holdings-items-update-webservice

env

mkdir ${GLASSFISH_USER_HOME}/${WAR}
unzip ${WAR}.war -d ${GLASSFISH_USER_HOME}/${WAR}
rm ${WAR}.war

cd ${GLASSFISH_USER_HOME}/${WAR}

cp ${GLASSFISH_USER_HOME}/02-ejb-jar.xml ${GLASSFISH_USER_HOME}/${WAR}/WEB-INF/ejb-jar.xml 
cp ${GLASSFISH_USER_HOME}/logback.xml ${GLASSFISH_USER_HOME}/${WAR}/WEB-INF/classes/logback.xml 

jar cmf META-INF/MANIFEST.MF ../${WAR}.war *
chown gfish:gfish  ../${WAR}.war
