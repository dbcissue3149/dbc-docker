--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

--
-- Name: archive_record(); Type: FUNCTION; Schema: public; Owner: rawrepo
--

CREATE FUNCTION archive_record() RETURNS trigger
    LANGUAGE plpgsql
    AS $$ -- V12
BEGIN
    INSERT INTO records_archive(bibliographicrecordid, agencyid, deleted, mimetype, content, created, modified, trackingId)
        VALUES(OLD.bibliographicrecordid, OLD.agencyid, OLD.deleted, OLD.mimetype, OLD.content, OLD.created, OLD.modified, OLD.trackingId);
    RETURN OLD;
END;
$$;


ALTER FUNCTION public.archive_record() OWNER TO rawrepo;

--
-- Name: archive_record_cleanup(); Type: FUNCTION; Schema: public; Owner: rawrepo
--

CREATE FUNCTION archive_record_cleanup() RETURNS trigger
    LANGUAGE plpgsql
    AS $$ -- V12
DECLARE
    ts TIMESTAMP;
BEGIN
    INSERT INTO records_archive(bibliographicrecordid, agencyid, deleted, mimetype, content, created, modified, trackingId)
        VALUES(OLD.bibliographicrecordid, OLD.agencyid, OLD.deleted, OLD.mimetype, OLD.content, OLD.created, OLD.modified, OLD.trackingId);
    FOR ts IN
        SELECT modified FROM records_archive WHERE bibliographicrecordid=OLD.bibliographicrecordid AND agencyid=OLD.agencyid AND
                                                   modified <=  NEW.modified - INTERVAL '42 DAYS' ORDER BY modified DESC OFFSET 1 LIMIT 1
    LOOP
        DELETE FROM records_archive WHERE bibliographicrecordid=OLD.bibliographicrecordid AND agencyid=OLD.agencyid AND modified<=ts;
    END LOOP;
    RETURN OLD;
END;
$$;


ALTER FUNCTION public.archive_record_cleanup() OWNER TO rawrepo;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: queue; Type: TABLE; Schema: public; Owner: rawrepo; Tablespace: 
--

CREATE TABLE queue (
    bibliographicrecordid character varying(64) NOT NULL,
    agencyid numeric(6,0) NOT NULL,
    worker character varying(32) NOT NULL,
    queued timestamp without time zone DEFAULT (timeofday())::timestamp without time zone NOT NULL
);


ALTER TABLE queue OWNER TO rawrepo;

--
-- Name: dequeue(character varying); Type: FUNCTION; Schema: public; Owner: rawrepo
--

CREATE FUNCTION dequeue(worker_ character varying) RETURNS SETOF queue
    LANGUAGE plpgsql
    AS $$ -- V8
DECLARE
 row queue;
 upd queue;
BEGIN
    <<done>>
    -- LIMIT xxx CUT OFF TO LARGE DATA SETS, SHOULD BE > MAX WORKERTHREADS
    FOR row IN SELECT * FROM queue WHERE worker=worker_ ORDER BY queued LIMIT 256 LOOP
        BEGIN
	    -- IF FIRST WITH THIS row.job IS TAKEN NONE WILL BE SELECTED
	    -- EVEN IF AN IDENTICAL IS LATER IN THE QUEUE
	    -- NO 2 WORKERS CAN RUN THE SAME JOB AT THE SAME TIME
            FOR upd in SELECT * FROM queue WHERE bibliographicrecordid=row.bibliographicrecordid AND agencyid=row.agencyid AND worker=worker_ FOR UPDATE NOWAIT LOOP
                DELETE FROM queue WHERE bibliographicrecordid=row.bibliographicrecordid AND agencyid=row.agencyid AND worker=worker_;
                RETURN NEXT row;
                EXIT done; -- We got one - exit
            END LOOP;
        EXCEPTION
            WHEN lock_not_available THEN
        END;
    END LOOP;
END
$$;


ALTER FUNCTION public.dequeue(worker_ character varying) OWNER TO rawrepo;

--
-- Name: dequeue(character varying, integer); Type: FUNCTION; Schema: public; Owner: rawrepo
--

CREATE FUNCTION dequeue(worker_ character varying, no_ integer) RETURNS SETOF queue
    LANGUAGE plpgsql
    AS $$ -- V8
DECLARE
 row queue;
 upd queue;
 no int = 0;
BEGIN
    <<done>>
    -- LIMIT xxx CUT OFF TO LARGE DATA SETS, SHOULD BE > MAX WORKERTHREADS
    FOR row IN SELECT * FROM queue WHERE worker=worker_ ORDER BY queued LOOP
        BEGIN
	    -- IF FIRST WITH THIS row.job IS TAKEN NONE WILL BE SELECTED
	    -- EVEN IF AN IDENTICAL IS LATER IN THE QUEUE
	    -- NO 2 WORKERS CAN RUN THE SAME JOB AT THE SAME TIME
            FOR upd in SELECT * FROM queue WHERE bibliographicrecordid=row.bibliographicrecordid AND agencyid=row.agencyid AND worker=worker_ FOR UPDATE NOWAIT LOOP
                DELETE FROM queue WHERE bibliographicrecordid=row.bibliographicrecordid AND agencyid=row.agencyid AND worker=worker_;
                RETURN NEXT row;
                no = no + 1;
                if no >= no_ THEN
                    EXIT done; -- We got one - exit
                END IF;
            END LOOP;
        EXCEPTION
            WHEN lock_not_available THEN
        END;
    END LOOP;
END
$$;


ALTER FUNCTION public.dequeue(worker_ character varying, no_ integer) OWNER TO rawrepo;

--
-- Name: enqueue(character varying, numeric, character varying, character varying, character, character); Type: FUNCTION; Schema: public; Owner: rawrepo
--

CREATE FUNCTION enqueue(bibliographicrecordid_ character varying, agencyid_ numeric, mimetype_ character varying, provider_ character varying, changed_ character, leaf_ character) RETURNS SETOF character varying
    LANGUAGE plpgsql
    AS $$ -- V3,V8
DECLARE
    row queuerules;
    exists queue;
    rows int;
BEGIN
    FOR row IN SELECT * FROM queuerules WHERE provider=provider_ AND (mimetype='' OR mimetype=mimetype_) AND (changed='A' OR changed=changed_) AND (leaf='A' OR leaf=leaf_) LOOP
    	-- RAISE NOTICE 'worker=%', row.worker;
	SELECT COUNT(*) INTO rows FROM queue WHERE bibliographicrecordid=bibliographicrecordid_ AND agencyid=agencyid_ AND worker=row.worker;
	-- RAISE NOTICE 'rows=%', rows;
	CASE
	    WHEN rows = 0 THEN -- none is queued
	        INSERT INTO queue(bibliographicrecordid, agencyid, worker) VALUES(bibliographicrecordid_, agencyid_, row.worker);
	    WHEN rows = 1 THEN -- one is queued - but may be locked by a worker
	    	BEGIN
		    SELECT * INTO exists FROM queue WHERE bibliographicrecordid=bibliographicrecordid_ AND agencyid=agencyid_ AND worker=row.worker FOR UPDATE NOWAIT;
                    -- By locking the row, we ensure that no worker can take this row until we commit / rollback
                    -- Ensuring that even if this job is next, it will not be processed until we're sure our data is used.
		EXCEPTION
	    	    WHEN lock_not_available THEN
                        INSERT INTO queue(bibliographicrecordid, agencyid, worker) VALUES(bibliographicrecordid_, agencyid_, row.worker);
		END;
	    ELSE
	        -- nothing
	END CASE;
    END LOOP;
END
$$;


ALTER FUNCTION public.enqueue(bibliographicrecordid_ character varying, agencyid_ numeric, mimetype_ character varying, provider_ character varying, changed_ character, leaf_ character) OWNER TO rawrepo;

--
-- Name: relation_immutable_false(); Type: FUNCTION; Schema: public; Owner: rawrepo
--

CREATE FUNCTION relation_immutable_false() RETURNS trigger
    LANGUAGE plpgsql
    AS $$ -- V10
BEGIN
    NEW.always_false = false;
    RETURN NEW;
END;
$$;


ALTER FUNCTION public.relation_immutable_false() OWNER TO rawrepo;

--
-- Name: jobdiag; Type: TABLE; Schema: public; Owner: rawrepo; Tablespace: 
--

CREATE TABLE jobdiag (
    bibliographicrecordid character varying(64) NOT NULL,
    agencyid numeric(6,0) NOT NULL,
    worker character varying(32) NOT NULL,
    error character varying(128) NOT NULL,
    queued timestamp without time zone NOT NULL
);


ALTER TABLE jobdiag OWNER TO rawrepo;

--
-- Name: queuerules; Type: TABLE; Schema: public; Owner: rawrepo; Tablespace: 
--

CREATE TABLE queuerules (
    provider character varying(32) NOT NULL,
    worker character varying(32) NOT NULL,
    mimetype character varying(128) DEFAULT ''::character varying NOT NULL,
    changed character(1) NOT NULL,
    leaf character(1) NOT NULL
);


ALTER TABLE queuerules OWNER TO rawrepo;

--
-- Name: queueworkers; Type: TABLE; Schema: public; Owner: rawrepo; Tablespace: 
--

CREATE TABLE queueworkers (
    worker character varying(32) NOT NULL
);


ALTER TABLE queueworkers OWNER TO rawrepo;

--
-- Name: records; Type: TABLE; Schema: public; Owner: rawrepo; Tablespace: 
--

CREATE TABLE records (
    bibliographicrecordid character varying(64) NOT NULL,
    agencyid numeric(6,0) NOT NULL,
    deleted boolean DEFAULT false NOT NULL,
    mimetype character varying(128) DEFAULT 'text/marcxchange'::character varying NOT NULL,
    content text,
    created timestamp without time zone NOT NULL,
    modified timestamp without time zone NOT NULL,
    trackingid character varying(256) DEFAULT ''::character varying NOT NULL
);


ALTER TABLE records OWNER TO rawrepo;

--
-- Name: records_archive; Type: TABLE; Schema: public; Owner: rawrepo; Tablespace: 
--

CREATE TABLE records_archive (
    bibliographicrecordid character varying(64) NOT NULL,
    agencyid numeric(6,0) NOT NULL,
    deleted boolean DEFAULT false NOT NULL,
    mimetype character varying(128) DEFAULT 'text/marcxchange'::character varying NOT NULL,
    content text,
    created timestamp without time zone NOT NULL,
    modified timestamp without time zone NOT NULL,
    trackingid character varying(256) DEFAULT ''::character varying NOT NULL
);


ALTER TABLE records_archive OWNER TO rawrepo;

--
-- Name: relations; Type: TABLE; Schema: public; Owner: rawrepo; Tablespace: 
--

CREATE TABLE relations (
    bibliographicrecordid character varying(64) NOT NULL,
    agencyid numeric(6,0) NOT NULL,
    refer_bibliographicrecordid character varying(64) NOT NULL,
    refer_agencyid numeric(6,0) NOT NULL,
    always_false boolean DEFAULT false NOT NULL
);


ALTER TABLE relations OWNER TO rawrepo;

--
-- Name: version; Type: TABLE; Schema: public; Owner: rawrepo; Tablespace: 
--

CREATE TABLE version (
    version numeric(6,0) NOT NULL,
    warning text
);


ALTER TABLE version OWNER TO rawrepo;

--
-- Data for Name: jobdiag; Type: TABLE DATA; Schema: public; Owner: rawrepo
--

COPY jobdiag (bibliographicrecordid, agencyid, worker, error, queued) FROM stdin;
20611529	191919	solr-sync	20611529	2016-03-10 10:10:42.896805
20611529	870970	solr-sync	20611529	2016-03-10 10:10:43.093077
\.


--
-- Data for Name: queue; Type: TABLE DATA; Schema: public; Owner: rawrepo
--

COPY queue (bibliographicrecordid, agencyid, worker, queued) FROM stdin;
20611529	191919	basis-decentral	2016-03-10 10:10:42.8959
20611529	191919	broend-sync	2016-03-10 10:10:42.896584
20611529	870970	basis-decentral	2016-03-10 10:10:43.092487
20611529	870970	broend-sync	2016-03-10 10:10:43.09281
\.


--
-- Data for Name: queuerules; Type: TABLE DATA; Schema: public; Owner: rawrepo
--

COPY queuerules (provider, worker, mimetype, changed, leaf) FROM stdin;
opencataloging-update	broend-sync		A	Y
opencataloging-update	solr-sync		Y	A
opencataloging-update	basis-decentral		Y	A
bulk-broend	broend-sync		A	Y
bulk-solr	solr-sync-bulk		Y	A
agency-delete	broend-sync		A	Y
agency-delete	solr-sync		Y	A
agency-maintain	broend-sync		A	Y
agency-maintain	solr-sync		Y	A
fbs-update	broend-sync		A	Y
fbs-update	solr-sync		Y	A
fbs-update	basis-decentral		Y	A
dataio-update	broend-sync		A	Y
dataio-update	solr-sync		Y	A
\.


--
-- Data for Name: queueworkers; Type: TABLE DATA; Schema: public; Owner: rawrepo
--

COPY queueworkers (worker) FROM stdin;
broend-sync
solr-sync
basis-decentral
solr-sync-bulk
\.


--
-- Data for Name: records; Type: TABLE DATA; Schema: public; Owner: rawrepo
--

COPY records (bibliographicrecordid, agencyid, deleted, mimetype, content, created, modified, trackingid) FROM stdin;
20611529	191919	f	text/marcxchange	PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9InllcyI/PjxyZWNvcmQgeHNpOnNjaGVtYUxvY2F0aW9uPSJodHRwOi8vd3d3LmxvYy5nb3Yvc3RhbmRhcmRzL2lzbzI1NTc3L21hcmN4Y2hhbmdlLTEtMS54c2QiIHhtbG5zPSJpbmZvOmxjL3htbG5zL21hcmN4Y2hhbmdlLXYxIiB4bWxuczp4c2k9Imh0dHA6Ly93d3cudzMub3JnLzIwMDEvWE1MU2NoZW1hLWluc3RhbmNlIj48bGVhZGVyPjAwMDAwbiAgICAyMjAwMDAwICAgNDUwMDwvbGVhZGVyPjxkYXRhZmllbGQgdGFnPSIwMDEiIGluZDE9IjAiIGluZDI9IjAiPjxzdWJmaWVsZCBjb2RlPSJhIj4yMDYxMTUyOTwvc3ViZmllbGQ+PHN1YmZpZWxkIGNvZGU9ImIiPjE5MTkxOTwvc3ViZmllbGQ+PHN1YmZpZWxkIGNvZGU9ImMiPjIwMTYwMzEwMTAxMDQxPC9zdWJmaWVsZD48c3ViZmllbGQgY29kZT0iZCI+MTk5NDA1MTY8L3N1YmZpZWxkPjxzdWJmaWVsZCBjb2RlPSJmIj5hPC9zdWJmaWVsZD48L2RhdGFmaWVsZD48ZGF0YWZpZWxkIHRhZz0iMDA0IiBpbmQxPSIwIiBpbmQyPSIwIj48c3ViZmllbGQgY29kZT0iciI+bjwvc3ViZmllbGQ+PHN1YmZpZWxkIGNvZGU9ImEiPmU8L3N1YmZpZWxkPjwvZGF0YWZpZWxkPjxkYXRhZmllbGQgdGFnPSIwMDgiIGluZDE9IjAiIGluZDI9IjAiPjxzdWJmaWVsZCBjb2RlPSJ0Ij5tPC9zdWJmaWVsZD48c3ViZmllbGQgY29kZT0idSI+cjwvc3ViZmllbGQ+PHN1YmZpZWxkIGNvZGU9ImEiPjE5OTQ8L3N1YmZpZWxkPjxzdWJmaWVsZCBjb2RlPSJ2Ij4wPC9zdWJmaWVsZD48c3ViZmllbGQgY29kZT0ieiI+MTk5Nzwvc3ViZmllbGQ+PHN1YmZpZWxkIGNvZGU9ImIiPmRrPC9zdWJmaWVsZD48c3ViZmllbGQgY29kZT0iaiI+Zjwvc3ViZmllbGQ+PHN1YmZpZWxkIGNvZGU9ImwiPmRhbjwvc3ViZmllbGQ+PC9kYXRhZmllbGQ+PGRhdGFmaWVsZCB0YWc9IjAwOSIgaW5kMT0iMCIgaW5kMj0iMCI+PHN1YmZpZWxkIGNvZGU9ImEiPmE8L3N1YmZpZWxkPjxzdWJmaWVsZCBjb2RlPSJnIj54eDwvc3ViZmllbGQ+PC9kYXRhZmllbGQ+PGRhdGFmaWVsZCB0YWc9IjAyMSIgaW5kMT0iMCIgaW5kMj0iMCI+PHN1YmZpZWxkIGNvZGU9ImEiPjg3LTc3MTQtMTU2LTM8L3N1YmZpZWxkPjxzdWJmaWVsZCBjb2RlPSJjIj5oZi48L3N1YmZpZWxkPjxzdWJmaWVsZCBjb2RlPSJkIj5rci4gNzgsMDA8L3N1YmZpZWxkPjwvZGF0YWZpZWxkPjxkYXRhZmllbGQgdGFnPSIwMzIiIGluZDE9IjAiIGluZDI9IjAiPjxzdWJmaWVsZCBjb2RlPSJhIj5EQkYxOTk3NDY8L3N1YmZpZWxkPjxzdWJmaWVsZCBjb2RlPSJ4Ij5TRkQxOTk3NDY8L3N1YmZpZWxkPjwvZGF0YWZpZWxkPjxkYXRhZmllbGQgdGFnPSIwNDEiIGluZDE9IjAiIGluZDI9IjAiPjxzdWJmaWVsZCBjb2RlPSJhIj5kYW48L3N1YmZpZWxkPjxzdWJmaWVsZCBjb2RlPSJjIj5lbmc8L3N1YmZpZWxkPjwvZGF0YWZpZWxkPjxkYXRhZmllbGQgdGFnPSIxMDAiIGluZDE9IjAiIGluZDI9IjAiPjxzdWJmaWVsZCBjb2RlPSJhIj5Lb29udHo8L3N1YmZpZWxkPjxzdWJmaWVsZCBjb2RlPSJoIj5EZWFuIFIuPC9zdWJmaWVsZD48L2RhdGFmaWVsZD48ZGF0YWZpZWxkIHRhZz0iMjQxIiBpbmQxPSIwIiBpbmQyPSIwIj48c3ViZmllbGQgY29kZT0iYSI+VGhlIMKkZG9vciB0byBEZWNlbWJlcjwvc3ViZmllbGQ+PC9kYXRhZmllbGQ+PGRhdGFmaWVsZCB0YWc9IjI0NSIgaW5kMT0iMCIgaW5kMj0iMCI+PHN1YmZpZWxkIGNvZGU9ImEiPkTDuHJlbiB0aWwgZGVjZW1iZXI8L3N1YmZpZWxkPjxzdWJmaWVsZCBjb2RlPSJlIj5EZWFuIEtvb250ejwvc3ViZmllbGQ+PHN1YmZpZWxkIGNvZGU9ImYiPm92ZXJzYXQgZnJhIGFtZXJpa2Fuc2sgYWYKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIEluZ2VyIFZlZGVyc8O4PC9zdWJmaWVsZD48L2RhdGFmaWVsZD48ZGF0YWZpZWxkIHRhZz0iMjUwIiBpbmQxPSIwIiBpbmQyPSIwIj48c3ViZmllbGQgY29kZT0iYSI+Mi4gZGFuc2tlIHVkZ2F2ZTwvc3ViZmllbGQ+PHN1YmZpZWxkIGNvZGU9IngiPm55dCBvcGxhZzwvc3ViZmllbGQ+PHN1YmZpZWxkIGNvZGU9ImIiPjIuIHVkZ2F2ZTwvc3ViZmllbGQ+PC9kYXRhZmllbGQ+PGRhdGFmaWVsZCB0YWc9IjI2MCIgaW5kMT0iMCIgaW5kMj0iMCI+PHN1YmZpZWxkIGNvZGU9IiZhbXA7Ij4xPC9zdWJmaWVsZD48c3ViZmllbGQgY29kZT0iYSI+W0tiaC5dPC9zdWJmaWVsZD48c3ViZmllbGQgY29kZT0iYiI+Q2ljZXJvPC9zdWJmaWVsZD48c3ViZmllbGQgY29kZT0iYyI+MTk5Nzwvc3ViZmllbGQ+PHN1YmZpZWxkIGNvZGU9ImsiPk7DuHJoYXZlbiBSb3RhdGlvbiwgVmlib3JnPC9zdWJmaWVsZD48L2RhdGFmaWVsZD48ZGF0YWZpZWxkIHRhZz0iMzAwIiBpbmQxPSIwIiBpbmQyPSIwIj48c3ViZmllbGQgY29kZT0iYSI+MzMxIHNpZGVyPC9zdWJmaWVsZD48L2RhdGFmaWVsZD48ZGF0YWZpZWxkIHRhZz0iNTA0IiBpbmQxPSIwIiBpbmQyPSIwIj48c3ViZmllbGQgY29kZT0iJmFtcDsiPjE8L3N1YmZpZWxkPjxzdWJmaWVsZCBjb2RlPSJhIj5JIHZpZGVuc2thYmVucyBoZWxsaWdlIG5hdm4KICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHVkc8OmdHRlciBlbiBzaW5kc3N5ZyBsw6ZnZSBzaW4gbGlsbGUgZGF0dGVyIGZvciBzw6UgaMOlcnJlanNlbmRlCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBla3NwZXJpbWVudGVyLCBhdCBvbmRlIG9nIHVrZW5kdGUga3LDpmZ0ZXIgZ3JhZHZpc3QgdGFnZXIKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG1hZ3RlbiBvdmVyIGhlbmRlPC9zdWJmaWVsZD48L2RhdGFmaWVsZD48ZGF0YWZpZWxkIHRhZz0iNTIwIiBpbmQxPSIwIiBpbmQyPSIwIj48c3ViZmllbGQgY29kZT0iYSI+VGlkbGlnZXJlOiAyLiB1ZGdhdmUgMTk5NC4KICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC0gMS4gdWRnYXZlLiAxOTkzPC9zdWJmaWVsZD48L2RhdGFmaWVsZD48ZGF0YWZpZWxkIHRhZz0iNTIxIiBpbmQxPSIwIiBpbmQyPSIwIj48c3ViZmllbGQgY29kZT0iYSI+Mi4gZGFuc2tlIHVkZ2F2ZTwvc3ViZmllbGQ+PHN1YmZpZWxkIGNvZGU9ImMiPjE5OTQ8L3N1YmZpZWxkPjwvZGF0YWZpZWxkPjxkYXRhZmllbGQgdGFnPSI1MjEiIGluZDE9IjAiIGluZDI9IjAiPjxzdWJmaWVsZCBjb2RlPSJiIj5ueXQgb3BsYWc8L3N1YmZpZWxkPjxzdWJmaWVsZCBjb2RlPSJjIj4xOTk3PC9zdWJmaWVsZD48L2RhdGFmaWVsZD48ZGF0YWZpZWxkIHRhZz0iNjUyIiBpbmQxPSIwIiBpbmQyPSIwIj48c3ViZmllbGQgY29kZT0ibiI+ODMuODwvc3ViZmllbGQ+PHN1YmZpZWxkIGNvZGU9InoiPjI2PC9zdWJmaWVsZD48L2RhdGFmaWVsZD48ZGF0YWZpZWxkIHRhZz0iNjUyIiBpbmQxPSIwIiBpbmQyPSIwIj48c3ViZmllbGQgY29kZT0ibyI+c2s8L3N1YmZpZWxkPjwvZGF0YWZpZWxkPjxkYXRhZmllbGQgdGFnPSI2NjYiIGluZDE9IjAiIGluZDI9IjAiPjxzdWJmaWVsZCBjb2RlPSIwIj48L3N1YmZpZWxkPjxzdWJmaWVsZCBjb2RlPSJzIj5neXM8L3N1YmZpZWxkPjwvZGF0YWZpZWxkPjxkYXRhZmllbGQgdGFnPSI2NjYiIGluZDE9IjAiIGluZDI9IjAiPjxzdWJmaWVsZCBjb2RlPSIwIj48L3N1YmZpZWxkPjxzdWJmaWVsZCBjb2RlPSJzIj5wc3lraXNrIHN5Z2U8L3N1YmZpZWxkPjwvZGF0YWZpZWxkPjxkYXRhZmllbGQgdGFnPSI5OTYiIGluZDE9IjAiIGluZDI9IjAiPjxzdWJmaWVsZCBjb2RlPSJhIj43MTQ3MDA8L3N1YmZpZWxkPjwvZGF0YWZpZWxkPjwvcmVjb3JkPg==	2016-03-10 10:10:42.6	2016-03-10 10:10:42.621	87f47671-500b-453e-b89a-62ef3d375390-{20611529:191919}
20611529	870970	f	text/enrichment+marcxchange	PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9InllcyI/PjxyZWNvcmQgeHNpOnNjaGVtYUxvY2F0aW9uPSJodHRwOi8vd3d3LmxvYy5nb3Yvc3RhbmRhcmRzL2lzbzI1NTc3L21hcmN4Y2hhbmdlLTEtMS54c2QiIHhtbG5zPSJpbmZvOmxjL3htbG5zL21hcmN4Y2hhbmdlLXYxIiB4bWxuczp4c2k9Imh0dHA6Ly93d3cudzMub3JnLzIwMDEvWE1MU2NoZW1hLWluc3RhbmNlIj48bGVhZGVyPjAwMDAwbiAgICAyMjAwMDAwICAgNDUwMDwvbGVhZGVyPjxkYXRhZmllbGQgdGFnPSIwMDEiIGluZDE9IjAiIGluZDI9IjAiPjxzdWJmaWVsZCBjb2RlPSJhIj4yMDYxMTUyOTwvc3ViZmllbGQ+PHN1YmZpZWxkIGNvZGU9ImIiPjg3MDk3MDwvc3ViZmllbGQ+PHN1YmZpZWxkIGNvZGU9ImMiPjIwMTYwMzEwMTAxMDQxPC9zdWJmaWVsZD48c3ViZmllbGQgY29kZT0iZCI+MTk5NDA1MTY8L3N1YmZpZWxkPjxzdWJmaWVsZCBjb2RlPSJmIj5hPC9zdWJmaWVsZD48L2RhdGFmaWVsZD48ZGF0YWZpZWxkIHRhZz0iMDA0IiBpbmQxPSIwIiBpbmQyPSIwIj48c3ViZmllbGQgY29kZT0iciI+bjwvc3ViZmllbGQ+PHN1YmZpZWxkIGNvZGU9ImEiPmU8L3N1YmZpZWxkPjwvZGF0YWZpZWxkPjwvcmVjb3JkPg==	2016-03-10 10:10:43.045	2016-03-10 10:10:43.057	87f47671-500b-453e-b89a-62ef3d375390-{20611529:870970}
\.


--
-- Data for Name: records_archive; Type: TABLE DATA; Schema: public; Owner: rawrepo
--

COPY records_archive (bibliographicrecordid, agencyid, deleted, mimetype, content, created, modified, trackingid) FROM stdin;
\.


--
-- Data for Name: relations; Type: TABLE DATA; Schema: public; Owner: rawrepo
--

COPY relations (bibliographicrecordid, agencyid, refer_bibliographicrecordid, refer_agencyid, always_false) FROM stdin;
20611529	870970	20611529	191919	f
\.


--
-- Data for Name: version; Type: TABLE DATA; Schema: public; Owner: rawrepo
--

COPY version (version, warning) FROM stdin;
7	\N
8	\N
9	\N
10	\N
11	\N
12	\N
13	\N
14	\N
\.


--
-- Name: queuerules_pk; Type: CONSTRAINT; Schema: public; Owner: rawrepo; Tablespace: 
--

ALTER TABLE ONLY queuerules
    ADD CONSTRAINT queuerules_pk PRIMARY KEY (provider, worker, changed, leaf);


--
-- Name: queueworkers_pk; Type: CONSTRAINT; Schema: public; Owner: rawrepo; Tablespace: 
--

ALTER TABLE ONLY queueworkers
    ADD CONSTRAINT queueworkers_pk PRIMARY KEY (worker);


--
-- Name: records_pk; Type: CONSTRAINT; Schema: public; Owner: rawrepo; Tablespace: 
--

ALTER TABLE ONLY records
    ADD CONSTRAINT records_pk PRIMARY KEY (bibliographicrecordid, agencyid);


--
-- Name: relations_pk; Type: CONSTRAINT; Schema: public; Owner: rawrepo; Tablespace: 
--

ALTER TABLE ONLY relations
    ADD CONSTRAINT relations_pk PRIMARY KEY (bibliographicrecordid, agencyid, refer_bibliographicrecordid, refer_agencyid);


--
-- Name: version_pkey; Type: CONSTRAINT; Schema: public; Owner: rawrepo; Tablespace: 
--

ALTER TABLE ONLY version
    ADD CONSTRAINT version_pkey PRIMARY KEY (version);


--
-- Name: jobdiag_idx; Type: INDEX; Schema: public; Owner: rawrepo; Tablespace: 
--

CREATE INDEX jobdiag_idx ON jobdiag USING btree (worker, error, queued);


--
-- Name: queue_idx_job; Type: INDEX; Schema: public; Owner: rawrepo; Tablespace: 
--

CREATE INDEX queue_idx_job ON queue USING btree (bibliographicrecordid, agencyid, worker);


--
-- Name: queue_idx_worker; Type: INDEX; Schema: public; Owner: rawrepo; Tablespace: 
--

CREATE INDEX queue_idx_worker ON queue USING btree (worker, queued);


--
-- Name: records_agencyid; Type: INDEX; Schema: public; Owner: rawrepo; Tablespace: 
--

CREATE INDEX records_agencyid ON records USING btree (agencyid);


--
-- Name: records_archive_id; Type: INDEX; Schema: public; Owner: rawrepo; Tablespace: 
--

CREATE INDEX records_archive_id ON records_archive USING btree (bibliographicrecordid, agencyid);


--
-- Name: records_archive_modified; Type: INDEX; Schema: public; Owner: rawrepo; Tablespace: 
--

CREATE INDEX records_archive_modified ON records_archive USING btree (modified);


--
-- Name: records_archive_pk; Type: INDEX; Schema: public; Owner: rawrepo; Tablespace: 
--

CREATE INDEX records_archive_pk ON records_archive USING btree (bibliographicrecordid, agencyid, modified);


--
-- Name: records_relation_id; Type: INDEX; Schema: public; Owner: rawrepo; Tablespace: 
--

CREATE UNIQUE INDEX records_relation_id ON records USING btree (bibliographicrecordid, agencyid, deleted);


--
-- Name: relations_reverse; Type: INDEX; Schema: public; Owner: rawrepo; Tablespace: 
--

CREATE INDEX relations_reverse ON relations USING btree (refer_bibliographicrecordid, refer_agencyid);


--
-- Name: archive_record_delete; Type: TRIGGER; Schema: public; Owner: rawrepo
--

CREATE TRIGGER archive_record_delete AFTER DELETE ON records FOR EACH ROW EXECUTE PROCEDURE archive_record();


--
-- Name: archive_record_update; Type: TRIGGER; Schema: public; Owner: rawrepo
--

CREATE TRIGGER archive_record_update AFTER UPDATE ON records FOR EACH ROW WHEN ((old.* IS DISTINCT FROM new.*)) EXECUTE PROCEDURE archive_record_cleanup();


--
-- Name: relation_immutable_false_insert; Type: TRIGGER; Schema: public; Owner: rawrepo
--

CREATE TRIGGER relation_immutable_false_insert BEFORE INSERT ON relations FOR EACH ROW EXECUTE PROCEDURE relation_immutable_false();


--
-- Name: relation_immutable_false_update; Type: TRIGGER; Schema: public; Owner: rawrepo
--

CREATE TRIGGER relation_immutable_false_update BEFORE UPDATE ON relations FOR EACH ROW WHEN ((old.* IS DISTINCT FROM new.*)) EXECUTE PROCEDURE relation_immutable_false();

--
-- Name: queue_fk_worker; Type: FK CONSTRAINT; Schema: public; Owner: rawrepo
--

ALTER TABLE ONLY queue
    ADD CONSTRAINT queue_fk_worker FOREIGN KEY (worker) REFERENCES queueworkers(worker);


--
-- Name: queuerules_fk_worker; Type: FK CONSTRAINT; Schema: public; Owner: rawrepo
--

ALTER TABLE ONLY queuerules
    ADD CONSTRAINT queuerules_fk_worker FOREIGN KEY (worker) REFERENCES queueworkers(worker);


--
-- Name: relations_fk_owner; Type: FK CONSTRAINT; Schema: public; Owner: rawrepo
--

ALTER TABLE ONLY relations
    ADD CONSTRAINT relations_fk_owner FOREIGN KEY (bibliographicrecordid, agencyid, always_false) REFERENCES records(bibliographicrecordid, agencyid, deleted);


--
-- Name: relations_fk_refer; Type: FK CONSTRAINT; Schema: public; Owner: rawrepo
--

ALTER TABLE ONLY relations
    ADD CONSTRAINT relations_fk_refer FOREIGN KEY (refer_bibliographicrecordid, refer_agencyid, always_false) REFERENCES records(bibliographicrecordid, agencyid, deleted);


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--
