#!/usr/bin/env bash

set -e

function box_out()
{
  local s="$*"
  tput setaf 3
  echo " -${s//?/-}-
| ${s//?/ } |
| $(tput setaf 4)$s$(tput setaf 3) |
| ${s//?/ } |
 -${s//?/-}-"
  tput sgr 0
}

function showHelp {
	echo " ---- USAGE ----"
	echo $0 -f file.yml" Cleans and builds everything to make sure a composition is run on the latest images"
	echo $0 -f file.yml" -n|--no-build stop and destroy existing docker-compose, but do not rebuild images"
}

if [[ $# -eq 0 ]] ; then
	showHelp
    exit 0
fi

# Defaults
BUILD=true

while [[ $# > 1 ]]
do
	key="$1"
	echo KEY is $key
	
	case $key in 
		-n|--no-build)
	    BUILD=false
	    shift # past argument
	    ;;
		-f|--file)
	    COMPOSE=$2
	    shift # past argument
	    ;;
		-h|--help)
		showHelp
		exit 0
	    shift # past argument
	    ;;
	    *)
	    echo "Unknown option $2"
	    exit 1
	    ;;
	esac
done

if [ -z "$COMPOSE" ] ; then
	echo Cant do work without a compose yaml file. Tell me where it is with -f file.yml 
	exit 1
fi	    

box_out Compose safe : $COMPOSE

docker-compose -f $COMPOSE rm --force
if [ "$BUILD" = true ] ; then
	./build-all.sh && \
	docker-compose -f $COMPOSE build
fi
docker-compose -f $COMPOSE up

exit 0