# Common docker stuff at DBC #

# Contributors
* Jesper Bruun Rasmussen <jbr@dbc.dk>
* Sarah Brofeldt <sab@dbc.dk>
* Jesper Wermuth <wermuth@lundogbendsen.dk>

# Introduktion
Dette projekt gør det muligt at få Docker images udviklet forskellige steder på DBC til at fungere sammen uden at kende
til detaljerne omkring de enkelte projekter.

Projektet gør det muligt at starte vilkårlige sammensætning af images, f.eks. dataio og teste dem i sammenhæng,
igen uden at kende detaljerne i de enkelte images.

Projektet er styret af maven, hviklet i sidste ende gør det muligt at teste store sammensætninger
(f.eks. hele dataio og hive sammen) i et enkelt og simpelt jenkins job
```
    mvn install
```

# Maven

* Vi bruger maven til at bygge alle projekter med transitive afhængigheder, derunder docker images.
* Vi har implementeret et test-system i maven, der gør det muligt for udviklere (dig?) at skrive
standardiserede tests, som kan afvikles af dig selv ved at skrive mvn install, og som afvikles af
Jenkins på samme måde. Det er altså et krav at dit projekt bygger med mvn install

Maven setup er baseret på konventioner, som _skal_ overholdes.

Maven setup er ikke perfekt. F.eks er test afviklingen ikke optimeret, og executions er ikke bundet til de rigtige faser - endnu.
Dette er et fælles project, så du er _meget_ velkommen til at hjælpe.

Der er 3 primære koncepter du skal ḱende for at forstå, bruge og udvide/ændre den måde maven bruges i dette projekt.

## Maven afhængigheder

### reactor pom bruges til transitive afhængigheder
Vi bruger maven modules, og det man kalder en "reactor pom". Reactor pom, er den pom.xml der ligger på yderste
niveau i dette projekt. [pom.xml]. Reactor pom'en indeholder en række moduler. Alle maven "definitioner" i reactor pom'en
(f.eks. <properties> ... </properties>) bliver arvet af hvert enkelt modul. Dette er en af de måder maven tillader nedarvning på.
Vi bruger denne nedarvningsform til at nedarve versionnumre af moduler og til at lade maven udregne dependencies og transitive dependencies for os.

Hvis du afhænger af andre images skal du angive navnet på det modul der beskriver dit image som dependency i din modul pom.
Du skal også angive denne afhængighed i reactor pom'en. Se den nuværende reactor pom og dbc-postgres-rawrepo som eksempel.

### maven-parent projektet bruges til fælles funktionalitet for alle moduler
Modulerne er ensartede. De beskriver alle Docker image (abstrakte eller konkrete). Vi har brug for at lave en række af
de samme operationer på hvert modul. For at samme denne ensartede funktionalitet bruger vi mavens parent nedarvning.
Alle modules har maven-parent som parent pom, der defineres således i den enkelte modul pom.xml

```
    <parent>
        <groupId>dk.dbc</groupId>
        <artifactId>dbc-maven-parent</artifactId>
        <version>1.0-SNAPSHOT</version>
        <relativePath>
            ../dbc-maven-parent
        </relativePath>
    </parent>
```

I prosa er nuværende eksempler på generelle funktionalitet for alle modules:

* Hvis projektet indeholder en Dockerfile, skal denne laves til et image med projektets navn
* Hvis du bruger _nuclear_ profilen med maven clean således mvn clean -Pnuclear gør vi os særligt umage med at fjerne alt,
  f.eks. dine images

### maven-testsuite bruges til at levere et test "framework"
Det skal være muligt for en udvikler at kunne teste sin kode ligesom Jenkins vil teste den. Desuden skal det være
nemt for en udvikler at skrive tests. For at understøtte dette, har vi lavet et særligt projekt, som alle moduler afhænger af
men som ikke arver fra maven-parent (da det ikke er beskriver et Docker image) og heller ikke selv er et modul.

maven-testsuite indholder en række basale testfunktioner der kopieres ud til alle moduler.
Derefter kan modulerne basere deres tests på disse basale testfunktioner. Det er tanken at de basale testfunktioner
modnes og udvides. Hvis du tilføjer fælles funktionalitet her (hvilket du er meget velkommen til) skal det være
generelt. Hvis det ikke er generelt skal du implementere det i dit eget projekt. Se afsnittet "Test" for flere detaljer.


## Docker afhængigheder

Hvert modul beskriver et Docker setup, og hvert setup kan have en eller følgende typer afhængigheder:

* Hvis dit modul har en Dockerfile, og denne Dockerfile har en FROM med et dbc image (f.eks. dbc-postgres)
  skal du eksplicit fortælle maven om denne afhængighed ved at tilføje modulet i din modul pom og reactor pom'en.
* Det er ikke alle moduler der indeholder en Dockerfile. Nogle moduler er konfigurationer af andre moduler (dvs. konfigurationer af andre images)
  se f.eks. dbc-postgres-logstore. Hvis dit modul er en konfiguration af et andet image, skal denne konfiguration være beskrevet i
  modulets compose fil, med reference til det image der konfigureres.

  ```
  dbc-postgres-logstore:
      image: dbc-postgres <---- her er din afhængighed
      environment:
          - POSTGRES_USER=logstore
          - POSTGRES_DB=logstore
          - POSTGRES_PASSWORD=logstore

  ```
  Du skal eksplicit fortælle maven om denne afhængighed ved at tilføje modulet i din modul pom og reactor pom'en.

* Hvis dit modul beskriver en samling af andre modules, men ikke selv er et image (f.eks. hele dataio) kaldes det en realm.
  En realm indeholder er antal images, der hver især er beskrevet i et module.
  Du skal eksplicit fortælle maven om denne afhængighed ved at tilføje modulet i din modul pom og reactor pom'en.

* Hvis dit modul beskriver et image der skal bruge andre images for at virke, udtrykkes dette behov i modulets compose fil.
  Konkret skrives afhængighederne som "links", f.eks.

  ```
    dbc-glassfish-gui:
        image: dbc-glassfish-gui
        extra_hosts:
            # svn.dbc.dk follows the docker naming convention.
            # This allows us to put in another svn if we have to.
            - dbc-svn:172.17.20.52
        links:
            - dbc-glassfish-flowstore
            - dbc-glassfish-logstore
            - dbc-glassfish-jobstore
            - dbc-openmq
  ```
    Du skal eksplicit fortælle maven om alle links ved at tilføje modulet i din modul pom og reactor pom'en.

## Fil afhængigheder
De enkelte moduler afhænger af forskellige filer for at virke korrekt. Som eksempel kan nævnes compose-filer fra andre
 projekter som bruges til at starte og fælles test filer. Disse filer kopieres automatisk ind i projektet under

 ```
 src/resources/docker
 ```

 Hvis du skal dele en fil med alle andre projekter, og det skal du tit, skal den ligges under

 ```
 src/main/docker/share
 ```

 hvorefter den automatisk kopieres til alle projekter der afhænger af dit projekt. Det er et krav at du leverer en
 compose fil der kan starte dit image (og dets afhængigheder) og det er et krav at denne fil ligger i /src/main/docker/share

 ## Hvordan finder jeg ud af hvilke maven kommandoer der virker i dette projekt

Se på [reactor maven pom file](pom.xml).

# Hvordan tilføjer jeg et nyt projekt / modul

Find et passende navn til dit projekt. Det er vigigt at du følger navnestandarderne. Hvis vi ikke har en fælles
navnestandard, bliver det hurtigt grænsende til umuligt at finde ud af hvordan tingene hænger sammen.
Se "Navngivning" for navnestandard.

Lad os sige at dit projekt skal hedde dbc-glassfish-coolui

1. Copy dbc-example as dbc-glassfish-coolui
2. Erstat Dockerfile med din egen Dockerfile og kopier alle dine docker afhængigheder til det samme dir som Dockerfile ligger i
3. Erstat den medbragte compose file med din egen. Den _skal_ hedde dbc-glassfish-coolui-compose.yml
4. For hvert "link" i din compose fil, skal du tilføje en dependency i din pom.xml (dbc-glassfish-coolui/pom.xml).
Din dependency skal også tilføjes i /pom.xml med versionsnummer.
5. Hvis din Dockerfile afhænger af et dbc-image (f.eks. FROM dbc-glassfish) skal denne afhængighed også indføres i
dbc-glassfish-coolui/pom.xml og /pom.xml
7. Hvis dit image har behov for konfiguration, skal du bruge confd som "motor". Se hvordan i [dbc-example/confd-readme.md]
6. Skriv tests
  1. Du har "arvet" en test-example.sh fil fra dbc-example. Omdøb den til dbc-glassfish-coolui-default-tests.sh
  2. Denne fil vil automatisk starte din compose, sammen med enhver compose fil du afhænger af,
   teste at din primære container (som hedder dbc-glassfish-coolui pr. konvention) kan startes og derefter lukke de
   containere du har startet ned.
  3. Følg beskrivelse i test-example.sh (nu dbc-glassfish-coolui-default-tests.sh) hvis du vil udvide eller tilføje tests
7. Kør mvn install for at se hele virker.

# Test
De *.sh filer du ligger i src/test/docker/my-tests/ vil blive afviklet af det integrerede test "framework".
Før din .sh fil bliver kaldt, bliver der opsat et par environment variable. For at afkoble din test fra det faktiske
fil-layout, _skal_ bruge disse environment variable til at navigere rundt i dit projekt. Hvis du får brug for flere,
kan du tilføje den i [maven-test-suite/src/main/docker/share/test-suite/setupEnvironmentAndCommonFunctionsForDevelopers.sh]
Se desuden [dbc-example/src/test/docker/my-tests/test-example.sh] for en beskrivelse af de tilgænglige variable og funktioner.

## Dine scripts skal håndtere skalering

Det skal være muligt at anvende test functioner til at teste om større, skalerede setups fungerer.
For at gør dette muligt skal du der hvor det giver mening tillade at man angiver et vilkærligt antal containere der skal testes.
Du kan se et eksempel på dette i kaldet nedenfor
```
exitIfContainerDoesNotStart 10 CONTAINER_IDs
```
Hvor det sidste argument kan være en eller flere CONTAINER_IDs. Scriptet (exitIfContainerDoesNotStart) vil blive
 anvendt på alle containerene.

# Docker design
En stor del af det design der ligger til grund for at alle de mange docker containers kan spille sammen er baseret på
 konventioner. Der er navngivningskonventioner, fil-strukturs konventioner og maven konventioner.

Vi anslår at indførelsen af disse konventioner har mindsket behovet for konfiguration med mindst 80%, og desuden
gjort det muligt at overskue og ændre større kompositioner i endelig tid.


# De 10 docker bud
## Et docker image skal navngives efter den eksisterende navnestandard
dbc-TECHNOLOGY-APPLICATION

f.eks.
dbc-glassfish-flowstore

Dette navn genbruges hvor det er muligt, bl.a. i :

* navngivning af maven artifact (<artifactId>dbc-glassfish-flowstore</artifactId>)
* navngivning af (evt.) tilhørende compose file (dbc-glassfish-flowstore-compose.yml)
* navngivning af den container der startes baseret på images i en compose file
```
dbc-glassfish-flowstore
    image: dbc-glassfish-flowstore
```

## Dockerfiler skal fungere selvstændigt og være selvdokumenterende

En Dockerfile skal have ENV definitioner (eller andet) der klart fortæller læseren hvordan imaget skal bruges og samtidig
fungerer som default værdier for imaget.

## En Container finder de services den skal bruge via configurations-konventioner

Vi bruger confd (se [confd](dbc-example/src/main/docker/dockerfile/confd-readme.md)

Confd gør det muligt at populere en configuration med key-values. Ved at navngive keys efter en konvention der
genanvender container navnet (og dermed image navnet da det altid er det samme) bliver det nemt gennemskueligt
at konfigurere services:

f.eks.
```
/dbc/postgres/logstore/env/postgres/password
```
hvor dbc/postres/logstore er navnet på den service man vil forbinde sig til, og env/postgres/password er det
password som servicen eksponerer. Password kan være eksponeret  som den default værdi der er sat i Dockerfile,
eller den værdi der er blevet overskrevet af en compose opsætning. Pointen er, at den service der skal bruge en database
ikke behøver at kende passwordet - det er miljøet der tilvejebringer det. I dette tilfælde via en compose fil,
navnekonventioner, confd og den måde Docker virker på.

When required, substitution in configuration files is done with _confd_ follow examples in the existing images,
e.g. dbc-glassfish-jobprocessor)

## Konvention over konfiguration
For eksempel kører alle glassfish på port 8080, og har det samme password.

# Brug Dockers automatiske ENVIRONMENT injection

Speifikt
```
HOSTXXX_PORT_5432_TCP_PORT
HOSTXXX_ENV_IMAGESPECIFICENVVARIABLE
```
I confd filer, hvor de navngives som noget i stil med
```
/dbc/postgres/flowstore/port/5432/tcp/port
/dbc/postgres/flowstore/env/imagespecificenvvariable
```

# [confd](dbc-example/src/main/docker/dockerfile/confd-readme.md)

confd has been used throughout most images, providing the necessary templating of configuration files as well as an opportunity for sourcing values from elsewhere in the future. The confd binary becomes (a part of) the entrypoint of each container, producing the necessary configuration files before the application itself is launched. A very simple example is provided in the dbc-example project.

# Fremadrettede ting vi kan se på

* Omskriv eksisterende jenkins tests til det nye framework. Der er ca 20
  * jenkins-build-apache.sh
  * jenkins-build-apache-php-openformat.sh
  * jenkins-build-apache-php-opensearch.sh
  * jenkins-build-es-sink.sh
  * jenkins-build-filestore.sh
  * jenkins-build-flowstore.sh
  * jenkins-build-glassfish.sh
  * jenkins-build-glassfish-openformat-java.sh
  * jenkins-build-harvester.sh
  * jenkins-build-jobprocessor.sh
  * jenkins-build-jobstore.sh
  * jenkins-build-logstore.sh
  * jenkins-build-openmq.sh
  * jenkins-build-postgres.sh
  * jenkins-build-postgres-danbib-is.sh
  * jenkins-build-rr-introspect.sh
  * jenkins-build-update.sh
  * jenkins-test-openformat-java7.sh
* Jenkins på den fede måde. Tests skal afvikles på Jenkins på en konfigurations-minimal måde.
  Det er tanken at Jenkins skal have absolut minimal tilstand, og at de tests der afvikles på
  Jenkins skal kunne afvikles af udviklere på deres egne maskiner via maven.
* håndtering af data og versionering af data.
* Effektivisering af Maven
  * bedre maven struktur. Den nuværende struktur virker, men den trænger til en maven ekspert der
    kan indføre best-practice og se på konkrete problemer. Et "problem" er at der pt. ikke skelnes
    mellem tests og integrations-tests. Alt er integrations-tests
  * Byg / commit af images fra maven
  * Modning af byggeproces i maven (mapping til executions)
    * Optimering af test (det samme testes mange gange)
    * Hurtigere kod/test iterationer
* Vi vil gerne kunne starte en version af "systemet" på et givet commit tidspunkt. Vi vil også kunne starte en
  version af systemet med den kode jeg sidder med i mit IDE lige nu. Dette er funktionaliteter der
  skal indarbejdes i vores setup
* Udlusning af kendte fejl
* Hvordan bruger man dette som udvikler?
  * Man vil måske gerne have mulighed for at injecte egen kode i en kørende komposition, så man kan live
    debugge sin applikation.
* Conf ude af sync med virkelighed
* Håndtering af artifakter
  * docker repo skal op at køre, vi skal have en navngivning / versions strategi
  * andre artifakter (docker) skal nok på artifactory
* Hvordan skal vi samle test og udvikling af DOcker containere med den faktiske kode der kører i images/containere
* Konsistent conf på tværs af adfelinger og udv, prod
* Data skal passe med den nuværende release
* Mere dokumentation
* Test af systemet selv
* Konsekvent tilretning af de nuværende images til at følge de 10 bud
* Fjern oracle som kø
* Opdater komponent-oversigts grafik (dependencies.dot og inheritance.dot)
* Lav en måde hvorpå man kan lave et sikkert genbyg - --nuclear
* Overlay fs farligt. Kernel Bug 109611

