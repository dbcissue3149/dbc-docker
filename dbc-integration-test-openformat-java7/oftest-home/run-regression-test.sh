#!/usr/bin/env bash

# Updates this directory from svn, then runs compare-tests.sh

# Location of PHP install
PHP=$HOME/public_html/trunk


# Helper function.
function die() {
    echo "RRT: run-regression-test.sh Error: ""$@"
    exit 1
}

# Update path with the dirname of this file. Make things easier
which realpath &> /dev/null || die "Unable to locate realpath binary"
which dirname &> /dev/null || die "Unable to locate dirname binary"
BASEDIR=$(dirname $(realpath "$0"))
export PATH="$BASEDIR":$PATH

echo "RRT: run-regression-test.sh starting (RRT)"
echo "RRT: Updating svn installation of regression-test scripts"
svn update "$BASEDIR" || die "Unable to update $BASEDIR directory from svn"

# Change the PHP file to point to the right one for us.
cp $PHP/openformat-tomcat-java-6.ini $PHP/openformat.ini || die "Unable to set up test for using tomcat and java 6"

exec update-compare.sh "$@"
