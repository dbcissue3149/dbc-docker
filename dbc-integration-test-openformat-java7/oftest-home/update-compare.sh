#!/usr/bin/env bash

# Script to update the installation, then run the tests.
# Works by calling some other scripts, obviously

# Update path with the dirname of this file. Make things easier
which realpath &> /dev/null || die "Unable to locate realpath binary"
which dirname &> /dev/null || die "Unable to locate dirname binary"
export PATH=$(dirname $(realpath "$0")):$PATH

function die() {
    echo "UC: update-compare.sh Error: " "$@"
    exit 1
}

echo "UC: update-compare.sh starting (UC)"
update-installation.sh || die "Unable to update the installation"
echo "UC: Waiting 30 seconds for the application server to start properly"
sleep 30
compare-test.sh || die "Unable to perform compare test or compare test failed"
