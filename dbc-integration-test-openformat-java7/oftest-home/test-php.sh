#!/usr/bin/env bash

FILENAME=$1

function die() {
    echo "Error:" "$@"
    exit 1
}

test -n "$FILENAME" || die "You must provide a filename"
test -a "$FILENAME" || die "Unable to locate $FILENAME"

curl -s  -H "Content-type:text/xml; charset=utf8" -X 'POST' -d @"$FILENAME" http://dempo.dbc.dk/~oftest/trunk/server.php
