#!/usr/bin/env bash

# This script starts the application container - in this case Tomcat

function die() {
    echo "SOA: start-oftest-appcontainer.sh Error: " "$@"
    exit 1
}

echo "SOA: start-oftest-appcontainer.sh starting (SOA)"
cd ~oftest || die "could not cd to ~oftest"

# And, this is for remote monitoring. No password or anything.
# You need port and hostname, use jconsole, jvisualvm or similar.
JMX_ENABLED="-Dcom.sun.management.jmxremote \
             -Dcom.sun.management.jmxremote.port=8999 \
             -Dcom.sun.management.jmxremote.ssl=false \
             -Dcom.sun.management.jmxremote.authenticate=false \
             -Djava.rmi.server.hostname=localhost"

# mbd 2012-12-19:
# The -XX:+CMSClassUnloadingEnabled allows unloading of classes from permgen
# This is an experiment to stop permgen errors.
MEM_OPTIONS="-Xms4000m -Xmx12000m -XX:MaxPermSize=4096m \
             -XX:+UseConcMarkSweepGC -XX:+CMSClassUnloadingEnabled"

# mbd 2013-01-02
# More trying to get rid of permgen - lets get a heap dump when it fails.
HEAP_DUMP="-XX:+HeapDumpOnOutOfMemoryError -XX:HeapDumpPath=/home/oftest/heap-dumps/"

# And, the actual options exported here.
export CATALINA_OPTS="-Djava.awt.headless=true $JMX_ENABLED $MEM_OPTIONS $HEAP_DUMP -server"
echo "SOA: Setting CATALINA_OPTS = \"$CATALINA_OPTS\""
echo "SOA: Starting Tomcat application container"
# ./bin/jsvc -cp ./bin/bootstrap.jar -pidfile catalina.pid -outfile ./logs/catalina.out -errfile ./logs/catalina.err org.apache.catalina.startup.Bootstrap || die "Unable to start Tomcat application container"
$HOME/bin/startup.sh || die "Unable to start Tomcat application container"
echo "SOA: Tomcat started"
