#!/usr/bin/env bash

function die() {
    echo "KOA: stop-oftest-appcontainer.sh Error: " "$@"
    exit 1
}

echo "KOA: stop-oftest-appcontainer.sh starting (KOA)"
cd ~oftest || die "could not cd to ~oftest"

echo "KOA: Stopping Tomcat application container"
# ./bin/jsvc -stop -pidfile catalina.pid org.apache.catalina.startup.Bootstrap 
$HOME/bin/shutdown.sh 
# Stop fails are ignored... is that what we want?


