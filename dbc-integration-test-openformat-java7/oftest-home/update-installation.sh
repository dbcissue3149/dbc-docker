#!/usr/bin/env bash

# This script updates the installation of the openformat servlet, and the php layer

# Update path with the dirname of this file. Make things easier
which realpath &> /dev/null || die "Unable to locate realpath binary"
which dirname &> /dev/null || die "Unable to locate dirname binary"
export PATH=$(dirname $(realpath "$0")):$PATH

# Location of java
export JAVA_HOME=/usr/dbc/jdk/1.6

# Path to the php layer
PHP=$HOME/public_html/trunk

# Where to stuff the openformat.war file we get from IS
WARF=`mktemp openformatXXXXXXXXXX.war`

# What to get from the IS
WARI=http://is.dbc.dk/job/openformat-head-guesstimate/lastSuccessfulBuild/artifact/target/openformat.war

# What URL to check if tomcat is running. Could be in start script... 
TURL=http://localhost:18080/
# Helper function
function die() {
    echo "UI: update-installation.sh Error: " "$@"
    exit 1
}

# Echo start
echo "UI: update-installation.sh starting (UI)"

# Get a new one from is.dbc.dk
echo "UI: Update installation of php and java layer on host" `hostname`
echo "UI: Getting a new war file from $WARI"
wget --quiet -O "$WARF" "$WARI" || die "Unable to retrieve new warfile : wget -O \"$WARF\" \"$WARI\""

# If problematic: Might want to check if the application server! is actually running at this point, and start it, if not.

# Restart application container
echo "UI: Stopping application container"
stop-oftest-appcontainer.sh || die "stop-oftest-appcontainer.sh failed"

MAXWAIT=60
echo -n "UI: Waiting at most $MAXWAIT seconds for JMX interface to release port 8999 "
for (( i=0 ; i < $MAXWAIT ; i++ )) ; do
    echo -n .
    if ! netstat -a | grep 8999 &> /dev/null ; then
	break
    fi
    sleep 1
done
echo " waited for $i seconds"
netstat -a | grep 8999 &> /dev/null && die "JMX interface refuses to release port 8999"

echo "UI: Starting application container"
start-oftest-appcontainer.sh || die "start-oftest-appcontainer.sh failed"

MAXWAIT=40
echo -n "UI: Waiting at most $MAXWAIT seconds for the application container to start "
for (( i=0 ; i < $MAXWAIT ; i++ )) ; do
    echo -n .
    if curl --silent --max-time 1 -o /dev/null "$TURL" ; then
	break
    fi
    sleep 1
done
echo " waited for $i seconds"
curl --silent --max-time 1 -o /dev/null "$TURL" || die "Timeout while waiting for application container to start"

# Install the war file
echo "UI: Calling update-openformat-servlet.sh to install war file into servlet container"
update-openformat-servlet.sh "$WARF" || die "Unable to install warfile : update-openformat-servlet.sh \"$WARF\""

# Clean out the old war file
rm -f $WARF || die "Unable to remove an war file after update: rm $WARF"

# Update the svn php layer
echo "UI: Updating the php installation at $PHP"
svn update "$PHP" || die "Unable to update the php layer : svn update \"$PHP\""
 
echo "UI: All done - installation updated"
