#!/usr/bin/env bash

# Script to update an openformat installation and restart it

# The url to our server
SERVER=http://localhost:18080
SPATH=/openformat
USERP=openformat:openformat

# And, quite hackish, the location of the catalina/tomcate log file...
CATALINA_LOG=$HOME/logs/catalina.err


function die() {
    echo -e "UOS: update-openformat-servlet.dh Error: " "$@"
    exit 1
}

function usage() {
    echo "Usage: $0 [options] <input-file>"
    echo
    echo -e "-h            \tThis message"
    echo -e "-s service-url\tThe URL of the service to use [$SERVER]"
    exit;
}

# Argument 1 is the name of the war file
# Parse the opts
while getopts  "hs:" flag
do
  case "$flag" in
      h)
          usage
          ;;
      s)
          SERVER=$OPTARG
          ;;
      *)
          die "Unable to parse $flag"
          ;;
  esac
done
shift $((OPTIND-1))
# echo "SERVER=$SERVER"

# Test at least one argument is given
if test "x$1" == "x" ; then
    usage;
fi
WARFILE=$1

echo "UOS: update-openformat-servlet.sh starting (UOS)"

# Check if file exists
WARFILE=`realpath "$WARFILE"`
test -a "$WARFILE" -a -r "$WARFILE" || die "Unable to open file $WARFILE"

# Create temporary file for curl output
TMPFILE=`mktemp` || die "Unable to run mktemp"


# Undeploy any existing openformat installation
echo "UOS: Undeploying old installation at $SERVER / $SPATH"
if ! curl -s -o $TMPFILE --user $USERP "$SERVER/manager/undeploy?path=$SPATH" ; then
    echo "Unable to undeploy - dumping output, and retrying"
    cat $TMPFILE
    if ! curl -s -o $TMPFILE --user $USERP "$SERVER/manager/undeploy?path=$SPATH" ; then
	echo "Unable to undeploy - dumping output, and giving up"
	cat $TMPFILE
	die "Unable to undeploy: curl -o $TMPFILE --user $USERP '$SERVER/manager/undeploy?path=$SPATH'"
    fi
fi
if grep -e FAIL -e 401 $TMPFILE &> /dev/null ; then
    # Check this is not "does not exist", we ignore that
    if ! grep "FAIL - No context exists for path $SPATH" $TMPFILE &> /dev/null ; then
	cat $TMPFILE
	die "Some kind of error happened - most likely a FAIL in the application container, or illegal access. Dumping 100 lines of catalina/tomcat logfile" `tail -100 $CATALINA_LOG`
    fi
else
    echo -n "UOS: Response from Application Container Server: "
    cat $TMPFILE
fi
rm $TMPFILE


# Deploy a new one - will restart if possible
echo "UOS: Old installation undeployed, deploying new installation at $SERVER / $SPATH"
curl -s -o $TMPFILE --user $USERP "$SERVER/manager/deploy?path=$SPATH&war=file:$WARFILE" || die "Unable to run curl to deploy"
if grep -e FAIL -e 401 $TMPFILE &> /dev/null ; then
    echo -n "UOS: Response from Application Container Server: "
    cat $TMPFILE
    die "Some kind of error happened - most likely a FAIL in the application container, or illegal access. Dumping 100 lines of catalina/tomcat logfile" `tail -100 $CATALINA_LOG`
else 
    echo -n "UOS: Response from the Application Container Servlet: "
    cat $TMPFILE
fi
echo "UOS: New installation deployed"
rm $TMPFILE
