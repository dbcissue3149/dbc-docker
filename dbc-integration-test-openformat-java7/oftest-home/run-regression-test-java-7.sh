#!/usr/bin/env bash

# Updates this directory from svn, then runs compare-tests.sh

# Location of PHP install
PHP=$HOME/public_html/trunk

# Helper function.
function die() {
    echo "RRT7: run-regression-test-java-7.sh Error: ""$@"
    exit 1
}

# Update path with the dirname of this file. Make things easier
which realpath &> /dev/null || die "Unable to locate realpath binary"
which dirname &> /dev/null || die "Unable to locate dirname binary"
BASEDIR=$(dirname $(realpath "$0"))
export PATH="$BASEDIR":$PATH

#echo "RRT7: run-regression-test.sh starting (RRT)"
#echo "RRT7: Updating svn installation of regression-test scripts"
#svn update "$BASEDIR" || die "Unable to update $BASEDIR directory from svn"

# Update the svn php layer
#echo "RRT7: Updating the php installation at $PHP"
#svn update "$PHP" || die "Unable to update the php layer : svn update \"$PHP\""

# Change the PHP file to point to the right one for us.
#cp $PHP/openformat-glassfish-java-7.ini $PHP/openformat.ini || die "Unable to set up test for using glassfish and java 7"

compare-test.sh "$@" || die "Unable to perform compare test or compare test failed"

