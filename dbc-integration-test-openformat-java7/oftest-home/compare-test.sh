#!/usr/bin/env bash

# This script has been ported from
#    ssh oftest@hypotese.dbc.dk oftest-bin/run-regression-test-java-7.sh
# In its original form, it depended on a Web Service started locally here:
#    WS=http://localhost/~oftest/trunk/server.php
# In the ported form, it depends on the same Webservice to be started externally and be accessible through this variable:
#   OPENFORMAT_SERVICE
# which (in the hour of writing) is set to OPENFORMAT_SERVICE=http://openformat-client/openformat/ in the Dockerfile
# containing _this_ file.
#
#=http://openformat-client/openformat/
# With docker, this could be achieved with:
# 1. start an openformat client in the version you want to test, call it e.g. CLIENT
# 2. docker run --link CLIENT:openformat-client -ti -v $PWD/oftest-home/:/home/oftest/ THIS_IMAGE bash


# Print command traces before executing command.
set -o xtrace

#set -o verbose
# Script that compares a number of calls to the OpenFormat request with canned responses

# curl -H "Content-type:text/xml; charset=utf8" -X 'POST' -d @xml/request/bog_hans_engell.xml
function die () {
    echo "CT: compare-test: Error: " "$@"
    exit 1
}

function reset_file()
{
    FILE_TO_RESET=$1
    echo "Resetting file"
    echo "Resetting file $FILE_TO_RESET"
    rm -f $FILE_TO_RESET
    touch $FILE_TO_RESET || die Could not reset $FILE_TO_RESET
}


## base directory for responses and requests
BASEDIR_REQ_RES=$HOME/public_html/trunk/xml

## request directories with templates in different input formats
REQUESTDIR_DM2=$BASEDIR_REQ_RES/request_templates
REQUESTDIR_OCLC=$BASEDIR_REQ_RES/request_oclc
REQUESTDIR_M21=$BASEDIR_REQ_RES/request_m21

## response directories
#RESPONSEDIR=$BASEDIR_REQ_RES/response
REFWORKSDIR=$BASEDIR_REQ_RES/response_refworks
FULLDISPLAYHTMLDIR=$BASEDIR_REQ_RES/response_fulldisplayhtml
BIBLIOTEKDKFULLDISPLAYDIR=$BASEDIR_REQ_RES/response_bibliotekdkfulldisplay
ORSDIR=$BASEDIR_REQ_RES/response_ors
RISDIR=$BASEDIR_REQ_RES/response_ris
NULLDISPLAYDIR=$BASEDIR_REQ_RES/response_nulldisplay
M21TODM2=$BASEDIR_REQ_RES/response_m21todm2
M21ISO2709DIR=$BASEDIR_REQ_RES/response_m21iso2709
M21OCLCDIR=$BASEDIR_REQ_RES/response_m21Oclc

## the response directories with responses to test
RESPONSEDIRS="$REFWORKSDIR $FULLDISPLAYHTMLDIR $BIBLIOTEKDKFULLDISPLAYDIR $RISDIR $ORSDIR $M21TODM2 $NULLDISPLAYDIR $M21OCLCDIR $M21ISO2709DIR"
#Error ... $M21TODM2

## the text to substitute in template file with name of actual format
PLACEHOLDER="PLACEHOLDER"

## logfiles
REQUESTLOGFILE=$HOME/logs/compare-test-request.log
CATALINAERRLOG=$HOME/logs/catalina.err
CATALINALOG=$HOME/logs/catalina.out
## other

mkdir -p $HOME/logs
reset_file $REQUESTLOGFILE
reset_file $CATALINAERRLOG
reset_file $CATALINALOG


# For the folders response_ris, response_fulldisplayhtml, response_refworks  we go
# through the folder request_templates and see if they have files with matcing names. 
# for each matcing file we send a request to the OpenFormat service with the 
# outputformat changed from $$OPENFORMAT$$ the either ris, fulldisplayhtml or refworks
# depending on which folder we are going through for responses. 

# For each folder in RESPONSEDIRS
#   For each request in the request directory
#     check if there is a response file
#       set the outputformat TODO
#       change the outputformat of the requestfile TODO
#       send it to the service
#       canonilize the result
#       compare against canon expected response
RET=0
RESP=1
echo
echo "CT: Compare test starting (CT) ..."
LOGFILE=`mktemp` || die "Unable to create logfile"
echo "=====================================================================" > $LOGFILE
echo "Detailed information about failed tests follows" >> $LOGFILE

echo
echo "CT: Result of tests"
echo

JUNIT=$HOME/junit.xml
rm -rf $JUNIT
touch $JUNIT
chmod a+rw $JUNIT
echo "<testsuite>" >> $JUNIT

# Reset request log file


for RESPONSEDIR in $RESPONSEDIRS ; do
    
    echo "CT: Testing against files from $RESPONSEDIR"
    #echo "CT: Using source files from " `svn info $REQUESTDIR`
    #echo "CT: Comparing against expected results from " `svn info $RESPONSEDIR`

    case "$RESPONSEDIR" in
       $REFWORKSDIR)  
           OUTPUTFORMAT=refWorks 
           REQUESTDIR=$REQUESTDIR_DM2
           ;;
       $RISDIR)  
           OUTPUTFORMAT=ris 
           REQUESTDIR=$REQUESTDIR_DM2
           ;;
       $FULLDISPLAYHTMLDIR) 
           OUTPUTFORMAT=fullDisplayHtml 
           REQUESTDIR=$REQUESTDIR_DM2
           ;;
       $BIBLIOTEKDKFULLDISPLAYDIR) 
           OUTPUTFORMAT=bibliotekdkWorkDisplay 
           REQUESTDIR=$REQUESTDIR_DM2
           ;;
       $NULLDISPLAYDIR)
           OUTPUTFORMAT=nullDisplay 
           REQUESTDIR=$REQUESTDIR_DM2
           ;;
       $ORSDIR) 
	   OUTPUTFORMAT=orsBibliographicFormatXml 
           REQUESTDIR=$REQUESTDIR_DM2
           ;;
       $M21ISO2709DIR) 
           OUTPUTFORMAT=dm2ToOclcMarc21Iso2709
           REQUESTDIR=$REQUESTDIR_OCLC
           ;;
       $M21OCLCDIR) 
           OUTPUTFORMAT=dm2ToOclcMarc21MarcXchange
           REQUESTDIR=$REQUESTDIR_OCLC
           ;;
       $M21TODM2)
           OUTPUTFORMAT=marc21ToDm2
           REQUESTDIR=$REQUESTDIR_M21
           ;;
       *) echo "unknown format: $RESPONSEDIR" ;;
    esac 

    echo "CT: OUTPUTFORMAT set to $OUTPUTFORMAT"
    for FILE in $REQUESTDIR/* ; do
    echo -n "Checking if there is a response file for $FILE ... "
	BASENAME=`basename "$FILE"`
	if test -a "$RESPONSEDIR/$BASENAME" ; then
	    RESP=0
	    echo yes there is a responsefile - change outputformat in request file from $PLACEHOLDER  to $OUTPUTFORMAT
	    sed -e "s/$PLACEHOLDER/$OUTPUTFORMAT/g" $FILE > tmpRequestFile
#           running request.
	    # Dumping request into log file
	    date >> $REQUESTLOGFILE
	    cat tmpRequestFile >> $REQUESTLOGFILE
	    TMPRES=`mktemp`
	    /usr/bin/time --output=/tmp/oftest.timer --format="%e" curl -s -S -o $TMPRES -H "Content-type:text/xml; charset=utf8" -X 'POST' -d "@tmpRequestFile" $OPENFORMAT_SERVICE || die "Unable to run: curl -s -S -o $TMPRES -H \"Content-type:text/xml; charset=utf8\" -X 'POST' -d \"@tmpRequestFile\" $OPENFORMAT_SERVICE"
	    TIME=`cat /tmp/oftest.timer`
	    # Checking if we got anything
	    test -s $TMPRES || die "Response was empty when doing: curl -S -s -o $TMPRES -H \"Content-type:text/xml; charset=utf8\" -X \'POST\' -d \"@tmpRequestFile\" $OPENFORMAT_SERVICE"
	    echo "Converting response to canonical format"
	    TMPACTUALC=`mktemp --tmpdir $BASENAME.actual.XXXXXXXXXX`
	    xmllint --c14n11 --format -o $TMPACTUALC $TMPRES || die "Unable to run: xmllint --c14n11 -o $TMPACTUALC $TMPRES"
	    echo "Converting expected to canonical format"
	    TMPEXPECTEDC=`mktemp --tmpdir $BASENAME.expected.XXXXXXXXXX`
##  xmlstarlet sel -fo -N of="http://oss.dbc.dk/ns/openformat" -t -c "//of:formatResponse" OpenFormat/response_bibliotekdkfulldisplay/forskbib_hundrup.xml | xmlstarlet fo -s 2
	    xmllint --c14n11 --format -o $TMPEXPECTEDC "$RESPONSEDIR/$BASENAME" || die "Unable to run: xmllint --c14n11 -o $TMPEXPECTEDC \"$RESPONSEDIR/$BASENAME\""
	    echo "<testcase classname=\"OpenFormat.$OUTPUTFORMAT\" name=\"$BASENAME\" time=\"$TIME\">" >> $JUNIT
	    if ! cmp -s "$TMPEXPECTEDC" "$TMPACTUALC" ; then
		RET=1
		DATE=`date`
		echo "!!! At : $DATE"
		echo "!!! At : $DATE" >> $LOGFILE
		echo "!!! $BASENAME: ERROR: Difference detected between expected and actual"
		echo "!!! $BASENAME: ERROR: Difference detected between expected and actual" >> $LOGFILE
		# Not using svn anymore
		# echo "!!! Source: " `svn info $FILE` >> $LOGFILE
		echo !!! Source: $FILE >> $LOGFILE
		# If the actual is an http error, grab the latest exception from tomcat, and see what happens
		if grep ":error>HTTP error " "$TMPACTUALC" &> /dev/null ; then
		    cat "$TMPACTUALC" >> $LOGFILE
		    echo "HTTP error detected." >> $LOGFILE
		    echo "The java server seems to have failed." >> $LOGFILE
		    echo "Logging last exception from tomcat. May or may not be related (note, not for Java 7/glassfish): " >> $LOGFILE
		    tac "$CATALINAERRLOG" | grep -B 500 -m 1 "^SEVERE: " | tac >> $LOGFILE
		    tac "$CATALINALOG" | grep -B 500 -m 1 "^Exception" | tac >> $LOGFILE
		else
		    diff -u "$TMPACTUALC" "$TMPEXPECTEDC" >> $LOGFILE
		fi
		echo "================================================================================" >> $LOGFILE
		echo "  <failure type=\"CompareError\">" >> $JUNIT
		# If the actual is a 500 error, grab the latest exception from tomcat, and see what happens
		# This is for the junit file, needs to be XML friendly.
		if grep ":error>HTTP error " "$TMPACTUALC" &> /dev/null ; then
		    cat "$TMPACTUALC" | xmlstarlet esc >> $JUNIT
		    echo "At $DATE" >> $JUNIT
		    echo "HTTP error detected." >> $JUNIT
		    echo "The java server seems to have failed." >> $JUNIT
		    echo "Logging last exception from tomcat. May or may not be related (note, NOT for Java 7/glassfish): " >> $JUNIT
		    tac "$CATALINAERRLOG" | grep -B 500 -m 1 "^SEVERE: " | tac | xmlstarlet esc >> $JUNIT 
		    tac "$CATALINALOG" | grep -B 500 -m 1 "^Exception" | tac | xmlstarlet esc >> $JUNIT
		else
		    diff -u "$TMPACTUALC" "$TMPEXPECTEDC" | xmlstarlet esc >> $JUNIT
		fi
	    # echo "    Difference detected between expected and actual" >> $JUNIT
		echo "  </failure>" >> $JUNIT
	    else
		echo "=== $BASENAME: OK"
	    fi
	    rm $TMPRES $TMPACTUALC $TMPEXPECTEDC
	else
	    echo "<testcase classname=\"OpenFormat.$OUTPUTFORMAT\" name=\"$BASENAME\">" >> $JUNIT
	    echo "??? $BASENAME: Warning: No expected result file found"
	    echo "  <skipped/>" >> $JUNIT
	fi
	echo "</testcase>" >> $JUNIT
    done
done
echo "</testsuite>" >> $JUNIT

if test $RESP != 0 ; then
    exit $RESP
fi

if test $RET != 0 ; then
    echo
    echo "CT: Errors was present."
    echo
    cat $LOGFILE
    echo "====================================================================="
else 
    echo "CT: No errors found."
fi
rm $LOGFILE
echo
echo "CT: Compare test finished"
echo
