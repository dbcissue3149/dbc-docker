#!/usr/bin/env bash

echo "installing docker"

sudo apt-get install curl -y
sudo curl -sSL https://get.docker.com/ubuntu/ | sudo sh
sudo restart docker
