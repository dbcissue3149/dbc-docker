#!/usr/bin/env bash
echo "╔═╗┌─┐┌─┐┌┐┌┌─┐┌─┐┬─┐┌┬┐┌─┐┌┬┐             ";
echo "║ ║├─┘├┤ │││├┤ │ │├┬┘│││├─┤ │              ";
echo "╚═╝┴  └─┘┘└┘└  └─┘┴└─┴ ┴┴ ┴ ┴              ";
echo "╦┌┐┌┌┬┐┌─┐┌─┐┬─┐┌─┐┌┬┐┬┌─┐┌┐┌  ╔╦╗┌─┐┌─┐┌┬┐";
echo "║│││ │ ├┤ │ ┬├┬┘├─┤ │ ││ ││││   ║ ├┤ └─┐ │ ";
echo "╩┘└┘ ┴ └─┘└─┘┴└─┴ ┴ ┴ ┴└─┘┘└┘   ╩ └─┘└─┘ ┴ ";

if [[ $# < 2  ]] ; then
    echo "USAGE: $0 [Path to folder with request] [hostname of transformation server] [-options]";
    echo "Example: $0 integration-test-openformat-sample-request-few/ localhost:80/openformat/ -clean";
    echo "Example: $0 integration-test-openformat-sample-request-few/ http://openformat.addi.dk/0.2/ -clean";

    echo "";
    echo "[Path to folder with request] = folder containing sample xml requests.";
    echo "[hostname of transformation server] = where the transformation server is located.";
    echo "options : -clean";

    echo " Both arguments are mandatory";
    echo " override the account credentials defined in this script.";
    echo "";
    exit 1;
fi

case "$3" in
    -clean) echo 'you gave the -clean argument. All responses will be deleted.' ;;
    *) echo 'will not clean up' ;;
esac

#FILES=dbc-apache-php-openformat/openformat-php-build-svn-job/xml/request/*.xml
FILES=$1/*.xml

#SERVERNAME=http://opensearch.addi.dk/3.0/
#SERVERNAME=http://localhost:80/openformat/
SERVERNAME=$2

FOLDERNAME="$(date +'%Y_%m_%d_%H_%M_%S')"

echo "Creating temporary folder for responses = $FOLDERNAME"

mkdir $FOLDERNAME
rm response-times.log
touch response-times.log

echo "Doing five requests types bibliotekdkFullDisplay, briefDisplayHtml, ris, briefDisplay, fullDisplayHtml for each file"
for f in $FILES
do
  echo "  Processing file $f "
  TMPFILENAME=${f##*/}

  # curl action on each file. $f the current full path name. Max time is 6min per request.
  curl -X POST -sS -w %{time_total} -d "no=1&outputFormat=bibliotekdkFullDisplay&subm=try+me" \
            --data-urlencode "xml=$(cat $f)" \
            -o "$FOLDERNAME/bibliotekdkFullDisplay_$TMPFILENAME" $SERVERNAME \
            --header "Content-Type:application/x-www-form-urlencoded" >> response-times.log
  echo "" >> response-times.log
  curl -X POST -sS -m 360 -w %{time_total} -d "no=1&outputFormat=briefDisplayHtml&subm=try+me" \
            --data-urlencode "xml=$(cat $f)" \
            -o "$FOLDERNAME/briefDisplayHtml_$TMPFILENAME" $SERVERNAME \
            --header "Content-Type:application/x-www-form-urlencoded" >> response-times.log
  echo "" >> response-times.log
  curl -X POST -sS -m 360 -w %{time_total} -d "no=1&outputFormat=ris&subm=try+me" \
            --data-urlencode "xml=$(cat $f)" \
            -o "$FOLDERNAME/ris_$TMPFILENAME" $SERVERNAME \
            --header "Content-Type:application/x-www-form-urlencoded" >> response-times.log
  echo "" >> response-times.log
  curl -X POST -sS -m 360 -w %{time_total} -d "no=1&outputFormat=briefDisplay&subm=try+me" \
            --data-urlencode "xml=$(cat $f)" \
            -o "$FOLDERNAME/briefDisplay_$TMPFILENAME" $SERVERNAME \
            --header "Content-Type:application/x-www-form-urlencoded" >> response-times.log
  echo "" >> response-times.log
  curl -X POST -sS -m 360 -w %{time_total} -d "no=1&outputFormat=fullDisplayHtml&subm=try+me" \
            --data-urlencode "xml=$(cat $f)" \
            -o "$FOLDERNAME/fullDisplayHtml_$TMPFILENAME" $SERVERNAME \
            --header "Content-Type:application/x-www-form-urlencoded" >> response-times.log
  echo "" >> response-times.log
done

echo "┬─┐┌─┐┌─┐┌─┐┬─┐┌┬┐";
echo "├┬┘├┤ ├─┘│ │├┬┘ │ ";
echo "┴└─└─┘┴  └─┘┴└─ ┴ ";
echo -ne "Requests files in total: " && ls -l $FILES | wc -l

RESPONSESINTOTAL=$(ls $FOLDERNAME/ | wc -l)
if [ $RESPONSESINTOTAL -gt 0 ]
then
  echo -ne "Responses in total: " && echo $RESPONSESINTOTAL
else
  echo "No responses received" >&2
  exit 1
fi
CLOCKEDRESPONSESINTOTAL=$(cat response-times.log | wc -l)
echo -ne "Clocked responses in total: " && echo $CLOCKEDRESPONSESINTOTAL

grep --color "<faultstring\|<faultcode\|error\|404 Not Found\|301 Moved Permanently" $FOLDERNAME/*.xml
ERRORSINTOTAL=$(grep "faultstring\|faultcode\|error\|404 Not Found\|301 Moved Permanently" $FOLDERNAME/*.xml | wc -l)
echo -ne "Errors in total: " && echo $ERRORSINTOTAL

echo ""
echo "Response time (seconds):"
# calculate average response time.
cat response-times.log  | awk '{if(min==""){min=max=$1}; if($1>max) {max=$1}; if($1<min) {min=$1}; total+=$1; count+=1} END {print "total   = ",total,"\ncount   = ",count,"\naverage = ",total/count,"\nslowest = ", max,"\nfastest = ", min}'


if [ "$3" == "-clean" ]; then
    echo "Removing folder  $FOLDERNAME with responses"
    rm -fR $FOLDERNAME
fi


if [ $ERRORSINTOTAL -gt 0 ]
then
 echo "Responses received, but contained errors " >&2
  exit $ERRORSINTOTAL

else
  echo "Openformat integration test finished. No errors found - everything okay. Exit 0."
  exit 0
fi