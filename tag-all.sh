#!/usr/bin/env bash

# Created by Jesper Wermuth as a means to populate a registry from scratch.
# The usecase was, that I had to work from home, and could not get access to the
# dbc.registry from home, so I created my own registry inside a virtual machine,
# filled it with images and took it home with me

function box_out()
{
echo "---------------"
echo $*
echo "---------------"
}

set -e

# The indents is a primitive way to keep track of dependencies, 
#   e.g. "dbc-glassfish-flowstore" depends on "dbc-glassfish" 
declare -a arr=(
						"dbc-apache-php" \
							"dbc-apache-php-openformat" \
				)

for dockerfile in "${arr[@]}"

do
	box_out Pusing $dockerfile to registry
    
    docker build -t $dockerfile $dockerfile
    
    # tag and push, so
    docker tag -f $dockerfile docker-index:5000/$dockerfile:built-from-source
    docker tag -f $dockerfile docker-index:5000/$dockerfile
    docker push docker-index:5000/$dockerfile:built-from-source
    BUILT+=$dockerfile,
done
echo "The following docker images have been built: " $BUILT

