#!/usr/bin/env bash
set -e  

/usr/local/bin/confd -onetime -backend env

echo "Starting Tomcat"
exec /usr/local/tomcat/bin/catalina.sh run
