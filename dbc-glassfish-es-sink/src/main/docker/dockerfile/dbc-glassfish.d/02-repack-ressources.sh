#!/usr/bin/env bash

# Stop on error
set -e

# when the shell expands, dont include the pattern in the list of results (e_g_ exclude *_xml)
shopt -s nullglob

#export ES_RESOURCE_NAME=$(/usr/local/bin/cmdb-get ${CMDB} ${TOKEN} esresourcename)
#export ES_DATABASENAME=$(/usr/local/bin/cmdb-get ${CMDB} ${TOKEN} esdatabasename)


ES_RESOURCE_NAME=${ES_RESOURCE_NAME:-$DEFES_RESOURCE_NAME}
ES_DATABASENAME=${ES_DATABASENAME:-$DEFES_DATABASENAME}

EAR=dataio-sink-es-ear-1.0-SNAPSHOT
EJB=dataio-sink-es-ejb-1.0-SNAPSHOT

mkdir ${GLASSFISH_USER_HOME}/${EAR}
mkdir ${GLASSFISH_USER_HOME}/${EJB}
mv ${GLASSFISH_USER_HOME}/${EAR}.ear ${GLASSFISH_USER_HOME}/${EAR}

cd ${GLASSFISH_USER_HOME}/${EAR}

unzip ${EAR}.ear
rm ${EAR}.ear
mv  ${GLASSFISH_USER_HOME}/${EAR}/${EJB}.jar ${GLASSFISH_USER_HOME}/${EJB}

cd ${GLASSFISH_USER_HOME}/${EJB}
unzip ${EJB}.jar
rm ${EJB}.jar

MODEL_FILE=${GLASSFISH_USER_HOME}/${EJB}/META-INF/persistence.xml
cp ${GLASSFISH_USER_HOME}/dbc-glassfish.d/02-persistance.xml $MODEL_FILE
rm ${GLASSFISH_USER_HOME}/dbc-glassfish.d/02-persistance.xml

MODEL_FILE=${GLASSFISH_USER_HOME}/${EJB}/META-INF/ejb-jar.xml
cp ${GLASSFISH_USER_HOME}/dbc-glassfish.d/02-ejb-jar.xml $MODEL_FILE
rm ${GLASSFISH_USER_HOME}/dbc-glassfish.d/02-ejb-jar.xml

cd ${GLASSFISH_USER_HOME}/${EJB}

jar cmf META-INF/MANIFEST.MF ${EJB}.jar *
mv ${EJB}.jar ../${EAR}/

cd ${GLASSFISH_USER_HOME}/${EAR}

jar cmf META-INF/MANIFEST.MF ${EAR}.ear *
mv ${EAR}.ear ..
cd ..

rm -rf ${EAR} 
rm -rf ${EJB}

