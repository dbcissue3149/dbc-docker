#!/usr/bin/env bash
set -e
exec java -jar dataio-gatekeeper-1.0-SNAPSHOT.jar \
  --guarded-dir $GUARDED_DIR \
  --shadow-dir $SHADOW_DIR \
  --file-store-service-url http://dbc-glassfish-filestore:8080/dataio/file-store-service/ \
  --job-store-service-url http://dbc-glassfish-jobstore:8080/dataio/job-store-service/ \
  --flow-store-service-url http://dbc-glassfish-flowstore:8080/dataio/flow-store-service/
