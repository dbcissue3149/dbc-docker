#!/usr/bin/env bash

shopt -s nullglob
for func in $BASEDIR/src/resource/docker/test-suite/common-test-functions/*.function; do
    source $func
done

COMPOSE_FILE=$BASEDIR/src/main/docker/share/compose/$ARTIFACT_ID-compose.yml

#### CLEANUP
function cleanup
{
    echo Cleaning up
    # do whatever you have to do
    # if [ ! -z "$CONTAINER_ID" ]; then
    #     #docker logs $CONTAINER_ID
    #     docker rm -f $CONTAINER_ID
    # fi
}
trap cleanup EXIT
#### CLEANUP

set -o verbose
set -o errexit


docker-compose -f $COMPOSE_FILE up -d
CONTAINER_ID=$(docker-compose -f $COMPOSE_FILE ps -q $ARTIFACT_ID)
exitIfContainerDoesNotStart 10 $CONTAINER_ID
exitIfOpenMqIsNotRunning $CONTAINER_ID

