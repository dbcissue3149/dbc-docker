#!/usr/bin/env bash

# Start in background
nohup /home/mq/MessageQueue/mq/bin/imqbrokerd -vmargs "-Xms2000m -Xmx8000m -Xss1000m" -startRmiRegistry >$MQ_LOGS_DIR/log.txt &

# Wait until openmq is up
while ! echo exit | nc localhost $MQ_PORT; do sleep 2; done

# Configure. It re-started it will attempt to re-create the queues, but 
#            the re-creation will be rejected without failing.
/home/mq/MessageQueue/mq/bin/imqcmd create dst -n jmsDataioSinks -t q -o "maxNumMsgs=1000000" -o "limitBehavior=REMOVE_OLDEST" -o "useDMQ=true" -u admin -passfile /home/mq/scripts/passfile
/home/mq/MessageQueue/mq/bin/imqcmd create dst -n jmsDataioProcessor -t q -o "maxNumMsgs=1000000" -o "limitBehavior=REMOVE_OLDEST" -o "useDMQ=true" -u admin -passfile /home/mq/scripts/passfile

# Stop
$MQ_USER_HOME/scripts/stop-mq.sh $MQ_PORT

# Wait until openmq is down
while echo exit | nc localhost $MQ_PORT; do sleep 2; done

# Copy new config to default broker conf
#cp /home/mq/config/* /home/mq/MessageQueue/var/mq/instances/imqbroker/props

# Start in foreground
# nohup?
exec /home/mq/MessageQueue/mq/bin/imqbrokerd -vmargs "-Xms2000m -Xmx8000m -Xss1000m" -startRmiRegistry >$MQ_LOGS_DIR/log.txt

