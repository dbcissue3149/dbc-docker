#!/usr/bin/expect

set port [lindex $argv 0]

spawn /home/mq/MessageQueue/mq/bin/imqcmd shutdown bkr -b 127.0.0.1:$port -time 1 -u admin -passfile /home/mq/scripts/passfile
expect "Are you sure you want to shutdown"
send "y\n"
expect eof
exit
