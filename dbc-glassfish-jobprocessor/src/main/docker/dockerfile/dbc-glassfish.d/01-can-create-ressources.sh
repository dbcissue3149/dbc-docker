#!/usr/bin/env bash

set -e

# when the shell expands, dont include the pattern in the list of results (e_g_ exclude *_xml)
shopt -s nullglob

env




cat << EOF > ${GLASSFISH_USER_HOME}/dbc-glassfish.d/18-jndi.xml
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE resources PUBLIC "-//GlassFish.org//DTD GlassFish Application Server 3.1 Resource Definitions//EN" "http://glassfish.org/dtds/glassfish-resources_1_5.dtd">
<resources>
        <custom-resource 
                factory-class="org.glassfish.resources.custom.factory.PrimitivesAndStringFactory" 
                res-type="java.lang.String" 
                jndi-name="url/dataio/jobstore/rs">
                        <property name="value" value="http://dbc-glassfish-jobstore:8080/dataio/job-store-service"></property>
        </custom-resource>

	 <custom-resource 
                factory-class="org.glassfish.resources.custom.factory.PrimitivesAndStringFactory" 
                res-type="java.lang.String" 
                jndi-name="url/dataio/logstore/rs">
                        <property 
                        name="value" value="http://dbc-glassfish-logstore:8080/dataio/log-store-service">
                        </property>
        </custom-resource>

	 <custom-resource 
                factory-class="org.glassfish.resources.custom.factory.PrimitivesAndStringFactory" 
                res-type="java.lang.String" 
                jndi-name="url/dataio/filestore/rs">
                        <property 
                        name="value" value="http://dbc-glassfish-filestore:8080/dataio/file-store-service">
                        </property>
        </custom-resource>


</resources>
EOF
