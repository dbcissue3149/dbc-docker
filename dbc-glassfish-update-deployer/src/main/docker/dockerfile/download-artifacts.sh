#!/usr/bin/env bash

ARTIFACTS_DIR=artifacts

UPDATE_ARTIFACT_URL=http://is.dbc.dk/job/updateservice-head-guesstimate/lastSuccessfulBuild/artifact/deploy/updateservice-1.0.0.tgz
OPENCAT_BUSINESS_ARTIFACT_URL=http://is.dbc.dk/job/opencat-business-guesstimate-head/lastSuccessfulBuild/artifact/deploy/opencat-business.tgz

if [ -d "$ARTIFACTS_DIR" ]; then
    rm -rf $ARTIFACTS_DIR
fi

mkdir $ARTIFACTS_DIR

cd $ARTIFACTS_DIR
wget $UPDATE_ARTIFACT_URL $ARTIFACTS_DIR/.
wget $OPENCAT_BUSINESS_ARTIFACT_URL $ARTIFACTS_DIR/.
cd -
