#!/usr/bin/env bash

# find the directory of _this_ file
DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
source $DIR/exitIfNotZero.function
source $DIR/exitIfContainerDoesNotStart.function
source $DIR/exitIfPostgresIsNotRunning.function
source $DIR/exitIfDBDoesNotExist.function

docker build -t dbc-postgres dbc-postgres/
docker build -t dbc-postgres-jobstore dbc-postgres-jobstore/
CONTAINER_ID=$(docker run -d -P -e POSTGRES_USER=jobstore dbc-postgres-jobstore)
exitIfContainerDoesNotStart $CONTAINER_ID 10
exitIfPostgresIsNotRunning $CONTAINER_ID
exitIfDBDoesNotExist $CONTAINER_ID jobstore

#docker logs $CONTAINER_ID
