#!/usr/bin/env bash

# find the directory of _this_ file
DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
source $DIR/exitIfNotZero.function
source $DIR/exitIfContainerDoesNotStart.function
source $DIR/exitIfPostgresIsNotRunning.function

set -o verbose
set -e

docker build -t dummy dbc-postgres/
CONTAINER_ID=$(docker run -d -P -e POSTGRES_PASSWORD=mysecretpassword dummy)
exitIfContainerDoesNotStart $CONTAINER_ID 10
exitIfPostgresIsNotRunning $CONTAINER_ID 

# Kill it fast, so I can get on with the other tests
docker logs $CONTAINER_ID   
docker rm -f $CONTAINER_ID
docker rmi dummy

