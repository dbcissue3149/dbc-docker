#!/usr/bin/env bash

# find the directory of _this_ file
DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

declare -a arr=(
    "dbc-postgres" \
	"dbc-postgres-flowstore" \
	"dbc-postgres-jobstore" \
)

set -o verbose
set -e

for dockerfile in "${arr[@]}"
do
	echo "Building" $dockerfile
	docker build -t $dockerfile $dockerfile
done



