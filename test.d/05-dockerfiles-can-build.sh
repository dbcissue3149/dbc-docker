#!/usr/bin/env bash

DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
source $DIR/exitIfNotZero.function

# Added boolean if, to make this work when Im not on DBC network
if true ; then
    declare -a arr=(
						    "dbc-postgres" \
							    "dbc-postgres-flowstore" \
							    "dbc-postgres-jobstore" \
				    )
else
declare -a arr=(
                "dbc-debian" \
					"dbc-java8" \
						"dbc-glassfish" \
							"dbc-glassfish-logstore" \
							"dbc-glassfish-gui" \
							"dbc-glassfish-flowstore" \
							"dbc-glassfish-jobstore" \
							"dbc-glassfish-filestore" \
						"dbc-postgres" \
							"dbc-postgres-flowstore" \
							"dbc-postgres-jobstore" \
						"dbc-openmq"
				)

fi

for dockerfile in "${arr[@]}"
do
    docker build -t dummy $dockerfile
    exitIfNotZero $?
done

docker rmi -f dummy

