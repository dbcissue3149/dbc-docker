#!/usr/bin/env bash
docker build --no-cache --rm=true -t dbc-glassfish-update .
docker run --name dbc-glassfish-update1 --rm --env-file=../env-update1.env -p 4848:4848 -p 8080:8080 dbc-glassfish-update
