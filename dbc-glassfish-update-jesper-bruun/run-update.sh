#!/usr/bin/env bash
echo "Stopping existing containers"

docker stop dbc-glassfish-update1 || true
docker rm dbc-glassfish-update1 || true

docker stop dbc-glassfish-update2 || true
docker rm dbc-glassfish-update2 || true


docker rmi docker-index:5000/dbc-glassfish-update:latest
docker pull docker-index:5000/dbc-glassfish-update:latest
echo "Staring containers"
nohup docker run --name dbc-glassfish-update1 --rm -v /cmdbdata:/tmp -v /data/dataio/docker/serverlogs:/usr/local/glassfish4/glassfish/domains/domain1/logs -v /data/dataio/docker/gf-logs:/data/gf-logs --env-file=/data/dataio/docker/env-update1.env -p 4852:4848 -p 8092:8080 -p 1868:1868 docker-index:5000/dbc-glassfish-update:latest >/data/dataio/docker/update.log 2>&1 &
nohup docker run --name dbc-glassfish-update2 --rm -v /cmdbdata:/tmp -v /data/dataio/docker/serverlogs:/usr/local/glassfish4/glassfish/domains/domain1/logs -v /data/dataio/docker/gf-logs:/data/gf-logs --env-file=/data/dataio/docker/env-update2.env -p 4853:4848 -p 8093:8080 -p 1869:1868 docker-index:5000/dbc-glassfish-update:latest >/data/dataio/docker/update.log 2>&1 &

