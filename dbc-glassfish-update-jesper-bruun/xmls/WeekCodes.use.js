//------------------------------------------------------------------------------------------------------
/*!
    \file weekcodes.js
    
    \Tilretninger (skriv nyeste �verst):
    \20110609/hue: KODE_IDA udvidet med *xDAR + KODE_IDR udvidet med *xDAN
*/

//------------------------------------------------------------------------------------------------------

use( "Log" );
use( "UnitTest" );

//------------------------------------------------------------------------------------------------------
EXPORTED_SYMBOLS = [ 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday',
                     'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December',
                     'createDate', 'AbstractWeekCalculator', 'WeekCalculator', 'FixedWeekCalculator', 'WeekModel', 'testCode' ];

//------------------------------------------------------------------------------------------------------
//              Constants
//------------------------------------------------------------------------------------------------------

var Monday = 1;
var Tuesday = 2;
var Wednesday = 3;
var Thursday = 4;
var Friday = 5;
var Saturday = 6;
var Sunday = 7;

var January = 1;
var February = 2;
var March = 3;
var April = 4;
var May = 5;
var June = 6;
var July = 7;
var August = 8;
var September = 9;
var October = 10;
var November = 11;
var December = 12;

//------------------------------------------------------------------------------------------------------
//              Global functions
//------------------------------------------------------------------------------------------------------

function createDate( date, month, year, switchDay ) {
    var newDate = new QDate( date, month, year );
    
    if( switchDay != undefined ) {
        newDate = newDate.addDays( switchDay - Monday );
    };
    
    return newDate;
};

//------------------------------------------------------------------------------------------------------
//              JavaScript Classes
//------------------------------------------------------------------------------------------------------
function AbstractWeekCalculator() {
    this.startDate = undefined;
};

AbstractWeekCalculator.prototype.match = function( date ) {
    if( this.startDate == undefined ) {
        return false;
    };
        
    return this.startDate.toJulianDay() <= date.toJulianDay();
};

AbstractWeekCalculator.prototype.calcWeek = function( date ) {
    throw "AbstractWeekCalculator::calcWeek( date ) is abstract.";
};

AbstractWeekCalculator.prototype.calcMonth = function( date ) {
    throw "AbstractWeekCalculator::calcMonth( date ) is abstract.";
};

AbstractWeekCalculator.prototype.calcYear = function( date ) {
    throw "AbstractWeekCalculator::calcYear( date ) is abstract.";
};

//------------------------------------------------------------------------------------------------------
WeekCalculator.prototype = new AbstractWeekCalculator();

function WeekCalculator() {
    this.switchDay = undefined;
    this.adjustWeeks = undefined;
};

WeekCalculator.prototype.calcDate = function( date ) {
    var newDate = createDate( date.day(), date.month(), date.year() );
    
    if( this.switchDay != undefined ) {
        if( newDate.dayOfWeek() >= this.switchDay ) {
            newDate = newDate.addDays( 7 - newDate.dayOfWeek() + 1 );
        };
    };

    if( this.adjustWeeks != undefined ) {
        newDate = newDate.addDays( this.adjustWeeks * 7 );
    };
    
    return newDate;
};

WeekCalculator.prototype.calcWeek = function( date ) {
    return this.calcDate( date ).weekNumber();
};

WeekCalculator.prototype.calcMonth = function( date ) {
    return this.calcDate( date ).month();
};

WeekCalculator.prototype.calcYear = function( date ) {
    return this.calcDate( date ).weekNumberYear();
};

//------------------------------------------------------------------------------------------------------
FixedWeekCalculator.prototype = new AbstractWeekCalculator();

function FixedWeekCalculator( weekNo, month, year ) {
    this.weekNo = weekNo;
    this.month = month;
    this.year = year;
};

FixedWeekCalculator.prototype.calcWeek = function( date ) {
    return this.weekNo;
};

FixedWeekCalculator.prototype.calcMonth = function( date ) {
    return this.month;
};

FixedWeekCalculator.prototype.calcYear = function( date ) {
    return this.year;
};

//------------------------------------------------------------------------------------------------------
function WeekModel() {
    this.weekCalcs = new Array();
};

WeekModel.prototype.addWeekCalc = function( startDate, switchDay, adjustWeeks ) {
    var weekCalc = new WeekCalculator();
    
    weekCalc.startDate = startDate;
    weekCalc.switchDay = switchDay;
    weekCalc.adjustWeeks = adjustWeeks;
    
    this.weekCalcs.push( weekCalc );
};

WeekModel.prototype.addFixedWeekCalc = function( startDate, fixedWeek, fixedMonth, fixedYear ) {
    var weekCalc = new FixedWeekCalculator( fixedWeek, fixedMonth, fixedYear );
    
    weekCalc.startDate = startDate;
    
    this.weekCalcs.push( weekCalc );
};

WeekModel.prototype.findWeekCalc = function( date ) {
    Log.trace( "Enter - WeekModel.findWeekCalc( ", date.toString(), " )" );
    var res = undefined;

    try {
        for( var i = 0; i < this.weekCalcs.length; i++ ) {
            var weekCalc = this.weekCalcs[i];

            if( !weekCalc.match( date ) ) {
                continue;
            }

            if( res == undefined || res.startDate.toJulianDay() < weekCalc.startDate.toJulianDay() ) {
                res = weekCalc;
            }
        }

        if (res != undefined) {
            Log.debug( "Found weekCalc: ", res.startDate.toString() );

            return res;
        }

        throw "Der findes ingen ugeberegning for datoen: " + date;
    }
    finally {
        Log.trace( "Exit - WeekModel.findWeekCalc()" );
    }
};

WeekModel.prototype.calcWeek = function( date ) {
    return this.findWeekCalc( date ).calcWeek( date );
};

WeekModel.prototype.calcMonth = function( date ) {
    return this.findWeekCalc( date ).calcMonth( date );
};

WeekModel.prototype.calcYear = function( date ) {
    return this.findWeekCalc( date ).calcYear( date );
};

WeekModel.prototype.createProductionModel = function( switchDay, adjustWeeks ) {
    var date;
    
    this.weekCalcs = new Array();
    
    this.addWeekCalc( createDate( 1, January, 2008 ), switchDay, adjustWeeks );
    
    date = createDate( 10, March, 2008, switchDay );
    date = date.addDays( adjustWeeks * -7 );
    this.addWeekCalc( date, switchDay, adjustWeeks + 1 );
    
    date = createDate( 31, March, 2008, switchDay );
    date = date.addDays( adjustWeeks * -7 );
    this.addWeekCalc( date, switchDay, adjustWeeks );
    
    var newSwitchDay = switchDay;
    if( newSwitchDay > Thursday ) {
        newSwitchDay = Thursday;
    };
    this.addWeekCalc( createDate( 14, April, 2008, newSwitchDay ), newSwitchDay, adjustWeeks );   
    this.addWeekCalc( createDate( 15, April, 2008, switchDay ), switchDay, adjustWeeks );
    
    date = createDate( 15, December, 2008, switchDay );
    date = date.addDays( adjustWeeks * -7 );
    this.addWeekCalc( date, switchDay, adjustWeeks + 1 );    

    date = createDate( 5, January, 2009, switchDay );
    date = date.addDays( adjustWeeks * -7 );
    this.addWeekCalc( date, switchDay, adjustWeeks );    

    date = createDate( 30, March, 2009, switchDay );
    date = date.addDays( adjustWeeks * -7 );
    this.addWeekCalc( date, switchDay, adjustWeeks + 1 );    

    date = createDate( 20, April, 2009, switchDay );
    date = date.addDays( adjustWeeks * -7 );
    this.addWeekCalc( date, switchDay, adjustWeeks );

    this.addWeekCalc( createDate( 27, April, 2009, newSwitchDay ), newSwitchDay, adjustWeeks );   
    this.addWeekCalc( createDate( 28, April, 2009, switchDay ), switchDay, adjustWeeks );

    this.addWeekCalc( createDate( 4, May, 2009, newSwitchDay ), newSwitchDay, adjustWeeks );   
    this.addWeekCalc( createDate( 5, May, 2009, switchDay ), switchDay, adjustWeeks );

    this.addWeekCalc( createDate( 1, June, 2009, newSwitchDay ), newSwitchDay, adjustWeeks );   
    this.addWeekCalc( createDate( 2, June, 2009, switchDay ), switchDay, adjustWeeks );

    date = createDate( 14, December, 2009, switchDay );
    date = date.addDays( adjustWeeks * -7 );
    this.addWeekCalc( date, switchDay, adjustWeeks + 2 );    

    date = createDate( 28, December, 2009, switchDay );
    date = date.addDays( adjustWeeks * -7 );
    this.addFixedWeekCalc( date, 1 + adjustWeeks, 01, 2010 );    

    this.addWeekCalc( createDate( 4, January, 2010, switchDay ), switchDay, adjustWeeks );
    
    switch( adjustWeeks ) {
    case 1:
        this.addWeekCalc( createDate( 15, March, 2010, switchDay ), switchDay, adjustWeeks + 1 );
        this.addWeekCalc( createDate( 29, March, 2010, switchDay ), switchDay, adjustWeeks );
	break;

    case 2:
        this.addWeekCalc( createDate( 8, March, 2010, switchDay ), switchDay, adjustWeeks + 1 );
        this.addWeekCalc( createDate( 29, March, 2010, switchDay ), switchDay, adjustWeeks );
	break;

    default:
        break;
    };
    
    this.addWeekCalc( createDate( 26, April, 2010, newSwitchDay ), newSwitchDay, adjustWeeks );   
    this.addWeekCalc( createDate( 27, April, 2010, switchDay ), switchDay, adjustWeeks );
    
    switch( adjustWeeks ) {
    case 0:
        this.addWeekCalc( createDate( 20, December, 2010, switchDay ), switchDay, adjustWeeks + 1 );
        this.addWeekCalc( createDate( 20, December, 2010, switchDay ), switchDay, adjustWeeks );
        break;
        
    case 1:
        this.addWeekCalc( createDate( 13, December, 2010, switchDay ), switchDay, adjustWeeks + 1 );
        this.addWeekCalc( createDate( 20, December, 2010, switchDay ), switchDay, adjustWeeks );
        break;
        
    case 2:
        this.addWeekCalc( createDate( 6, December, 2010, switchDay ), switchDay, adjustWeeks + 1 );
        this.addWeekCalc( createDate( 20, December, 2010, switchDay ), switchDay, adjustWeeks );
        break;
        
    default:
        break;
        
    }
    
    this.addWeekCalc( createDate( 27, December, 2010, newSwitchDay ), newSwitchDay, adjustWeeks );   
    this.addWeekCalc( createDate( 28, December, 2010, switchDay ), switchDay, adjustWeeks );

    switch( adjustWeeks ) {
    case 0:
        this.addWeekCalc( createDate( 11, April, 2011, switchDay ), switchDay, adjustWeeks + 1 );
        this.addWeekCalc( createDate( 18, April, 2011, switchDay ), switchDay, adjustWeeks );
        this.addWeekCalc( createDate( 16, May, 2011, newSwitchDay ), newSwitchDay, adjustWeeks );
        this.addWeekCalc( createDate( 17, May, 2011, switchDay ), switchDay, adjustWeeks );
        break;

    case 1:
        this.addWeekCalc( createDate( 4, April, 2011, switchDay ), switchDay, adjustWeeks + 1 );
        this.addWeekCalc( createDate( 18, April, 2011, switchDay ), switchDay, adjustWeeks );
        this.addWeekCalc( createDate( 16, May, 2011, newSwitchDay ), newSwitchDay, adjustWeeks );
        this.addWeekCalc( createDate( 17, May, 2011, switchDay ), switchDay, adjustWeeks );
        this.addWeekCalc( createDate( 12, December, 2011, switchDay ), switchDay, adjustWeeks + 1 );
        this.addWeekCalc( createDate( 19, December, 2011, switchDay ), switchDay, adjustWeeks );
        break;

    case 2:
        this.addWeekCalc( createDate( 28, March, 2011, switchDay ), switchDay, adjustWeeks + 1 );
        this.addWeekCalc( createDate( 18, April, 2011, switchDay ), switchDay, adjustWeeks );
        this.addWeekCalc( createDate( 16, May, 2011, newSwitchDay ), newSwitchDay, adjustWeeks );
        this.addWeekCalc( createDate( 17, May, 2011, switchDay ), switchDay, adjustWeeks );
        this.addWeekCalc( createDate( 5, December, 2011, switchDay ), switchDay, adjustWeeks + 1 );
        this.addWeekCalc( createDate( 19, December, 2011, switchDay ), switchDay, adjustWeeks );
        break;

    default:
        break;
    }

    switch( adjustWeeks ) {
    case 1:
        this.addWeekCalc( createDate( 12, March, 2012, switchDay ), switchDay, adjustWeeks + 1 );
        this.addWeekCalc( createDate( 2, April, 2012, switchDay ), switchDay, adjustWeeks );
	break;

    case 2:
        this.addWeekCalc( createDate( 12, March, 2012, switchDay ), switchDay, adjustWeeks + 1 );
        this.addWeekCalc( createDate( 2, April, 2012, switchDay ), switchDay, adjustWeeks );
	break;

    default:
        break;
    };

    this.addWeekCalc( createDate( 30, April, 2012, newSwitchDay ), newSwitchDay, adjustWeeks );   
    this.addWeekCalc( createDate( 1, May, 2012, switchDay ), switchDay, adjustWeeks );

    switch( adjustWeeks ) {
    case 1:
        this.addWeekCalc( createDate( 3, December, 2012, switchDay ), switchDay, adjustWeeks + 1 );
        this.addWeekCalc( createDate( 24, December, 2012, switchDay ), switchDay, adjustWeeks );
	break;

    case 2:
        this.addWeekCalc( createDate( 3, December, 2012, switchDay ), switchDay, adjustWeeks + 1 );
        this.addWeekCalc( createDate( 24, December, 2012, switchDay ), switchDay, adjustWeeks );
	break;

    default:
        break;
    };

    switch( adjustWeeks ) {
    case 1:
        this.addWeekCalc( createDate( 4, March, 2013, switchDay ), switchDay, adjustWeeks + 1 );
        this.addWeekCalc( createDate( 25, March, 2013, switchDay ), switchDay, adjustWeeks );
	break;

    case 2:
        this.addWeekCalc( createDate( 4, March, 2013, switchDay ), switchDay, adjustWeeks + 1 );
        this.addWeekCalc( createDate( 25, March, 2013, switchDay ), switchDay, adjustWeeks );
	break;

    default:
        break;
    };

    this.addWeekCalc( createDate( 22, April, 2013, newSwitchDay ), newSwitchDay, adjustWeeks );   
    this.addWeekCalc( createDate( 23, April, 2013, switchDay ), switchDay, adjustWeeks );

    switch( adjustWeeks ) {
    case 1:
        this.addWeekCalc( createDate( 2, December, 2013, switchDay ), switchDay, adjustWeeks + 1 );
        this.addWeekCalc( createDate( 23, December, 2013, switchDay ), switchDay, adjustWeeks );
	break;

    case 2:
        this.addWeekCalc( createDate( 2, December, 2013, switchDay ), switchDay, adjustWeeks + 1 );
        this.addWeekCalc( createDate( 23, December, 2013, switchDay ), switchDay, adjustWeeks );
	break;
	};
	
// 2014
switch( adjustWeeks ) {
    case 1:
        this.addWeekCalc( createDate( 31, March, 2014, switchDay ), switchDay, adjustWeeks + 1 );
        this.addWeekCalc( createDate( 14, April, 2014, switchDay ), switchDay, adjustWeeks );
	break;

    case 2:
        this.addWeekCalc( createDate( 31, March, 2014, switchDay ), switchDay, adjustWeeks + 1 );
        this.addWeekCalc( createDate( 14, April, 2014, switchDay ), switchDay, adjustWeeks );
	break;

    default:
        break;
    };

    this.addWeekCalc( createDate( 12, May, 2014, newSwitchDay ), newSwitchDay, adjustWeeks );   
    this.addWeekCalc( createDate( 13, May, 2014, switchDay ), switchDay, adjustWeeks );

    switch( adjustWeeks ) {
    case 1:
        this.addWeekCalc( createDate( 1, December, 2014, switchDay ), switchDay, adjustWeeks + 1 );
        this.addWeekCalc( createDate( 22, December, 2014, switchDay ), switchDay, adjustWeeks );
	break;

    case 2:
        this.addWeekCalc( createDate( 1, December, 2014, switchDay ), switchDay, adjustWeeks + 1 );
        this.addWeekCalc( createDate( 22, December, 2014, switchDay ), switchDay, adjustWeeks );
	break;

    default:
        break;
    };

// 2015
switch( adjustWeeks ) {
    case 1:
        this.addWeekCalc( createDate( 16, March, 2015, switchDay ), switchDay, adjustWeeks + 1 );
        this.addWeekCalc( createDate( 30, March, 2015, switchDay ), switchDay, adjustWeeks );
	break;

    case 2:
        this.addWeekCalc( createDate( 16, March, 2015, switchDay ), switchDay, adjustWeeks + 1 );
        this.addWeekCalc( createDate( 30, March, 2015, switchDay ), switchDay, adjustWeeks );
	break;

    default:
        break;
    };
// Bededag
    newSwitchDay = switchDay;
    if( newSwitchDay > Thursday ) {
        newSwitchDay = Thursday;
    };

    this.addWeekCalc( createDate( 27, April, 2015, newSwitchDay ), newSwitchDay, adjustWeeks ); 	
    this.addWeekCalc( createDate( 28, April, 2015, switchDay ), switchDay, adjustWeeks ); 	
// Kr. Himmelfart skift onsdag
	
	newSwitchDay = switchDay;
    if( newSwitchDay > Wednesday ) {
        newSwitchDay = Wednesday;
    };
	this.addWeekCalc( createDate( 11, May, 2015, newSwitchDay ), newSwitchDay, adjustWeeks );   	
    this.addWeekCalc( createDate( 12, May, 2015, switchDay ), switchDay, adjustWeeks );
	
// 21.5. skift torsdag foer pinse pga bogvogn
	this.addWeekCalc( createDate( 18, May, 2015, newSwitchDay ), newSwitchDay,  adjustWeeks );   
    this.addWeekCalc( createDate( 25, May, 2015, switchDay ), switchDay, adjustWeeks );
	
// Grundlovsdag
	this.addWeekCalc( createDate( 1, June, 2015, newSwitchDay ), newSwitchDay,  adjustWeeks );   
    this.addWeekCalc( createDate( 8, June, 2015, switchDay ), switchDay, adjustWeeks );
	
// Nyt�r - med uge 53.	
    switch( adjustWeeks ) {
    case 1:
        this.addWeekCalc( createDate( 30, November, 2015, switchDay ), switchDay, adjustWeeks + 1 );
        this.addWeekCalc( createDate( 4, January, 2016, switchDay ), switchDay, adjustWeeks );
	break;

    case 2:
        this.addWeekCalc( createDate( 30, November, 2015, switchDay ), switchDay, adjustWeeks + 2 );
        this.addWeekCalc( createDate( 21, December, 2015, switchDay ), switchDay, adjustWeeks + 1 );
        this.addWeekCalc( createDate( 4, January, 2016, switchDay ), switchDay, adjustWeeks );
	break;

    default:
        break;
    };



// 2016
switch( adjustWeeks ) {
    //paaske
	case 1:
        this.addWeekCalc( createDate( 7, March, 2016, switchDay ), switchDay, adjustWeeks + 1 );
        this.addWeekCalc( createDate( 21, March, 2016, switchDay ), switchDay, adjustWeeks );
	break;

    case 2:
        this.addWeekCalc( createDate( 7, March, 2016, switchDay ), switchDay, adjustWeeks + 1 );
        this.addWeekCalc( createDate( 21, March, 2016, switchDay ), switchDay, adjustWeeks );
	break;

    default:
        break;
    };
// Bededag
    newSwitchDay = switchDay;
    if( newSwitchDay > Thursday ) {
        newSwitchDay = Thursday;
    };

    this.addWeekCalc( createDate( 18, April, 2016, newSwitchDay ), newSwitchDay, adjustWeeks ); 	
    this.addWeekCalc( createDate( 19, April, 2016, switchDay ), switchDay, adjustWeeks ); 	
// Kr. Himmelfart skift onsdag
	
	newSwitchDay = switchDay;
    if( newSwitchDay > Wednesday ) {
        newSwitchDay = Wednesday;
    };
	this.addWeekCalc( createDate( 2, May, 2016, newSwitchDay ), newSwitchDay, adjustWeeks );   	
    this.addWeekCalc( createDate( 3, May, 2016, switchDay ), switchDay, adjustWeeks );
	
// 12.5. skift torsdag foer pinse pga bogvogn
	this.addWeekCalc( createDate( 9, May, 2016, newSwitchDay ), newSwitchDay,  adjustWeeks );   
    this.addWeekCalc( createDate( 16, May, 2016, switchDay ), switchDay, adjustWeeks );
	
// Grundlovsdag - ikke relevant soendag
	//this.addWeekCalc( createDate( 1, June, 2015, newSwitchDay ), newSwitchDay,  adjustWeeks );   
    //this.addWeekCalc( createDate( 8, June, 2015, switchDay ), switchDay, adjustWeeks );
	
// Nyt�r 	
    switch( adjustWeeks ) {
    case 1:
        this.addWeekCalc( createDate( 5, December, 2016, switchDay ), switchDay, adjustWeeks + 1 );
        this.addWeekCalc( createDate( 2, January, 2017, switchDay ), switchDay, adjustWeeks );
	break;

    case 2:
        this.addWeekCalc( createDate( 5, December, 2016, switchDay ), switchDay, adjustWeeks + 1 );
        this.addWeekCalc( createDate( 2, January, 2017, switchDay ), switchDay, adjustWeeks );
	break;

    default:
        break;
    };

};

WeekModel.prototype.createSimpleModel = function( switchDay, adjustWeeks ) {
    this.weekCalcs = new Array();
    
    this.addWeekCalc( createDate( 1, January, new QDate().year() ), switchDay, adjustWeeks );       
};

WeekModel.prototype.numberToStr = function( no ) {
    var prefix = "";
    
    if( no < 0 )
        return "<Ukendt>";
    
    if( no < 10 )
        prefix = "0";
        
    return prefix + no;
};

WeekModel.prototype.getCode = function( code, date ) {
    var week;
    var month;
    var year;

    switch( code ) {
    case "DBF_SFD":
        this.createProductionModel( Friday, 2 );
        
        week = this.numberToStr( this.calcWeek( date ) );
        month = this.numberToStr( this.calcMonth( date ) );
        year = this.numberToStr( this.calcYear( date ) );
        
        return "DBF" + year + week + " *xBKM" + year + week;
        
    case "DBF_SFDA":
        this.createProductionModel( Friday, 2 );
        
        week = this.numberToStr( this.calcWeek( date ) );
        month = this.numberToStr( this.calcMonth( date ) );
        year = this.numberToStr( this.calcYear( date ) );
        
        return "DBF" + year + week;
        
    case "DBI_BKM":
        this.createProductionModel( Friday, 2 );
        
        week = this.numberToStr( this.calcWeek( date ) );
        month = this.numberToStr( this.calcMonth( date ) );
        year = this.numberToStr( this.calcYear( date ) );
        
        return "DBI" + year + week + " *xBKM" + year + week;
    
    case "DLF_SFA":
        this.createProductionModel( Friday, 2 );
        
        week = this.numberToStr( this.calcWeek( date ) );
        month = this.numberToStr( this.calcMonth( date ) );
        year = this.numberToStr( this.calcYear( date ) );
        
        return "DLF" + year + week + " *xBKM" + year + week;
    
    case "KODE_DAN":
        this.createSimpleModel( Saturday, 1 );
        
        week = this.numberToStr( this.calcWeek( date ) );
        month = this.numberToStr( this.calcMonth( date ) );
        year = this.numberToStr( this.calcYear( date ) );
        
        return "DAN" + year + week;
    
    case "KODE_DAR":
        this.createSimpleModel( Saturday, 1 );
        
        week = this.numberToStr( this.calcWeek( date ) );
        month = this.numberToStr( this.calcMonth( date ) );
        year = this.numberToStr( this.calcYear( date ) );
        
        return "DAR" + year + week;
    
    case "KODE_DAT":
	var switchDay = Friday;
	var newSwitchDay = Thursday;
	var adjustWeeks = 1;

    	this.addWeekCalc( createDate( 2, June, 2009, switchDay ), switchDay, adjustWeeks );

	var dt;
    	dt = createDate( 14, December, 2009, switchDay );
    	dt = dt.addDays( adjustWeeks * -7 );
    	this.addWeekCalc( dt, switchDay, adjustWeeks + 2 );    

    	dt = createDate( 28, December, 2009, switchDay );
    	dt = dt.addDays( adjustWeeks * -7 );
    	this.addFixedWeekCalc( dt, 1 + adjustWeeks, 01, 2010 );

  	this.addWeekCalc( createDate( 4, January, 2010, switchDay ), switchDay, adjustWeeks );
    	this.addWeekCalc( createDate( 26, April, 2010, newSwitchDay ), newSwitchDay, adjustWeeks );   
    	this.addWeekCalc( createDate( 27, April, 2010, switchDay ), switchDay, adjustWeeks );
    
    	switch( adjustWeeks ) {
    	case 0:
            this.addWeekCalc( createDate( 20, December, 2010, switchDay ), switchDay, adjustWeeks + 1 );
            this.addWeekCalc( createDate( 20, December, 2010, switchDay ), switchDay, adjustWeeks );
            break;
        
    	case 1:
            this.addWeekCalc( createDate( 13, December, 2010, switchDay ), switchDay, adjustWeeks + 1 );
            this.addWeekCalc( createDate( 20, December, 2010, switchDay ), switchDay, adjustWeeks );
            break;
        
    	case 2:
            this.addWeekCalc( createDate( 6, December, 2010, switchDay ), switchDay, adjustWeeks + 1 );
            this.addWeekCalc( createDate( 20, December, 2010, switchDay ), switchDay, adjustWeeks );
            break;
        
    	default:
            break;
        
        }
    
        this.addWeekCalc( createDate( 27, December, 2010, newSwitchDay ), newSwitchDay, adjustWeeks );   
        this.addWeekCalc( createDate( 28, December, 2010, switchDay ), switchDay, adjustWeeks );
        
        week = this.numberToStr( this.calcWeek( date ) );
        month = this.numberToStr( this.calcMonth( date ) );
        year = this.numberToStr( this.calcYear( date ) );
        
        return "DAT" + year + week;
    
    case "KODE_EMA":
        this.createProductionModel( Saturday, 2 );
        
        week = this.numberToStr( this.calcWeek( date ) );
        month = this.numberToStr( this.calcMonth( date ) );
        year = this.numberToStr( this.calcYear( date ) );
        
        return "EMO" + year + week;
    
    case "KODE_EMB":
        this.createProductionModel( Saturday, 2 );
        
        week = this.numberToStr( this.calcWeek( date ) );
        month = this.numberToStr( this.calcMonth( date ) );
        year = this.numberToStr( this.calcYear( date ) );
        
        return "EMO" + year + week;
    
    case "KODE_EMO":
        this.createProductionModel( Saturday, 2 );
        
        week = this.numberToStr( this.calcWeek( date ) );
        month = this.numberToStr( this.calcMonth( date ) );
        year = this.numberToStr( this.calcYear( date ) );
        
        return "EMO" + year + week;
    
    case "KODE_EMONY":
        this.createProductionModel( Saturday, 2 );
        
        week = this.numberToStr( this.calcWeek( date ) );
        month = this.numberToStr( this.calcMonth( date ) );
        year = this.numberToStr( this.calcYear( date ) );
        
        return "EMO" + year + week;
    
    case "KODE_EMS":
        this.createProductionModel( Saturday, 2 );
        
        week = this.numberToStr( this.calcWeek( date ) );
        month = this.numberToStr( this.calcMonth( date ) );
        year = this.numberToStr( this.calcYear( date ) );
        
        return "EMS" + year + week;
    
    case "KODE_EMT":
        this.createProductionModel( Saturday, 2 );
        
        week = this.numberToStr( this.calcWeek( date ) );
        month = this.numberToStr( this.calcMonth( date ) );
        year = this.numberToStr( this.calcYear( date ) );
        
        return "EMO" + year + week;
    
    case "KODE_ESE":
        this.createProductionModel( Saturday, 1 );
        
        week = this.numberToStr( this.calcWeek( date ) );
        month = this.numberToStr( this.calcMonth( date ) );
        year = this.numberToStr( this.calcYear( date ) );
        
        return "ESE" + year + week;
    
    case "KODE_ESO":
        this.createProductionModel( Saturday, 1 );
        
        week = this.numberToStr( this.calcWeek( date ) );
        month = this.numberToStr( this.calcMonth( date ) );
        year = this.numberToStr( this.calcYear( date ) );
        
        return "ESO" + year + week;
    
    case "KODE_FLX":
        this.createProductionModel( Friday, 2 );
        
        week = this.numberToStr( this.calcWeek( date ) );
        month = this.numberToStr( this.calcMonth( date ) );
        year = this.numberToStr( this.calcYear( date ) );
        
        return "FLX" + year + week;
    
	case "KODE_ACC":
        this.createSimpleModel( Friday, 0 );
        
        week = this.numberToStr( this.calcWeek( date ) );
        month = this.numberToStr( this.calcMonth( date ) );
        year = this.numberToStr( this.calcYear( date ) );
        
        return "ACC" + year + week;
    
    case "KODE_IDA":
        this.createSimpleModel( Saturday, 1 );
        
        week = this.numberToStr( this.calcWeek( date ) );
        month = this.numberToStr( this.calcMonth( date ) );
        year = this.numberToStr( this.calcYear( date ) );
        
        return "IDA" + year + week;
    
    case "KODE_IDA_DAR":
        this.createSimpleModel( Saturday, 1 );
        
        week = this.numberToStr( this.calcWeek( date ) );
        month = this.numberToStr( this.calcMonth( date ) );
        year = this.numberToStr( this.calcYear( date ) );
        
        return "DAR" + year + week + " *xIDA" + year + week;
    
    case "KODE_IDO":
        this.createProductionModel( Friday, 1 );
        
        week = this.numberToStr( this.calcWeek( date ) );
        month = this.numberToStr( this.calcMonth( date ) );
        year = this.numberToStr( this.calcYear( date ) );
        
        return "IDO" + year + week;
    
    case "KODE_IDR":
        this.createSimpleModel( Saturday, 1 );
        
        week = this.numberToStr( this.calcWeek( date ) );
        month = this.numberToStr( this.calcMonth( date ) );
        year = this.numberToStr( this.calcYear( date ) );
        
        return "IDR" + year + week;
    
    case "KODE_IDR_DAN":
        this.createSimpleModel( Saturday, 1 );
        
        week = this.numberToStr( this.calcWeek( date ) );
        month = this.numberToStr( this.calcMonth( date ) );
        year = this.numberToStr( this.calcYear( date ) );
        
        return "DAN" + year + week + " *xIDR" + year + week;
    
    case "KODE_KBP":
        this.createProductionModel( Saturday, 1 );
        
        week = this.numberToStr( this.calcWeek( date ) );
        month = this.numberToStr( this.calcMonth( date ) );
        year = this.numberToStr( this.calcYear( date ) );
        
        return "KBP" + year + week;
    
    case "KODE_LIT":
        this.createProductionModel( Friday, 2 );
        
        week = this.numberToStr( this.calcWeek( date ) );
        month = this.numberToStr( this.calcMonth( date ) );
        year = this.numberToStr( this.calcYear( date ) );
        
        return "LIT" + year + week;
    
	    case "KODE_MAT":
        this.createProductionModel( Friday, 2 );
        
        week = this.numberToStr( this.calcWeek( date ) );
        month = this.numberToStr( this.calcMonth( date ) );
        year = this.numberToStr( this.calcYear( date ) );
        
        return year + week;
    
    case "KODE_PLA":
        this.createProductionModel( Saturday, 0 );
        
        week = this.numberToStr( this.calcWeek( date ) );
        month = this.numberToStr( this.calcMonth( date ) );
        year = this.numberToStr( this.calcYear( date ) );
        
        return "PLA9906" + week;

    case "KODE_PLN":
        this.createProductionModel( Saturday, 0 );

        week = this.numberToStr( this.calcWeek( date ) );
        month = this.numberToStr( this.calcMonth( date ) );
        year = this.numberToStr( this.calcYear( date ) );

        return "PLN9906" + week;

    case "KODE_REK":
        this.createProductionModel( Saturday, 0 );
        
        week = this.numberToStr( this.calcWeek( date ) );
        month = this.numberToStr( this.calcMonth( date ) );
        year = this.numberToStr( this.calcYear( date ) );
        
        return "REK" + year;
    
	case "KODE_SBA":
        this.createProductionModel( Saturday, 1  );
        
        week = this.numberToStr( this.calcWeek( date ) );
        month = this.numberToStr( this.calcMonth( date ) );
        year = this.numberToStr( this.calcYear( date ) );
        
        return "SBA" + year + week;
	
    case "KODE_SBP":
        this.createProductionModel( Saturday, 1 );
        
        week = this.numberToStr( this.calcWeek( date ) );
        month = this.numberToStr( this.calcMonth( date ) );
        year = this.numberToStr( this.calcYear( date ) );
        
        return "SBP" + year + week;
        
    case "KODE_VPL":
        this.createProductionModel( Saturday, 0 );
        
        week = this.numberToStr( this.calcWeek( date ) );
        month = this.numberToStr( this.calcMonth( date ) );
        year = this.numberToStr( this.calcYear( date ) );
        
        return "VPL" + year + month;
    
    case "KODE_VPR":
        this.createProductionModel( Saturday, 0 );
        
        week = this.numberToStr( this.calcWeek( date ) );
        month = this.numberToStr( this.calcMonth( date ) );
        year = this.numberToStr( this.calcYear( date ) );
        
        return "VPR" + year + month;
    
    default:
        return "Ukendt ugekode: " + code;
    };

};

//------------------------------------------------------------------------------------------------------
//              Tests
//------------------------------------------------------------------------------------------------------

function testCode() {
    var model = new WeekModel();
    model.createSimpleModel( Friday, 0 );
    
    var startDate = createDate( 1, November, 2012 );
    var endDate = createDate( 31, January, 2017 );
    
    var codes = [ "KODE_IDO" ];
    codes.forEach( function(code, index, array) {
        print( "\n" );
        print( "Test af ugekoden " + code + ":\n" );
        print( "\n" );
        print( "=========================================================\n" );
        print( "Dato:      Uge:     Kode:\n" );
        print( "=========================================================\n" );
        
        for( var curDate = startDate; curDate.daysTo( endDate ) >= 0; curDate = curDate.addDays( 1 ) ) {
            //var calcDate = model.findWeekCalc( curDate ).calcDate( curDate );
            
            print( model.numberToStr( curDate.day() ) + "/" + model.numberToStr( curDate.month() ) + "-" + curDate.year() + 
                   " (Uge " + model.numberToStr( curDate.weekNumber() ) + ") " + model.getCode( code, curDate ) + "\n" );
        };
    });
};

UnitTest.addFixture( "WeekCodes.use.js module", function() {
    // ===============================================================================
    //             Basis tests
    // ===============================================================================
    Assert.equal( "Wrong code", 
		  'new WeekModel().getCode( "xxx", new QDate( 1, 1, 2012  ) )', 
		  "Ukendt ugekode: xxx" );

    Assert.exception( "Wrong date", 
		      'new WeekModel().getCode( "DBF_SFD", new QDate( 87, 44, 2012  ) )' );

    // ===============================================================================
    //             Code: DBF_SFD
    // ===============================================================================
    Assert.equal( "DBF_SFD - 2014: 12", 
		  'new WeekModel().getCode( "DBF_SFD", new QDate( 5, 12, 2014  ) )', 
		  "DBF201501 *xBKM201501" );
    Assert.equal( "DBF_SFD - 2014: 13", 
		  'new WeekModel().getCode( "DBF_SFD", new QDate( 26, 12, 2014  ) )', 
		  "DBF201503 *xBKM201503" );
    Assert.equal( "DBF_SFD - 2014: 14", 
		  'new WeekModel().getCode( "DBF_SFD", new QDate( 2, 1, 2015  ) )', 
		  "DBF201504 *xBKM201504" );
// 2015
Assert.equal( "DBF_SFD - 2015: 1", 
		  'new WeekModel().getCode( "DBF_SFD", new QDate( 2, 1, 2015  ) )', 
		  "DBF201504 *xBKM201504" );
// 201515 udgaar		  
   Assert.equal( "DBF_SFD - 2015: 2", 
		  'new WeekModel().getCode( "DBF_SFD", new QDate( 20, 3, 2015  ) )', 
		  "DBF201516 *xBKM201516" );
// paaske		  
    Assert.equal( "DBF_SFD - 2015: 3", 
		  'new WeekModel().getCode( "DBF_SFD", new QDate( 7, 4, 2015  ) )', 
		  "DBF201517 *xBKM201517" );
// skift tilbage efter paaske		  
    Assert.equal( "DBF_SFD - 2015: 4", 
		  'new WeekModel().getCode( "DBF_SFD", new QDate( 13, 4, 2015  ) )', 
		  "DBF201518 *xBKM201518" );
//Bededag		  
    Assert.equal( "DBF_SFD - 2015: 5", 
		  'new WeekModel().getCode( "DBF_SFD", new QDate( 30, 4, 2015  ) )', 
		  "DBF201521 *xBKM201521" );
// skift tilbage efter bededag		  
    Assert.equal( "DBF_SFD - 2015: 6", 
		  'new WeekModel().getCode( "DBF_SFD", new QDate( 8, 5, 2015  ) )', 
		  "DBF201522 *xBKM201522" );
// Skift onsdag Kri. Himmelfart		  
    Assert.equal( "DBF_SFD - 2015: 7", 
		  'new WeekModel().getCode( "DBF_SFD", new QDate( 13, 5, 2015  ) )', 
		  "DBF201523 *xBKM201523" );
// skift torsdag foer pinse		  
    Assert.equal( "DBF_SFD - 2015: 8", 
		  'new WeekModel().getCode( "DBF_SFD", new QDate( 21, 5, 2015  ) )', 
		  "DBF201524 *xBKM201524" );
// Skift tilbage efter pinse		  
    Assert.equal( "DBF_SFD - 2015: 9", 
		  'new WeekModel().getCode( "DBF_SFD", new QDate( 29, 5, 2015  ) )', 
		  "DBF201525 *xBKM201525" );
// Skift torsdag grundlovsdag		  
    Assert.equal( "DBF_SFD - 2015: 10", 
		  'new WeekModel().getCode( "DBF_SFD", new QDate( 4, 6, 2015  ) )', 
		  "DBF201526 *xBKM201526" );
// Skift tilbage efter grundlovsdag		  
    Assert.equal( "DBF_SFD - 2015: 11", 
		  'new WeekModel().getCode( "DBF_SFD", new QDate( 12, 6, 2015  ) )', 
		  "DBF201527 *xBKM201527" );
// 52 udgaar	  
    Assert.equal( "DBF_SFD - 2015: 12", 
		  'new WeekModel().getCode( "DBF_SFD", new QDate( 4, 12, 2015  ) )', 
		  "DBF201601 *xBKM201601" );
// Skift foer jul		  
    Assert.equal( "DBF_SFD - 2015: 13", 
		  'new WeekModel().getCode( "DBF_SFD", new QDate( 28, 12, 2015  ) )', 
		  "DBF201603 *xBKM201603" );	
// Skift tilbage efter jul		  
    Assert.equal( "DBF_SFD - 2015: 14", 
		  'new WeekModel().getCode( "DBF_SFD", new QDate( 8, 1, 2016  ) )', 
		  "DBF201604 *xBKM201604" );			  

// 2016
Assert.equal( "DBF_SFD - 2016: 1", 
		  'new WeekModel().getCode( "DBF_SFD", new QDate( 8, 1, 2016  ) )', 
		  "DBF201604 *xBKM201604" );
// 201613 udgaar		  
   Assert.equal( "DBF_SFD - 2016: 2", 
		  'new WeekModel().getCode( "DBF_SFD", new QDate( 11, 3, 2016  ) )', 
		  "DBF201614 *xBKM201614" );
// paaske		  
    Assert.equal( "DBF_SFD - 2016: 3", 
		  'new WeekModel().getCode( "DBF_SFD", new QDate( 29, 3, 2016  ) )', 
		  "DBF201614 *xBKM201614" );
// skift tilbage efter paaske		  
    Assert.equal( "DBF_SFD - 2016: 4", 
		  'new WeekModel().getCode( "DBF_SFD", new QDate( 4, 4, 2016  ) )', 
		  "DBF201616 *xBKM201616" );
//Bededag		  
    Assert.equal( "DBF_SFD - 2016: 5", 
		  'new WeekModel().getCode( "DBF_SFD", new QDate( 22, 4, 2016  ) )', 
		  "DBF201619 *xBKM201619" );
// skift tilbage efter bededag		  
    Assert.equal( "DBF_SFD - 2016: 6", 
		  'new WeekModel().getCode( "DBF_SFD", new QDate( 29, 4, 2016  ) )', 
		  "DBF201620 *xBKM201620" );
// Skift onsdag Kri. Himmelfart		  
    Assert.equal( "DBF_SFD - 2016: 7", 
		  'new WeekModel().getCode( "DBF_SFD", new QDate( 4, 5, 2016  ) )', 
		  "DBF201621 *xBKM201621" );
// skift torsdag foer pinse		  
    Assert.equal( "DBF_SFD - 2016: 8", 
		  'new WeekModel().getCode( "DBF_SFD", new QDate( 12, 5, 2016  ) )', 
		  "DBF201622 *xBKM201622" );
// Skift tilbage efter pinse		  
    Assert.equal( "DBF_SFD - 2016: 9", 
		  'new WeekModel().getCode( "DBF_SFD", new QDate( 20, 5, 2016  ) )', 
		  "DBF201623 *xBKM201623" );
// Skift torsdag grundlovsdag		  
    //Assert.equal( "DBF_SFD - 2015: 10", 
		  //'new WeekModel().getCode( "DBF_SFD", new QDate( 4, 6, 2015  ) )', 
		  //"DBF201526 *xBKM201526" );
// Skift tilbage efter grundlovsdag		  
    //Assert.equal( "DBF_SFD - 2015: 11", 
		  //'new WeekModel().getCode( "DBF_SFD", new QDate( 12, 6, 2015  ) )', 
		  //"DBF201527 *xBKM201527" );
// 52 udgaar	  
    Assert.equal( "DBF_SFD - 2016: 12", 
		  'new WeekModel().getCode( "DBF_SFD", new QDate( 9, 12, 2016  ) )', 
		  "DBF201701 *xBKM201701" );
// Skift foer jul		  
    //Assert.equal( "DBF_SFD - 2015: 13", 
		  //'new WeekModel().getCode( "DBF_SFD", new QDate( 28, 12, 2015  ) )', 
		  //"DBF201603 *xBKM201603" );	
// Skift tilbage efter jul		  
    Assert.equal( "DBF_SFD - 2016: 14", 
		  'new WeekModel().getCode( "DBF_SFD", new QDate( 6, 1, 2017  ) )', 
		  "DBF201704 *xBKM201704" );			  
		  
		  
    // ===============================================================================
    //             Code: KODE_IDO (DAT-koder)
    // ===============================================================================
    Assert.equal( "KODE_IDO - 2014: 12", 
		  'new WeekModel().getCode( "KODE_IDO", new QDate( 5, 12, 2014  ) )', 
		  "IDO201452" );
    Assert.equal( "KODE_IDO - 2014: 13", 
		  'new WeekModel().getCode( "KODE_IDO", new QDate( 26, 12, 2014  ) )', 
		  "IDO201502" );
    Assert.equal( "KODE_IDO - 2014: 14", 
		  'new WeekModel().getCode( "KODE_IDO", new QDate( 2, 1, 2015  ) )', 
		  "IDO201503" );
 //2015
	Assert.equal( "KODE_IDO - 2015: 1", 
		  'new WeekModel().getCode( "KODE_IDO", new QDate( 2, 1, 2015  ) )', 
		  "IDO201503" );
    Assert.equal( "KODE_IDO - 2015: 2", 
		  'new WeekModel().getCode( "KODE_IDO", new QDate( 20, 3, 2015  ) )', 
		  "IDO201515" );
    Assert.equal( "KODE_IDO - 2015: 3", 
		  'new WeekModel().getCode( "KODE_IDO", new QDate( 7, 4, 2015  ) )', 
		  "IDO201516" );
    Assert.equal( "KODE_IDO - 2015: 4", 
		  'new WeekModel().getCode( "KODE_IDO", new QDate( 13, 4, 2015  ) )', 
		  "IDO201517" );
    Assert.equal( "KODE_IDO - 2015: 5", 
		  'new WeekModel().getCode( "KODE_IDO", new QDate( 30, 4, 2015  ) )', 
		  "IDO201520" );
    Assert.equal( "KODE_IDO - 2015: 6", 
		  'new WeekModel().getCode( "KODE_IDO", new QDate( 8, 5, 2015  ) )', 
		  "IDO201521" );
    Assert.equal( "KODE_IDO - 2015: 7", 
		  'new WeekModel().getCode( "KODE_IDO", new QDate( 13, 5, 2015  ) )', 
		  "IDO201522" );
    Assert.equal( "KODE_IDO - 2015: 8", 
		  'new WeekModel().getCode( "KODE_IDO", new QDate( 21, 5, 2015  ) )', 
		  "IDO201523" );
    Assert.equal( "KODE_IDO - 2015: 9", 
		  'new WeekModel().getCode( "KODE_IDO", new QDate( 29, 5, 2015  ) )', 
		  "IDO201524" );
    Assert.equal( "KODE_IDO - 2015: 10", 
		  'new WeekModel().getCode( "KODE_IDO", new QDate( 4, 6, 2015  ) )', 
		  "IDO201525" );
    Assert.equal( "KODE_IDO - 2015: 11", 
		  'new WeekModel().getCode( "KODE_IDO", new QDate( 12, 6, 2015  ) )', 
		  "IDO201526" );
    Assert.equal( "KODE_IDO - 2015: 12", 
		  'new WeekModel().getCode( "KODE_IDO", new QDate( 4, 12, 2015  ) )', 
		  "IDO201552" ); 
	Assert.equal( "KODE_IDO - 2015: 13", 
		  'new WeekModel().getCode( "KODE_IDO", new QDate( 28, 12, 2015  ) )', 
		  "IDO201602" ); 
	Assert.equal( "KODE_IDO - 2015: 14", 
		  'new WeekModel().getCode( "KODE_IDO", new QDate( 8, 1, 2016  ) )', 
		  "IDO201603" ); 	  
//2016
	Assert.equal( "KODE_IDO - 2016: 1", 
		  'new WeekModel().getCode( "KODE_IDO", new QDate( 8, 1, 2016  ) )', 
		  "IDO201603" );
    Assert.equal( "KODE_IDO - 2016: 2", 
		  'new WeekModel().getCode( "KODE_IDO", new QDate( 11, 3, 2016  ) )', 
		  "IDO201613" );
    Assert.equal( "KODE_IDO - 2016: 3", 
		  'new WeekModel().getCode( "KODE_IDO", new QDate( 29, 4, 2016  ) )', 
		  "IDO201619" );
    //Assert.equal( "KODE_IDO - 2016: 4", 
		  //'new WeekModel().getCode( "KODE_IDO", new QDate( 4, 5, 2016  ) )', 
		  //"IDO201620" );
    Assert.equal( "KODE_IDO - 2016: 5", 
		  'new WeekModel().getCode( "KODE_IDO", new QDate( 21, 4, 2016  ) )', 
		  "IDO201618" );
    Assert.equal( "KODE_IDO - 2016: 6", 
		  'new WeekModel().getCode( "KODE_IDO", new QDate( 29, 4, 2016  ) )', 
		  "IDO201619" );
    Assert.equal( "KODE_IDO - 2016: 7", 
		  'new WeekModel().getCode( "KODE_IDO", new QDate( 4, 5, 2016  ) )', 
		  "IDO201620" );
    Assert.equal( "KODE_IDO - 2016: 8", 
		  'new WeekModel().getCode( "KODE_IDO", new QDate( 12, 5, 2016  ) )', 
		  "IDO201621" );
    Assert.equal( "KODE_IDO - 2016: 9", 
		  'new WeekModel().getCode( "KODE_IDO", new QDate( 20, 5, 2016  ) )', 
		  "IDO201622" );
    //Assert.equal( "KODE_IDO - 2015: 10", 
		  //'new WeekModel().getCode( "KODE_IDO", new QDate( 4, 6, 2015  ) )', 
		  //"IDO201525" );
    //Assert.equal( "KODE_IDO - 2015: 11", 
		  //'new WeekModel().getCode( "KODE_IDO", new QDate( 12, 6, 2015  ) )', 
		  //"IDO201526" );
    Assert.equal( "KODE_IDO - 2016: 12", 
		  'new WeekModel().getCode( "KODE_IDO", new QDate( 9, 12, 2016  ) )', 
		  "IDO201652" ); 
	//Assert.equal( "KODE_IDO - 2015: 13", 
		  //'new WeekModel().getCode( "KODE_IDO", new QDate( 28, 12, 2015  ) )', 
		  //"IDO201602" ); 
	Assert.equal( "KODE_IDO - 2016: 14", 
		  'new WeekModel().getCode( "KODE_IDO", new QDate( 6, 1, 2017  ) )', 
		  "IDO201703" ); 	  

}
);
