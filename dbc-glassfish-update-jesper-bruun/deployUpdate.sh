function usage
{
    echo "deployUpdate.sh: -h or --host <Host to deploy to> -d or --dir <Temp dir to use when deploying docker image> -u or --user <User to login as on host> "
}

if [ $# -eq 0 ]; then
    usage
    exit 1
fi



while [ "$1" != "" ]; do
    
    case $1 in
       -h | --host )            shift
                                HOST=$1
                                ;;
       -d | --dir )             shift
                                DEPLOYDIR=$1
                                ;;
      -u | --user )             shift
                                USER=$1
                                ;;
        * )                     usage
                                exit 1
    esac
    shift
done




#HOST=danbib-i01
#DEPLOYDIR=/data/dataio/docker
#USER=jenkins

scp ./env-update* $USER@$HOST:$DEPLOYDIR/

# Build run sript
cat  << EOF > ./run-update.sh
#!/usr/bin/env bash
echo "Stopping existing containers"

docker stop dbc-glassfish-update1 || true
docker rm -f dbc-glassfish-update1 || true

docker stop dbc-glassfish-update2 || true
docker rm -f dbc-glassfish-update2 || true


docker rmi -f docker-index:5000/dbc-glassfish-update:latest
docker pull docker-index:5000/dbc-glassfish-update:latest
echo "Staring containers"
nohup docker run --name dbc-glassfish-update1 --rm -v /cmdbdata:/tmp -v /data/dataio/docker/serverlogs:/usr/local/glassfish4/glassfish/domains/domain1/logs -v /data/dataio/docker/gf-logs:/data/gf-logs --env-file=${DEPLOYDIR}/env-update1.env -p 4852:4848 -p 8092:8080 -p 1868:1868 docker-index:5000/dbc-glassfish-update:latest >${DEPLOYDIR}/update.log 2>&1 &
nohup docker run --name dbc-glassfish-update2 --rm -v /cmdbdata:/tmp -v /data/dataio/docker/serverlogs:/usr/local/glassfish4/glassfish/domains/domain1/logs -v /data/dataio/docker/gf-logs:/data/gf-logs --env-file=${DEPLOYDIR}/env-update2.env -p 4853:4848 -p 8093:8080 -p 1869:1868 docker-index:5000/dbc-glassfish-update:latest >${DEPLOYDIR}/update.log 2>&1 &

EOF
# End of build runscript

chmod +x ./run-update.sh

# Push to targethost
scp ./run-update.sh $USER@$HOST:${DEPLOYDIR}/run-update.sh

# Run it
ssh $USER@$HOST ${DEPLOYDIR}/run-update.sh 
