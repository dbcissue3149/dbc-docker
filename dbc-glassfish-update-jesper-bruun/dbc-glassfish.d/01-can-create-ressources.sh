#!/usr/bin/env bash

set -e

# when the shell expands, dont include the pattern in the list of results (e_g_ exclude *_xml)
shopt -s nullglob
export RAWREPO_DB=$(/usr/local/bin/cmdb-get /rawrepo/database $DEFRAWREPO_DB)
export RAWREPO_DBUSER=$(/usr/local/bin/cmdb-get /rawrepo/username $DEFRAWREPO_DBUSER)
export RAWREPO_DBPASSWORD=$(/usr/local/bin/cmdb-get /rawrepo/password $DEFRAWREPO_DBPASSWORD)
export RAWREPO_DBHOST=$(/usr/local/bin/cmdb-get /rawrepo/host $DEFRAWREPO_DBHOST)
export RAWREPO_DBPORT=$(/usr/local/bin/cmdb-get /rawrepo/port $DEFRAWREPO_DBPORT)
export OPENAGENCYURL=$(/usr/local/bin/cmdb-get /openagency/url $DEFOPENAGENCYURL)
export FORSRIGHTSURL=$(/usr/local/bin/cmdb-get /forsrights/url $DEFFORSRIGHTSURL)
export RAWREPO_SOLRURL=$(/usr/local/bin/cmdb-get /rawrepo/solr/url $DEFRAWREPO_SOLRURL)

export HOLDINGS_DB=$(/usr/local/bin/cmdb-get /holdings-db/database $DEFHOLDINGS_DB)
export HOLDINGS_DBUSER=$(/usr/local/bin/cmdb-get /holdings-db/username $DEFHOLDINGS_DBUSER)
export HOLDINGS_DBPASSWORD=$(/usr/local/bin/cmdb-get /holdings-db/password $DEFHOLDINGS_DBPASSWORD)
export HOLDINGS_DBHOST=$(/usr/local/bin/cmdb-get /holdings-db/host $DEFHOLDINGS_DBHOST)
export HOLDINGS_DBPORT=$(/usr/local/bin/cmdb-get /holdings-db/port $DEFHOLDINGS_DBPORT)

wget http://is.dbc.dk/view/Iscrum/job/updateservice-head-guesstimate/lastSuccessfulBuild/artifact/deploy/updateservice-1.0.0.tgz
mv updateservice-1.0.0.tgz /data
tar -xvzf  /data/updateservice-1.0.0.tgz -C /data/
tar -xvzf  /data/opencat-business.tgz -C /data/opencat

env


cat << EOF > ${GLASSFISH_USER_HOME}/dbc-glassfish.d/19-db-connection-pools.xml
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE resources PUBLIC "-//GlassFish.org//DTD GlassFish Application Server 3.1 Resource Definitions//EN" "http://glassfish.org/dtds/glassfish-resources_1_5.dtd">

  <resources>

   <jdbc-connection-pool datasource-classname="org.postgresql.ds.PGSimpleDataSource" name="jdbc/updateservice/raw-repo/pool" res-type="javax.sql.DataSource">
      <property name="Password" value="${RAWREPO_DBPASSWORD}"></property>
      <property name="ServerName" value="${RAWREPO_DBHOST}"></property>
      <property name="DatabaseName" value="${RAWREPO_DB}"></property>
      <property name="Port" value="${RAWREPO_DBPORT}"></property>
      <property name="User" value="${RAWREPO_DBUSER}"></property>
    </jdbc-connection-pool>

    <jdbc-resource pool-name="jdbc/updateservice/raw-repo/pool" jndi-name="jdbc/updateservice/raw-repo-readonly"></jdbc-resource>
    <jdbc-resource pool-name="jdbc/updateservice/raw-repo/pool" jndi-name="jdbc/updateservice/raw-repo-writable"></jdbc-resource>

    <jdbc-connection-pool datasource-classname="org.postgresql.ds.PGSimpleDataSource" name="jdbc/updateservice/holdingitems/pool" res-type="javax.sql.DataSource">
      <property name="Password" value="${HOLDINGS_DBPASSWORD}"></property>
      <property name="ServerName" value="${HOLDINGS_DBHOST}"></property>
      <property name="DatabaseName" value="${HOLDINGS_DB}"></property>
      <property name="Port" value="${HOLDINGS_DBPORT}"></property>
      <property name="User" value="${HOLDINGS_DBUSER}"></property>
    </jdbc-connection-pool>

    <jdbc-resource pool-name="jdbc/updateservice/holdingitems/pool" jndi-name="jdbc/updateservice/holdingitems"></jdbc-resource>

</resources> 
EOF


 
cat << EOF >  ${GLASSFISH_USER_HOME}/dbc-glassfish.d/18-updateservice-settings.xml
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE resources PUBLIC "-//GlassFish.org//DTD GlassFish Application Server 3.1 Resource Definitions//EN" "http://glassfish.org/dtds/glassfish-resources_1_5.dtd">
      <resources>

         <custom-resource factory-class="org.glassfish.resources.custom.factory.PropertiesFactory" res-type="java.util.Properties" jndi-name="updateservice/settings">
                <property name="forsrights.url" value="${FORSRIGHTSURL}"></property>
                <property name="auth.product.name" value="netpunkt.dk"></property>
                <property name="solr.url" value="${RAWREPO_SOLRURL}"></property>
                <property name="auth.use.ip" value="True"></property>
                <property name="allow.extra.record.data" value="True"></property>
                <property name="javascript.basedir" value="/data/opencat/opencat-business"></property>
                <property name="javascript.install.name" value="dataio"></property>
                <property name="rawrepo.provider.id" value="opencataloging-update"></property>
                <property name="openagency.url" value="${OPENAGENCYURL}"></property>
        </custom-resource>


      </resources>
EOF
 

