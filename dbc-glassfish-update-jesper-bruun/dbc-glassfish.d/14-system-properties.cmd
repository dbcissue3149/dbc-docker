create-system-properties --target server-config BUILD_LOGBACK_FILENAME=/data/gf-logs/build_dataio/build --passwordfile=./passfile.txt
create-system-properties --target server-config JMS_PROVIDER_PORT=30676 --passwordfile=./passfile.txt
create-system-properties --target server-config UPDATE_LOGBACK_FILENAME=/data/gf-logs/update_dataio/update --passwordfile=./passfile.txt
