#!/usr/bin/env bash

# Settings some defensive bash vars, see
# http://www.davidpashley.com/articles/writing-robust-shell-scripts/#id2382181
# These will apply to alle the scripts.

set -o errexit # Exit whenever you encounter an error
set -o nounset # Exit if an unset variable is used
#set -o verbose # Be loud. You could keep this on, as it will make it easier to
               #   pinpoint an error during automatic (CI) test execution.

function showHelp {
	echo " ---- USAGE ----"
	echo $0 " Runs all tests"
	echo $0 "--prefix PREFIX [--prefix|-p] Runs all tests with prefix."
	echo "     If prefix is 'dbc-postgres' test 01-dbc-postgres-create.sh"
	echo "     and 02-dbc-postgres-destroy will be executed,"
	echo "     but 01-dbc-glassfish.sh will not. This allows you to "
	echo "     run partial tests."
} 

PREFIX=test.d/*.sh
BASEDIR=null
ARTIFACT_ID=null

# As long as there are arguments given by the user
while [[ $# > 0 ]]
do
	key="$1"
	
	case $key in 
		--basedir)
        # allow user to execute only tests with a certain prefix
        BASEDIR=$2
	    shift # past switch
	    shift # past argument
	    ;;
		--artifact_id)
        # allow user to execute only tests with a certain prefix
        ARTIFACT_ID=$2
	    shift # past switch
	    shift # past argument
	    ;;
		-p|--prefix)
        # allow user to execute only tests with a certain prefix
        PREFIX=$2
	    shift # past switch
	    shift # past argument
	    ;;
		-h|--help)
		showHelp
		exit 0
	    ;;
	    *)
	    echo "Unknown option $2"
	    shift # Shift to avoid endless loop. Not sure this works
	    ;;
	esac
done	    

echo Executing tests with prefix $PREFIX
TESTS_EXECUTED=""

shopt -s nullglob

echo "===> Maven test runner executing tests in [$PREFIX]"
for test in $PREFIX; do
	echo "=====> Maven test runner preparing test [$test] with setupEnvironmentAndCommonFunctionsForDeveloper and running it."

	echo PWD $PWD

    BASEDIR=$BASEDIR ARTIFACT_ID=$ARTIFACT_ID $BASEDIR/src/resource/docker/test-suite/setupEnvironmentAndCommonFunctionsForDevelopers.sh $test

    EXIT_CODE=$?
    if [[ "$EXIT_CODE" != "0" ]]; then
        echo "Exit code from script ["$test"] is not zero ["$EXIT_CODE"] stopping test execution"
        echo "FAILURE-DBC-DOCKER-TEST Test exited with error code" 
        exit 1
      fi	
	TESTS_EXECUTED+="$test "
done

echo "Executed tests " $TESTS_EXECUTED
echo "SUCCESS-DBC-DOCKER-TEST All tests executed without error codes"

