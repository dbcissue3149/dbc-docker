#!/usr/bin/env bash

function checkInput
{
    if [ $# -ne 1 ]; then
        echo I am just a messenger. You have to pass me the test script you want me to call.
        echo When I have it, I will source common functions and pass them to your script,
        echo so its easy to use them in your script.
        echo You passed
        exit 1
    fi
}
checkInput $@

TEST_SCRIPT=$1

function loadSharedCommonFunctions
{
    shopt -s nullglob
    for func in $BASEDIR/src/resource/docker/test-suite/common-test-functions/*.function; do
        source $func
        base=$(basename "$func" .function)
        echo $base
        export -f $base
    done
}
loadSharedCommonFunctions

function setupConvenienceEnvironmentVariables
{
    # Your own compose file (you have to write it)
    export COMPOSE_FILE=$BASEDIR/src/main/docker/share/compose/$ARTIFACT_ID-compose.yml

    # The compose files of the projects you depend on are placed here.
    export SHARED_COMPOSE_FILE_DIR=$BASEDIR/src/resource/docker/compose

    # The collection of shared compose files with -f parameter in front,
    # so they are easy to use when calling compose.
    COMPOSE_DEPENDENCIES=" "
    shopt -s nullglob
    for composefile in $SHARED_COMPOSE_FILE_DIR/*.yml; do
        COMPOSE_DEPENDENCIES+="-f $composefile "
    done
    export COMPOSE_DEPENDENCIES
}
setupConvenienceEnvironmentVariables

echo BASEDIR from $0 $BASEDIR
echo COMPOSE_FILE from $0 $COMPOSE_FILE
echo COMPOSE_DEPENDENCIES from $0 $COMPOSE_DEPENDENCIES

function defaultTest
{
    exitIfContainerDoesNotStart 20 $CONTAINER_ID
}
export -f defaultTest

function defaultCleanup
{
    echo Cleaning up
    if [ ! -z "$CONTAINER_ID" ]; then
        EXITED_ID=$(docker ps --filter "status=exited" -q)
        echo This container has Exited $EXITED_ID
        if [ ! -n "$EXITED_ID" ]; then
          echo $(docker logs "$EXITED_ID")
        fi;
        echo Stopping compose $COMPOSE_FILE $COMPOSE_DEPENDENCIES
        docker-compose -f $COMPOSE_FILE $COMPOSE_DEPENDENCIES down -v
    fi
}
export -f defaultCleanup

function defaultStartup
{
    docker-compose -f $COMPOSE_FILE $COMPOSE_DEPENDENCIES up -d
    CONTAINER_ID=$(docker-compose -f $COMPOSE_FILE $COMPOSE_DEPENDENCIES ps -q $ARTIFACT_ID)
}
export -f defaultStartup

# Execute script. It is (should be) an absolute path
$TEST_SCRIPT
