#!/usr/bin/env bash

function exitIfDBDoesNotExist {
    echo EXECUTING exitIfDBDoesNotExist
    if [ "$#" -ne 2 ]; then
        echo "You must provide CONTAINER_ID DATABASE"
        exit 1
    fi

    CONTAINER_ID=$1
    DATABASE=$2
    PORT=$(docker inspect --format='{{(index (index .NetworkSettings.Ports "5432/tcp") 0).HostPort}}' $CONTAINER_ID)
    echo postgres running on port $PORT
    
    # Look at the list of databases and count the number of occurences of the database
    #   we are looking for. The count should be 1
    docker exec -u postgres $CONTAINER_ID psql -lqt

    let SLEPT_SECONDS=1
    let SECONDS_TO_WAIT=10
 
    STARTED=false   
    while (( $"SLEPT_SECONDS" < $"SECONDS_TO_WAIT" )) ; do
        DB_PRESENT=$(docker exec -u postgres $CONTAINER_ID psql -lqt | cut -d \| -f 1 | grep -w $2 | wc -l)
        if [ "$DB_PRESENT" -eq "1" ] ; then
            STARTED=true
            docker exec -u postgres $CONTAINER_ID psql -lqt
            break
        fi
        sleep 1
        echo "Slept "$SLEPT_SECONDS" seconds while waiting for database"
        SLEPT_SECONDS=$((SLEPT_SECONDS+1))
    done
    
    if ! $STARTED ; then
        echo Got tired waiting for database $DATABASE. Waited $SLEPT_SECONDS seconds
        exit 1
    fi
    echo Found database $2
}

