#!/usr/bin/env bash
set -e
/usr/local/bin/confd -onetime -confdir=confd -backend env
exec java -jar $JARS_DIR/hive.jar -c $CONFIG_DIR/hive.properties
