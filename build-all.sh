#!/usr/bin/env bash



function box_out()
{
echo "---------------------------------------------------"
echo "---------------------------------------------------"
echo $*
echo "---------------------------------------------------"
echo "---------------------------------------------------"
}

function showHelp {
	echo " ---- USAGE ----"
	echo $0 " Builds all docker projects. Nice if you want to be sure that dependent projects are up2date"
	echo $0 "--rm [--force|-f] remove and build all. Use the force if Docker complains that containers are using your images"
	echo Add in OPTS to control docker build, e.g. OPTS="--no-cache=true" ./build-all.sh	
} 

set -e

while [[ $# > 0 ]]
do
	key="$1"
	
	case $key in 
		-f|--force)
	    FORCE=true
	    shift # past argument
	    ;;
		--rm|--remove)
	    REMOVE=true
	    shift # past argument
	    ;;
		-h|--help)
		showHelp
		exit 0
	    shift # past argument
	    ;;
	    *)
	    echo "Unknown option $2"
	    ;;
	esac
done	    

# The indents is a primitive way to keep track of dependencies, 
#   e.g. "dbc-glassfish-flowstore" depends on "dbc-glassfish" 
declare -a arr=(
		"dbc-jessie-docker-tools" \
	            "dbc-java8" \
                        "dbc-java-hive/src/main/docker/dockerfile" \
                        "dbc-java-fedora-indexer/src/main/docker/dockerfile" \
                        "dbc-fakesmtp/src/main/docker/dockerfile" \
                        "dbc-java-gatekeeper/src/main/docker/dockerfile" \
                    "dbc-postgres/src/main/docker/dockerfile" \
                        "dbc-postgres-rawrepo/src/main/docker/dockerfile" \
                        "dbc-postgres-rawrepo-minimal" \
                        "dbc-postgres-holdingsitems/src/main/docker/dockerfile" \
                        "dbc-postgres-flowstore" \
                        "dbc-postgres-filestore/src/main/docker/dockerfile" \
                        "dbc-postgres-inflight" \
                        "dbc-postgres-jobstore/src/main/docker/dockerfile" \
                        "dbc-glassfish/src/main/docker/dockerfile" \
                        "dbc-glassfish-logstore/src/main/docker/dockerfile/src/main/docker/dockerfile/" \
                        "dbc-glassfish-gui/src/main/docker/dockerfile/" \
                        "dbc-glassfish-flowstore/src/main/docker/dockerfile/" \
                        "dbc-glassfish-jobstore/src/main/docker/dockerfile/" \
                        "dbc-glassfish-filestore/src/main/docker/dockerfile/" \
                        "dbc-glassfish-jobprocessor/src/main/docker/dockerfile/" \
                        "dbc-glassfish-harvester/src/main/docker/dockerfile/" \
                        "dbc-glassfish-openformat-java" \
                        "dbc-glassfish-update" \
                        "dbc-glassfish-update-sink" \
                        "dbc-glassfish-rawrepo-indexer" \
                        "dbc-glassfish-rr-content-service/src/main/docker/dockerfile" \
                        "dbc-glassfish-holdingsitems-indexer" \
                        "dbc-glassfish-holdingsitems-update/src/main/docker/dockerfile/" \
                "dbc-openmq/src/main/docker/dockerfile" \
                "dbc-apache-php" \
                    "dbc-apache-php-openformat" \
                    "dbc-apache-php-opensearch" \
                "dbc-tomcat" \
                    "dbc-tomcat-fcrepo/src/main/docker/dockerfile/" \
                    "dbc-tomcat-solr" \
                        "dbc-tomcat-solr-rr/src/main/docker/dockerfile/" \
                        "dbc-tomcat-solr-fedora-holdingsitems/src/main/docker/dockerfile/" \
                "tools/tools-dot/docker-graph/" \
                "dbc-glassfish-es-sink/" \
                "dbc-glassfish-diff-sink/src/main/docker/dockerfile/" \
				)


box_out If this is the first time you are running this script locally, you have to _make_ dbc-jessie first
for dockerfile in "${arr[@]}"

do
	box_out Building $dockerfile	

	if [ "$REMOVE" = true ] ; then
		if [ "$FORCE" = true ] ; then
			RM_ARGS+=" --force"
		fi
		if [ "docker images -q | grep $dockerfile | wc -l" gt 0 ] ; then
			docker rmi $RM_ARGS $dockerfile
		fi 
		# Remove untagged images
		if [ "docker images -q --filter 'dangling=true' | wc -l'" gt 0 ] ; then
			docker rmi $(docker images -q --filter "dangling=true")
		fi
	fi
	imagename=$(echo $dockerfile | sed "s/\/.*//")
    docker build $OPTS -t $imagename $dockerfile
    BUILT+=$dockerfile,
done
echo "The following docker images have been built: " $BUILT

