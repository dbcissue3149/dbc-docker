#!/usr/bin/env bash

YML_OPTIONS="-f dbc-glassfish-jobstore-compose.yml -f dbc-postgres-filestore-compose.yml -f dbc-glassfish-flowstore-compose.yml -f dbc-fakesmtp-compose.yml -f dbc-java-gatekeeper-compose.yml -f openmq-compose.yml -f dbc-glassfish-gui-compose.yml -f dbc-glassfish-logstore-compose.yml -f dbc-postgres-logstore-compose.yml -f dbc-glassfish-jobprocessor-compose.yml -f dbc-glassfish-harvester-compose.yml -f dbc-glassfish-es-sink-compose.yml -f docker-compose.yml"

echo "docker-compose $YML_OPTIONS $@"
docker-compose $YML_OPTIONS $@
