#!/usr/bin/env bash

set -e

env

cat << EOF > ${APP_USER_HOME}/plan/settings.properties
# ===================================================================
#               RawRepo Services
# ===================================================================

rawrepo.solr.host = ${RAWREPO_SOLR_PORT_8080_TCP_ADDR}
rawrepo.solr.port = ${RAWREPO_SOLR_PORT_8080_TCP_PORT}
rawrepo.solr.path = solr/rawrepo

# ===================================================================
#               Update Services
# ===================================================================

update.url.path = CatalogingUpdateServices/UpdateService
update.forwarded.host = 172.16.0.200

update.fbs.host = ${UPDATE_FBS_PORT_8080_TCP_ADDR}
update.fbs.port = ${UPDATE_FBS_PORT_8080_TCP_PORT}
EOF
