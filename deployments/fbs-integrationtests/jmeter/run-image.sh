#!/usr/bin/env bash

echo "docker run --rm --link service_update-stresstests-gf_1:update $@ update-stress-test"
docker run --rm --link fbsintegrationtests_update-fbs_1:update-fbs --link fbsintegrationtests_rawrepo-solr_1:rawrepo-solr $@ fbs-integration-test
