#!/usr/bin/env bash

COMPOSE_FILES="-f rawrepo.yml -f holdingsitems.yml"

case "$1" in
    up)
    docker-compose -f rawrepo.yml -f holdingsitems.yml up -d
    ;;

    start)
    docker-compose $COMPOSE_FILES start
    ;;

    stop)
    docker-compose $COMPOSE_FILES stop
    ;;

    rm)
    docker-compose $COMPOSE_FILES rm -f
    ;;

    *)
    echo "Unknown command $2"
    exit 1
    ;;
esac
