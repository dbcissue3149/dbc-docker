#!/usr/bin/env bash

UPDATESERVCE_BASE_DIR=../../../updateservice
LOG_DIR=logs

if [ -d "$LOG_DIR" ]; then
    rm -rf $LOG_DIR
fi

mkdir -p $LOG_DIR/update-dataio/app
mkdir -p $LOG_DIR/update-fbs/app

cp $UPDATESERVCE_BASE_DIR/*logback*.xml $LOG_DIR/update-dataio/app/.
cp $UPDATESERVCE_BASE_DIR/*logback*.xml $LOG_DIR/update-fbs/app/.
