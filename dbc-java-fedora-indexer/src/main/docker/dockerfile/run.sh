#!/usr/bin/env bash
set -e
/usr/local/bin/confd -onetime -backend env
exec java -jar $JARS_DIR/solr-indexer.jar -c $CONFIG_DIR/solr-dih.properties
