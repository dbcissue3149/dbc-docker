#!/usr/bin/env bash

echo "This test is a no-op, needs fixing."

# # find the directory of _this_ file
# DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
# 
# shopt -s nullglob
# for func in $DIR/../test-suite/common-test-functions/*.function; do
#     source $func
# done
# 
# PG_COMPOSE_FILE=../dbc-postgres-rawrepo/src/main/docker/compose/dbc-postgres-rawrepo-compose.yml
# CS_COMPOSE_FILE=src/main/docker/compose/dbc-glassfish-rr-content-service-compose.yml
# 
# function insert
# {
#   echo "Inserting test data into rawrepo database"
#   if [ ! -z "$PG_CONTAINER_ID" ]; then
#     cat insert.sql | docker exec -i $PG_CONTAINER_ID \
#       bash -c 'psql -d "$POSTGRES_DB"'
#   fi  
# }
# 
# function get
# {
#   echo "Getting test data from content service"
#   if [ ! -z "CS_CONTAINER_ID" ]; then
#     RESPONSE=cat fetch.xml | docker exec -i $CS_CONTAINER_ID \
#       bash -c ' \
#         curl -X POST --data "@-" \ 
#         --header "Accept: text/plain" \
#         --header "Content-type: text/xml"\
#         "http://localhost:4080/rawrepo-content/RawRepoContentService"
#       '
#   fi
# }
# 
# function cleanup
# {
#     echo Cleaning up
#     if [ ! -n "$PG_CONTAINER_ID" || ! -n "$CS_CONTAINER_ID" ]; then
#         docker-compose -f $PG_COMPOSE_FILE -f $CS_COMPOSE_FILE down
#     fi
# }
# 
# trap cleanup EXIT
# 
# #set -o verbose
# set -o errexit
# 
# echo "test"
# echo $PG_COMPOSE_FILE
# 
# #docker-compose -f $PG_COMPOSE_FILE -f $CS_COMPOSE_FILE up -d
# PG_CONTAINER_ID=$(docker-compose -f $PG_COMPOSE_FILE ps -q dbc-postgres-rawrepo-foo)
# CS_CONTAINER_ID=$(docker-compose -f $CS_COMPOSE_FILE ps -q dbc-glassfish-rr-content-service)
# 
# exitIfContainerDoesNotStart 10 $PG_CONTAINER_ID
# exitIfContainerDoesNotStart 10 $CS_CONTAINER_ID
# exitIfPostgresIsNotRunning $PG_CONTAINER_ID
# insert
# get
# 
echo "Job's done"
exit 0
