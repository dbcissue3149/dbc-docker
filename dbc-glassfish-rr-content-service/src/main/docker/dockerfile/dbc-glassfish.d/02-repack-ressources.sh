#!/usr/bin/env bash

set -e

# when the shell expands, dont include the pattern in the list of results (e_g_ exclude *_xml)
shopt -s nullglob


WAR=rawrepo-content-service

env

mkdir ${GLASSFISH_USER_HOME}/${WAR}
mv ${GLASSFISH_USER_HOME}/${WAR}.war ${GLASSFISH_USER_HOME}/${WAR}

cd ${GLASSFISH_USER_HOME}/${WAR}

unzip ${WAR}.war

cp ${GLASSFISH_USER_HOME}/logback.xml ${GLASSFISH_USER_HOME}/${WAR}/WEB-INF/classes/.

rm ${WAR}.war


jar cmf META-INF/MANIFEST.MF ../${WAR}.war *
chown gfish:gfish  ../${WAR}.war

