#!/usr/bin/env bash

# find the directory of _this_ file
DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

shopt -s nullglob
for func in $DIR/../test-suite/common-test-functions/*.function; do
    source $func
done


#### CLEANUP
function cleanup
{
    echo Cleaning up
    # do whatever you have to do
    # if [ ! -z "$CONTAINER_ID" ]; then
    #     #docker logs $CONTAINER_ID
    #     docker rm -f $CONTAINER_ID
    # fi
}
trap cleanup EXIT
#### CLEANUP

if 0; then
    exit 1
fi
