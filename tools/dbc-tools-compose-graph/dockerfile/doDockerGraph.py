import sys, getopt
import os, re
from graphviz import Digraph
import yaml
from xml.sax.saxutils import escape


def getPatternObj():
    return re.compile('.*realm\(\s*([A-Za-z0-9\-_\.]+):([A-Za-z0-9\-_\.]+)\s*\).*')


def getFilesAsMaps(yamfiles):

    ymls={}
    dockers= {}
    fullText = ''
    realmmatch = getPatternObj()

    # Really nutty solution due to docker
    os.chdir("/tmp")
    for afile in yamfiles:
        fullText+=str(open(afile, 'r').read())
    for line in fullText.splitlines():
        match = realmmatch.match(line)
        if match:
            realm=str(match.group(1))
            docker=str(match.group(2))
            print ("Realm:"+realm+" contains "+docker )
            if not docker in dockers:
                dockers[docker]=realm

    ymls = yaml.load(fullText)


    return ymls, dockers


def getShape(name):
    if ('-glassfish-' in name) or ('-tomcat-' in name):
        return 'component'
    elif '-postgres-' in name:
        return 'tripleoctagon'
    else:
        return 'house'

def usage():
    print('doDockerGraph -y or --yamfiles <list of commaseparated compose files>')



def makeHTML(title, contentMap):
    ret = '< <table border="0" cellspacing="0" valign="top"><tr><td colspan="2"><u>'+title+'</u></td></tr>'


    rev = {}
    names = sorted(contentMap.keys(),reverse=True)

    for name in names:
        rev[name]=contentMap[name]

    for name, val in rev.iteritems():
        #print (type(val))
        if val==None:
            val='none'

        # Turn a sub-dict object into a html table string
        if type(val) is dict:

            #print (json.dumps(val))
            val2 = '<table border="0" cellspacing="0" cellpadding="0">'
            for k,v in val.iteritems():

                v1 = shorten(v, 25)


                val2=val2+'<tr><td  align="left"><font point-size="9"><b>'+escape(k)+':</b>'+escape(v1)+'</font></td></tr>'
            val2=val2+'</table>'
            val=val2
        else:
            val=escape(val)



        ret = ret + '<tr><td  align="left" valign="top"><font point-size="9">'+escape(name)+'</font>:</td><td  align="left"><font point-size="9">'+val+'</font></td></tr>'
    return ret + '</table> >'

def shorten(s, size):
    value = (str(s)[:size] + '(..)') if len(str(s)) > size else str(s)
    print ("Val:"+value)
    return value

def getEnvironment(name,compose):
    env={}
    if "environment" in compose:
        for s in compose["environment"]:

            pair = str(s).split("=")

            pair[1]=shorten(pair[1], 25)
            print("Name:"+name+" Pair:"+str(pair))
            env[pair[0]]=pair[1]
    return makeHTML(name, env)

def real_main(args_list):


    yamlfiles=[]

    try:
        opts, args = getopt.getopt(args_list, "y:", ["yamlfiles="])
    except getopt.GetoptError as err:
        # print help information and exit:
        print str(err) # will print something like "option -a not recognized"
        usage()
        sys.exit(2)
    output = None
    verbose = False

    if opts==None or opts==[] or args==None or args=={}:
        usage()
        sys.exit(2)

    for o, a in opts:
        print (o+" and "+a)
        if o in ("-y", "--yamlfiles"):
            yamlfiles = a.split(',')
        else:
            assert False, "unhandled option"
            usage()
            sys.exit(2)


    composeObjs, dockers = getFilesAsMaps(yamlfiles)
    print ("Dockers:"+str(dockers))
    subgraphs = {}
    mainDataioGraph = Digraph('G',format='png')

    notClassifiedGraph = Digraph('Ikke angivet miljoe')
    #print (composeObjs)
    acolor=0xCCCCCC
    for docker,realm in dockers.iteritems():
        if not realm in subgraphs:
            print ("adding subgraph "+realm)
            subgraphs[realm]=Digraph('cluster_'+realm)
            theColor=str("#%6x" % acolor) #random.randint(0xEEEEEE, 0xFFFFFF)
            theColor=theColor.upper()
            acolor=acolor+0x202020
            subgraphs[realm].node_attr.update(style='solid', color=theColor)
            subgraphs[realm].attr('node', shape='egg', penwidth='2.0', color='#101010', style='filled', fillcolor=theColor)

            subgraphs[realm].graph_attr.update(style='solid', color='blue')
            subgraphs[realm].body.append('label="'+realm+'"')

    for d,g in composeObjs.iteritems():
        gr=None
        if d in dockers:
            gr=subgraphs[dockers[d]]
        else:
            gr=notClassifiedGraph
        #print(g)

        env=getEnvironment(d,g)
        #print (env)
        gr.node(name=d, shape=getShape(d), label=env)



        if "links" in g:
            for l in g["links"]:
                    #gr.node( name=l, shape=getShape(l))
                    gr.edge(d,l)

    for g in subgraphs:

        mainDataioGraph.subgraph(subgraphs[g])

    mainDataioGraph.subgraph(notClassifiedGraph)

    mainDataioGraph.graph_attr["rankdir"]='DT'
    mainDataioGraph.render('docker-graph', view=False)

    print (mainDataioGraph)
if __name__ == "__main__":
        real_main(sys.argv[1:])
