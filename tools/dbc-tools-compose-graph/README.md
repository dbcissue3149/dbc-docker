# dbc-tools-compose-graph

## Synopsis

Sammenhængen mellem kørende containere i store setups (f.eks. DataIo) kan være svær at gennemskue.
dbc-tools-docker-graph hjælper ved at visualisere sammenhængen mellem containere i et compose setup.

## Code Example

Byg med:
 docker build --no-cache -t dbc-tools-compose-graph dockerfile

Kør med:
 docker run -it --rm -v <basis-sti-til-compose-filer>:/tmp dbc-tools-compose-graph <kommasepareret liste af filer uden sti>

Fx:
docker run -it --rm -v /home/jbr/dbc-docker:/tmp dbc-tools-compose-graph dataio-realm-example.yml

eller find automatisk alle filerne og put dem ind:
TODO der er vist en bug i graph programmet
docker run -it --rm -v $PWD/../..:/tmp dbc-tools-compose-graph `find ../.. -path '*/share/compose/*.yml' -printf "%h/%f\n"| sed -e :a  -e 'N;s/\n/,/;ta;N' | sed -e 's#\.\./\.\./#./#g'`

## Usage
dbc-tools-compose-graph analyserer bl.a. links i compose filer, for at lave en graph derudfra.
 dbc-tools-compose-graph kan ikke automatisk se sammenhænge mellem flere containere (f.eks. dataio), men du kan styre dette
 ved at tilføje keywordet

```
# realm (search:dbc-tomcat-solr-rr)
```

i din compose file(r). dbc-tools-compose-graph samler disse keywords op, og grupperer efter det, så du får en visuel
 repræsentation der er mere overskuelig.

### Find flere compose filer

```
find . -path '*/share/compose/*.yml' > composes && sed -e :a  -e 'N;s/\n/,/;ta'  composes > result
```


Som bruger i commandline således

```
docker run -it --rm -v /home/jbr/dbc-docker:/tmp dbc-tools-compose-graph ./dbc-java-fedora-indexer/src/main/docker/share/compose/dbc-java-fedora-indexer-compose.yml,./dbc-postgres-filestore/src/main/docker/share/compose/dbc-postgres-filestore-compose.yml,./dbc-postgres-inflight/src/main/docker/share/compose/dbc-postgres-inflight-compose.yml,./dbc-java-hive/src/main/docker/share/compose/dbc-java-hive-compose.yml,./dbc-example/src/main/docker/share/compose/dbc-new-image-compose.yml,./dbc-glassfish-diff-sink/src/main/docker/share/compose/dbc-glassfish-diff-sink-compose.yml,./dbc-postgres/src/main/docker/share/compose/dbc-postgres-compose.yml,./dbc-postgres-flowstore/src/main/docker/share/compose/dbc-postgres-flowstore-compose.yml,./dbc-java-gatekeeper/src/main/docker/share/compose/dbc-java-gatekeeper-compose.yml,./dbc-glassfish-jobprocessor/src/main/docker/share/compose/dbc-glassfish-jobprocessor-compose.yml,./dbc-glassfish/src/main/docker/share/compose/dbc-glassfish-compose.yml,./dbc-glassfish-jobstore/src/main/docker/share/compose/dbc-glassfish-jobstore-compose.yml,./dbc-glassfish-gui/src/main/docker/share/compose/dbc-glassfish-gui-compose.yml,./dbc-glassfish-dummy-sink/src/main/docker/share/compose/dbc-glassfish-dummy-sink-compose.yml,./dbc-postgres-logstore/src/main/docker/share/compose/dbc-postgres-logstore-compose.yml,./dbc-glassfish-harvester/src/main/docker/share/compose/dbc-glassfish-harvester-compose.yml,./dbc-openmq/src/main/docker/share/compose/dbc-openmq-compose.yml,./dbc-postgres-holdingsitems/src/main/docker/share/compose/dbc-postgres-holdingsitems-compose.yml,./dbc-tomcat-solr-rr/src/main/docker/share/compose/dbc-tomcat-solr-rr-compose.yml,./dbc-postgres-rawrepo/src/main/docker/share/compose/dbc-postgres-rawrepo-compose.yml,./dbc-tomcat-solr/src/main/docker/share/compose/dbc-tomcat-solr-compose.yml,./dbc-glassfish-flowstore/src/main/docker/share/compose/dbc-glassfish-flowstore-compose.yml,./dbc-glassfish-filestore/src/main/docker/share/compose/dbc-glassfish-filestore-compose.yml,./dbc-glassfish-es-sink/src/main/docker/share/compose/dbc-glassfish-es-sink-compose.yml,./dbc-glassfish-holdingsitems-update/src/main/docker/share/compose/dbc-glassfish-holdingsitems-update-compose.yml,./dbc-postgres-jobstore/src/main/docker/share/compose/dbc-postgres-jobstore-compose.yml,./dbc-fakesmtp/src/main/docker/share/compose/dbc-fakesmtp-compose.yml,./dbc-tomcat-fcrepo/src/main/docker/share/compose/dbc-tomcat-fcrepo-compose.yml,./dbc-glassfish-logstore/src/main/docker/share/compose/dbc-glassfish-logstore-compose.yml,./dbc-glassfish-rr-content-service/src/main/docker/share/compose/dbc-glassfish-rr-content-service-compose.yml,./dbc-tomcat-solr-fedora-holdingsitems/src/main/docker/share/compose/dbc-tomcat-solr-fedora-holdingsitems-compose.yml
```


No work:
python doDockerGraph.py -y <commaseparated yml files>
Brug docker image

## Motivation

Der er en del afhængigheder til dbc-tools-compose-graph. For at skærme udvikleren for disse afhængigheder er de pakket
ind i et docker image.

## Tests
mvn test

## Contributors

Jesper Bruun

