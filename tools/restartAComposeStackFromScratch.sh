#!/usr/bin/env bash

# Stop on error
set -e
# Debug
set -x


ALL_ARGUMENTS=$@

function restartAComposeStackFromScratch
{
    echo Nuking $ALL_ARGUMENTS
    docker-compose $ALL_ARGUMENTS down
    docker-compose $ALL_ARGUMENTS rm -f
    docker-compose $ALL_ARGUMENTS build
    docker-compose $ALL_ARGUMENTS up --force-recreate
}

restartAComposeStackFromScratch
