#!/usr/bin/env bash
echo "Stopping existing containers"
docker stop dbc-glassfish-rr-introspect || true
#docker rm dbc-glassfish-rr-introspect || true
echo "Staring container"
nohup docker run --name dbc-glassfish-rr-introspect --rm -v /cmdbdata:/tmp --env-file=/data/dataio/docker/env-rr-introspect.env -p 4851:4848 -p 8091:8080 docker-index:5000/dbc-glassfish-rr-introspect >/data/dataio/docker/rr-inspect.log 2>&1 &
