import getopt
import sys
import os, re
import urllib2


def parseFiles(argv):
    opts, args = getopt.getopt(argv,"u:",["url="])

    url=''
    for opt, arg in opts:

       if opt in ("-u", "--url"):

           url = arg

    return url


def main(argv):
    url=parseFiles(argv=argv)

    response = urllib2.urlopen(url)
    html = response.readlines()
    max_rel=0
    for line in html:
        match = re.compile('.*"([0-9]*)/".*').match(line)
        if match:
            relnum = int(match.group(1))
            if relnum>max_rel:
                max_rel=relnum
    print (max_rel)
if __name__ == "__main__":
   main(sys.argv[1:])
