#!/usr/bin/env bash



function usage
{
    echo "deployIntrospect.sh: -h or --host <Host to deploy to> -d or --dir <Temp dir to use when deploying docker image> -u or --user <User to login as on host>"
}

if [ $# -eq 0 ]; then
    usage
    exit 1
fi



while [ "$1" != "" ]; do
    
    case $1 in
       -h | --host )            shift
                                HOST=$1
                                ;;
       -d | --dir )             shift
                                DEPLOYDIR=$1
                                ;;
      -u | --user )             shift
                                USER=$1
                                ;;
        * )                     usage
                                exit 1
    esac
    shift
done




#HOST=danbib-i01
#DEPLOYDIR=/data/dataio/docker
#USER=jenkins

scp ./*-rr-introspect.env $USER@$HOST:$DEPLOYDIR/

# Build run sript
cat  << EOF > ./run.sh
#!/usr/bin/env bash
echo "Stopping existing containers"
docker stop dbc-glassfish-rr-introspect || true
#docker rm dbc-glassfish-rr-introspect || true
echo "Staring container"
nohup docker run --name dbc-glassfish-rr-introspect --rm -v /cmdbdata:/tmp --env-file=${DEPLOYDIR}/env-rr-introspect.env -p 4851:4848 -p 8098:8080 docker-index:5000/dbc-glassfish-rr-introspect >${DEPLOYDIR}/rr-inspect.log 2>&1 &
EOF
# End of build runscript

chmod +x ./run.sh

# Push to targethost
scp ./run.sh $USER@$HOST:${DEPLOYDIR}/run.sh

# Run it
ssh $USER@$HOST ${DEPLOYDIR}/run.sh 
