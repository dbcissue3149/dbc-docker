#!/usr/bin/env bash

set -e

# when the shell expands, dont include the pattern in the list of results (e.g. exclude *.xml)
shopt -s nullglob
export DB=$(/usr/local/bin/cmdb-get db/dbname ${DEFDB})
export DBUSER=$(/usr/local/bin/cmdb-get db/dbusername ${DEFDBUSER})
export DBPASSWORD=$(/usr/local/bin/cmdb-get db/dbpassword ${DEFDBPASSWORD})
export DBHOST=$(/usr/local/bin/cmdb-get db/dbhost ${DEFDBHOST})
export DBPORT=$(/usr/local/bin/cmdb-get db/dbport ${DEFDBPORT})

export DB2=$(/usr/local/bin/cmdb-get db/dbname2 ${DEFDB2})
export DBUSER2=$(/usr/local/bin/cmdb-get db/dbusername2 ${DEFDBUSER2})
export DBPASSWORD2=$(/usr/local/bin/cmdb-get db/dbpassword2 ${DEFDBPASSWORD2})
export DBHOST2=$(/usr/local/bin/cmdb-get db/dbhost2 ${DEFDBHOST2})
export DBPORT2=$(/usr/local/bin/cmdb-get db/dbport2 ${DEFDBPORT2})
export JDBCNAME2=$(/usr/local/bin/cmdb-get db/jdbcname2 ${DEFJDBCNAME2})
export JDBCNAME=$(/usr/local/bin/cmdb-get db/jdbcname ${DEFJDBCNAME})


env
cat << EOF > ${GLASSFISH_USER_HOME}/dbc-glassfish.d/18-rawrepo.xml
<?xml version="1.0" encoding="UTF-8"?> 
<!DOCTYPE resources PUBLIC "-//GlassFish.org//DTD GlassFish Application Server 3.1 Resource Definitions//EN" "http://glassfish.org/dtds/glassfish-resources_1_5.dtd"> 
<resources>
    <jdbc-connection-pool  
                          datasource-classname="org.postgresql.ds.PGSimpleDataSource" 
                          name="jdbc/dataio/rawrepo2/pool">            
      <property name="user" value="${DBUSER}"></property>
      <property name="PortNumber" value="${DBPORT}"></property>
      <property name="password" value="${DBPASSWORD}"></property>
      <property name="DriverClass" value="org.postgresql.Driver"></property>
      <property name="DatabaseName" value="${DB}"></property>
      <property name="serverName" value="${DBHOST}"></property>
    </jdbc-connection-pool>

    <jdbc-resource pool-name="jdbc/dataio/rawrepo/pool" jndi-name="jdbc/rawrepointrospect/${JDBCNAME}"></jdbc-resource>
    
    <jdbc-connection-pool  
                          datasource-classname="org.postgresql.ds.PGSimpleDataSource" 
                          name="jdbc/dataio/rawrepo/pool">            
      <property name="user" value="${DBUSER2}"></property>
      <property name="PortNumber" value="${DBPORT2}"></property>
      <property name="password" value="${DBPASSWORD2}"></property>
      <property name="DriverClass" value="org.postgresql.Driver"></property>
      <property name="DatabaseName" value="${DB2}"></property>
      <property name="serverName" value="${DBHOST2}"></property>
    </jdbc-connection-pool>

    <jdbc-resource pool-name="jdbc/dataio/rawrepo2/pool" jndi-name="jdbc/rawrepointrospect/${JDBCNAME2}"></jdbc-resource>


</resources> 
EOF
