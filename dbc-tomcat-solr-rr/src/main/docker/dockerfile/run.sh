#!/usr/bin/env bash
set -e  

/usr/local/bin/confd -onetime -backend env

sed -i "s?\${solr.autoSoftCommit.maxTime:-1}?$DBC_TOMCAT_SOLR_RR_ENV_MAXTIME?" $CATALINA_HOME/solr/rawrepo/conf/solrconfig.xml

echo "Starting Tomcat"
exec /usr/local/tomcat/bin/catalina.sh run
