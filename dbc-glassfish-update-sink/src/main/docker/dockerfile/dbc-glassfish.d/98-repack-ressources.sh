#!/usr/bin/env bash

set -e

# when the shell expands, dont include the pattern in the list of results (e_g_ exclude *_xml)
shopt -s nullglob




env


cd ${GLASSFISH_USER_HOME}/${WAR}



jar cmf META-INF/MANIFEST.MF ../${WAR}.war *
chown gfish:gfish  ../${WAR}.war
