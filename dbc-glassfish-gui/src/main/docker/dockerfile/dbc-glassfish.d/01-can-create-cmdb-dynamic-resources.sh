#!/usr/bin/env bash

set -e

# when the shell expands, dont include the pattern in the list of results (e.g. exclude *.xml)
shopt -s nullglob
echo DEBUG
echo $HARVESTERCONFIG
HARVESTERCONFIGDATA=$(curl $HARVESTERCONFIG|python $GLASSFISH_USER_HOME/dbc-glassfish.d/getHarvesterConfig.py) 
env



cat << EOF > ${GLASSFISH_USER_HOME}/dbc-glassfish.d/21-harvester-jndi.cmd
create-custom-resource --restype=java.lang.String --passwordfile=./passfile.txt --factoryclass=org.glassfish.resources.custom.factory.PrimitivesAndStringFactory --property value=\'${HARVESTERCONFIGDATA}\' config/dataio/harvester/rr
EOF
