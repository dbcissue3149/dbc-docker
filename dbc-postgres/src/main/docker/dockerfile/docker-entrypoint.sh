#!/usr/bin/env bash

# This script initializes the database with users and imports *.sql and *.pgdump files into it

# Tell me what you do, before you do it
set -o verbose



echo Bootstrapping database [$POSTGRES_DB] with user [$POSTGRES_USER] and password [$POSTGRES_PASSWORD]

/etc/init.d/postgresql start

until pg_isready; do
    sleep 1
    echo "Waiting ... for postgres to be ready"
done;
echo "postgres is ready"

# Run all scripts in dbc-postgres.d/ dir

set -e # Exit on any error in the executed shell scripts
shopt -s nullglob # shopt -s nullglob prevents shell expansion to return a result when there are no *.sql files
if [ -d /docker-entrypoint.d ]; then
    for script in /docker-entrypoint.d/*.sh; do
        echo "===> Executing pre script " $script
        $script
    done
fi

/etc/init.d/postgresql stop

# Use exec to ensure that process can be killed properly.
exec /usr/lib/postgresql/9.4/bin/postgres -D /var/lib/postgresql/9.4/main -c config_file=/etc/postgresql/9.4/main/postgresql.conf
