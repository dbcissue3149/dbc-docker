#!/usr/bin/env bash

set -o verbose

# Allows a postgres instance to import any dump file, by adding the file to docker-entrypoint-initdb.d 

shopt -s nullglob
for sqlFile in /docker-entrypoint.d/*.pgdump; do
	echo "===> Importing [" $sqlFile "]"
    PGPASSWORD=$POSTGRES_PASSWORD psql $POSTGRES_USER -h localhost -d $POSTGRES_DB -f $sqlFile;
done

for sqlFile in /docker-entrypoint.d/*.sql; do
	echo "===> Importing [" $sqlFile "]"
    PGPASSWORD=$POSTGRES_PASSWORD psql $POSTGRES_USER -h localhost -d $POSTGRES_DB -f $sqlFile;
done
