#!/usr/bin/env bash

set -o verbose

# Allows a postgres instance to import any dump file, by adding the file to docker-entrypoint-initdb.d 

shopt -s nullglob
echo

psql -c "CREATE USER $POSTGRES_USER WITH PASSWORD '$POSTGRES_PASSWORD';"

echo Creating database
createdb -O $POSTGRES_USER $POSTGRES_DB;
